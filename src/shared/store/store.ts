import {configureStore} from '@reduxjs/toolkit'
import {authReducer} from "./features/authSlice";
import thunk from "redux-thunk";
// import logger from 'redux-logger'
import {contestReducer} from "./features/contest/contestSlice";
import {userReducer} from "./features/userSlice";
import {paystackReducer} from "./features/contest/paystackSlice";
import {dashboardReducer} from "./features/dashboardSlice";
import {categoryReducer} from "./features/contest/categorySlice";
import {contestantReducer} from "./features/contest/contestantSlice";
import {Environment} from "../utils/environment";
import {contestVoteReducer} from "./features/contest/contestVoteSlice";
import {pollReducer} from "./features/poll/pollSlice";
import {pollVoteReducer} from "./features/poll/pollVoteSlice";

export const store = configureStore({
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(thunk),
  devTools: Environment.isDevelopment,
  reducer: {
    auth: authReducer,
    contest: contestReducer,
    poll: pollReducer,
    user: userReducer,
    dashboard: dashboardReducer,
    bank: paystackReducer,
    category: categoryReducer,
    contestant: contestantReducer,
    contestVote: contestVoteReducer,
    pollVote: pollVoteReducer,
  },
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;

// Inferred type: {auth: AuthState}
export type AppDispatch = typeof store.dispatch;
