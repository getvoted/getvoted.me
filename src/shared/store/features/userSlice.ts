import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import {API} from "../../utils/api";
import {RootState} from "../store";
import {toast} from "react-toastify";

export interface UserInterface {
    userName: string;
    firstname?: string;
    lastname?: string;
    email: string;
    id: number;
}

interface UserState {
    user: UserInterface,
}

export const userInitialState: UserState = {
    user: {
        userName: '',
        firstname: '',
        lastname: '',
        email: '',
        id: 0,
    },
}

export const getUserAsync = createAsyncThunk(
    'user/async',
    async (_, {rejectWithValue}) => {
        try {
            const response = await API.getUser();
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

const userSlice = createSlice({
    name: 'user',
    initialState: userInitialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(getUserAsync.fulfilled, (state, action) => {
                state.user = action.payload
            })
    }
})

// export const {} = userSlice.actions

// Other code such as selectors can use the imported `RootState` type
// export const selectCount = (state: RootState) => state.todos.value

export const selectUser = (state: RootState) => state.user.user

export const userReducer = userSlice.reducer;
