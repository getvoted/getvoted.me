import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import {API} from "../../../utils/api";
import {RootState} from "../../store";
import {toast} from "react-toastify";
import {UserInterface} from "../userSlice";

export interface PollVoteInterface {
    pollId: number | null;
    pollOption: string | null;
    userIdentifier: string | null;
    createdAt?: string;
    voter?: UserInterface;
    id?: number;
}

interface State {
    pollVotes: { [pollLink: string]: PollVoteInterface[] },
    myVote: PollVoteInterface
}

export const pollVoteInitialState: State = {
    pollVotes: {},
    myVote: {
        pollId: null,
        pollOption: null,
        userIdentifier: null,
    },
}

export const getPollVotesAsync = createAsyncThunk(
    'pollVote/get-poll-votes',
    async (payload: { username: string, pollLink: string }, {rejectWithValue}) => {
        try {
            const response = await API.getPollVotes(payload.username, payload.pollLink);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const getMyVoteForPollAsync = createAsyncThunk(
    'pollVote/get-my-poll-vote',
    async (payload: { username: string, pollLink: string, userIdentifier: string }, {rejectWithValue}) => {
        try {
            const response = await API.getMyVoteForPoll(payload.username, payload.pollLink, payload.userIdentifier);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const voteFreePollAsync = createAsyncThunk(
    'pollVote/vote',
    async (payload: PollVoteInterface, {rejectWithValue}) => {
        try {
            const response = await API.voteFreePoll(payload);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

const pollSlice = createSlice({
    name: 'pollVote',
    initialState: pollVoteInitialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(getPollVotesAsync.fulfilled, (state, action) => {
                state.pollVotes[action.meta.arg.pollLink] = action.payload
            })
            .addCase(getMyVoteForPollAsync.fulfilled, (state, action) => {
                state.myVote = action.payload
            })
            .addCase(voteFreePollAsync.fulfilled, (state, action) => {
                state.myVote = action.payload
            })
    }
})

// export const {} = voteSlice.actions

export const selectMyPollVote = (state: RootState) => state.pollVote.myVote

export const getPollVotesFromState = (pollLink: string) => (state: RootState) => {
    return state.pollVote.pollVotes[pollLink];
}

export const pollVoteReducer = pollSlice.reducer;
