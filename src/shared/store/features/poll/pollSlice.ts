import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import {API} from "../../../utils/api";
import {RootState} from "../../store";
import {toast} from "react-toastify";
import {UserInterface} from "../userSlice";

export interface PollInterface {
    name: string;
    description?: string;
    link: string;
    status?: string;
    visibility?: string;
    displayPicture?: string;
    id?: number;
    totalNoOfEntries?: number;
    createdAt?: string;
    user?: UserInterface;
    options?: any[];
}

interface State {
    poll: PollInterface,
    polls: PollInterface[],
}

export const pollInitialState: State = {
    poll: {
        name: '',
        description: '',
        link: '',
        options: ['', '']
    },
    polls: []
}

export const getMyPollsAsync = createAsyncThunk(
    'poll/get-my',
    async (_, {rejectWithValue}) => {
        try {
            const response = await API.getMyPolls();
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const getProfilePollsAsync = createAsyncThunk(
    'poll/get-profile',
    async (userName: string, {rejectWithValue}) => {
        try {
            const response = await API.getProfilePolls(userName);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const getPollAsync = createAsyncThunk(
    'poll/get',
    async (payload: {username: string, pollLink: string}, {rejectWithValue}) => {
        try {
            const response = await API.getPoll(payload.username, payload.pollLink);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const createPollAsync = createAsyncThunk(
    'poll/create',
    async (payload: PollInterface, {rejectWithValue}) => {
        try {
            const response = await API.createPoll(payload);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const deletePollAsync = createAsyncThunk(
    'poll/delete',
    async (pollId: number, {rejectWithValue}) => {
        try {
            const response = await API.deletePoll(pollId);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const startPollAsync = createAsyncThunk(
    'poll/start',
    async (pollId: number, {rejectWithValue}) => {
        try {
            const response = await API.startPoll(pollId);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const endPollAsync = createAsyncThunk(
    'poll/end',
    async (pollId: number, {rejectWithValue}) => {
        try {
            const response = await API.endPoll(pollId);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

const pollSlice = createSlice({
    name: 'poll',
    initialState: pollInitialState,
    reducers: {
        clearStatePoll: (state) => {
            state.poll = {
                name: '',
                description: '',
                link: '',
            };
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(getMyPollsAsync.fulfilled, (state, action) => {
                state.polls = action.payload
            })
            .addCase(getPollAsync.fulfilled, (state, action) => {
                state.poll = action.payload
                state.poll.totalNoOfEntries = action.payload.totalNoOfEntries
            })
            .addCase(endPollAsync.fulfilled, (state, action) => {
                state.poll = action.payload
            })
    }
})

export const {clearStatePoll} = pollSlice.actions

export const selectPolls = (state: RootState) => state.poll.polls
export const selectPoll = (state: RootState) => state.poll.poll

export const pollReducer = pollSlice.reducer;
