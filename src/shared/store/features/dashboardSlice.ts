import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import {toast} from "react-toastify";
import {PollInterface} from "./poll/pollSlice";
import {ContestInterface} from "./contest/contestSlice";
import {API} from "../../utils/api";
import {RootState} from "../store";

interface State {
    events: ContestInterface[],
    polls: PollInterface[],
    noOfFreeContests: number
    noOfPaidContests: number
    noOfPolls: number
}

export const dashboardInitialState: State = {
    events: [],
    polls: [],
    noOfFreeContests: 0,
    noOfPaidContests: 0,
    noOfPolls: 0
}

export const getDashboardSummaryAsync = createAsyncThunk(
    'dashboard/get-summary',
    async (_, {rejectWithValue}) => {
        try {
            const response = await API.getDashboardSummary();
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

const pollSlice = createSlice({
    name: 'dashboard',
    initialState: dashboardInitialState,
    reducers: {
    },
    extraReducers: (builder) => {
        builder
            .addCase(getDashboardSummaryAsync.fulfilled, (state, action) => {
                state.events = action.payload.contests
                state.polls = action.payload.polls
                state.noOfFreeContests = action.payload.noOfFreeContests;
                state.noOfPaidContests = action.payload.noOfPaidContests;
                state.noOfPolls = action.payload.noOfPolls;
            })
    }
})

export const selectDashboardEvents = (state: RootState) => state.dashboard.events
export const selectDashboardPolls = (state: RootState) => state.dashboard.polls
export const selectDashboardNoOfPaidContests = (state: RootState) => state.dashboard.noOfPaidContests
export const selectDashboardNoOfFreeContests = (state: RootState) => state.dashboard.noOfFreeContests
export const selectDashboardNoOfPolls = (state: RootState) => state.dashboard.noOfPolls

export const dashboardReducer = pollSlice.reducer;
