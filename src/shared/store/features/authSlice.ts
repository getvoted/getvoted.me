import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import {API} from "../../utils/api";
import {App} from "../../utils/app";
import {Tokens} from "../../utils/tokens";

export interface LoginInterface {
    email: string;
    password: string;
}

export interface ForgotPasswordInterface {
    email: string;
}

export interface RegisterInterface {
    userName: string;
    email: string;
    password: string;
    confirmPassword: string;
}

export interface VerifyEmailInterface {
    email: string;
    token: string;
}

export interface ResendEmailVerificationLinkInterface {
    email: string;
    redirectTo: string;
}

export interface ResetPasswordInterface {
    newPassword: string;
    confirmPassword: string;
    token: string;
}

interface State {
    login: LoginInterface,
    forgotPassword: ForgotPasswordInterface,
    register: RegisterInterface,
    verifyEmail: VerifyEmailInterface
    resendEmailVerificationLink: ResendEmailVerificationLinkInterface
    resetPassword: ResetPasswordInterface
}

export const authInitialState: State = {
    login: {
        email: '',
        password: '',
    },
    forgotPassword: {
        email: '',
    },
    register: {
        userName: '',
        email: '',
        password: '',
        confirmPassword: '',
    },
    verifyEmail: {
        email: '',
        token: '',
    },
    resendEmailVerificationLink: {
        email: '',
        redirectTo: '',
    },
    resetPassword: {
        newPassword: '',
        confirmPassword: '',
        token: '',
    },
}

export const loginAsync = createAsyncThunk(
    'auth/login-async',
    async (payload: LoginInterface, {rejectWithValue}) => {
        try {
            const response = await API.login(payload);
            App.storeAuthTokens(response.data.data.accessToken, response.data.data.refreshToken)
            return response.data;
        } catch (error: any) {
            return rejectWithValue(error.response.data)
        }
    }
);

export const forgotPasswordAsync = createAsyncThunk(
    'auth/forgot-async',
    async (payload: ForgotPasswordInterface, {rejectWithValue}) => {
        try {
            const response = await API.forgotPassword(payload);
            return response.data;
        } catch (error: any) {
            return rejectWithValue(error.response.data)
        }
    }
);

export const registerAsync = createAsyncThunk(
    'auth/register-async',
    async (payload: RegisterInterface, {rejectWithValue}) => {
        try {
            const response = await API.register(payload);
            return response.data.data;
        } catch (error: any) {
            return rejectWithValue(error.response.data)
        }
    }
);

export const verifyEmailAsync = createAsyncThunk(
    'auth/verify-async',
    async (payload: VerifyEmailInterface, {rejectWithValue}) => {
        try {
            const response = await API.verifyEmail(payload);
            App.storeAuthTokens(response.data.data.accessToken, response.data.data.refreshToken)
            return response.data.data;
        } catch (error: any) {
            return rejectWithValue(error.response.data)
        }
    }
);

export const resendEmailVerificationLinkAsync = createAsyncThunk(
    'auth/resend-async',
    async (payload: ResendEmailVerificationLinkInterface, {rejectWithValue}) => {
        try {
            const response = await API.resendEmailVerificationLink(payload);
            return response.data.data;
        } catch (error: any) {
            return rejectWithValue(error.response.data)
        }
    }
);

export const resetPasswordAsync = createAsyncThunk(
    'auth/reset-async',
    async (payload: ResetPasswordInterface, {rejectWithValue}) => {
        try {
            const response = await API.resetPassword(payload);
            App.storeAuthTokens(response.data.data.accessToken, response.data.data.refreshToken)
            return response.data.data;
        } catch (error: any) {
            return rejectWithValue(error.response.data)
        }
    }
);

export const refreshTokenAsync = createAsyncThunk(
    'auth/refresh-async',
    async (_, {rejectWithValue}) => {
        try {
            const response = await API.refreshToken({token: Tokens.getRefreshToken()});
            App.storeAuthTokens(response.data.data.accessToken, response.data.data.refreshToken)
            return response.data;
        } catch (error: any) {
            return rejectWithValue(error.response.data)
        }
    }
);

export const logoutAsync = createAsyncThunk(
    'auth/logout-async',
    async (payload: LoginInterface, {rejectWithValue}) => {
        try {
            const response = await API.logout();
            App.storeAuthTokens(response.data.data.accessToken, response.data.data.refreshToken)
            return response.data;
        } catch (error: any) {
            return rejectWithValue(error.response.data)
        }
    }
);

const authSlice = createSlice({
    name: 'auth',
    initialState: authInitialState,
    reducers: {},
})

export const authReducer = authSlice.reducer;
