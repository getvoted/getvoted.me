import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import {API} from "../../../utils/api";
import {RootState} from "../../store";
import {toast} from "react-toastify";

export interface CategoryInterface {
    name: string;
    link: string;
    contestId?: number;
    id?: number;
    totalNoOfVotes?: number;
    totalNoOfVoters?: number;
}

interface State {
    category: CategoryInterface,
    categories: CategoryInterface[],
}

export const categoryInitialState: State = {
    category: {
        name: '',
        link: '',
    },
    categories: []
}

export const getCategoriesAsync = createAsyncThunk(
    'category/get-all',
    async (payload: {username: string, contestLink: string}, {rejectWithValue}) => {
        try {
            const response = await API.getCategories(payload.username, payload.contestLink);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const getCategoriesForCreatorAsync = createAsyncThunk(
    'category/get-all-for-creator',
    async (payload: {username: string, contestLink: string}, {rejectWithValue}) => {
        try {
            const response = await API.getCategoriesForCreator(payload.username, payload.contestLink);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const getCategoryAsync = createAsyncThunk(
    'category/get',
    async (payload: { username: string, contestLink: string, categoryLink: string }, {rejectWithValue}) => {
        try {
            const response = await API.getCategory(payload.username, payload.contestLink, payload.categoryLink);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const createCategoryAsync = createAsyncThunk(
    'category/create',
    async (payload: CategoryInterface, {rejectWithValue}) => {
        try {
            const response = await API.createCategory(payload);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const updateCategoryAsync = createAsyncThunk(
    'category/update',
    async (payload: CategoryInterface, {rejectWithValue}) => {
        try {
            const response = await API.updateCategory(payload);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const deleteCategoryAsync = createAsyncThunk(
    'category/delete',
    async (payload: { eventId: number, categoryId: number }, {rejectWithValue}) => {
        try {
            const response = await API.deleteCategory(payload.eventId, payload.categoryId);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

const categorySlice = createSlice({
    name: 'category',
    initialState: categoryInitialState,
    reducers: {
        clearStateCategory: (state) => {
            state.category = {
                name: '',
                link: '',
            };
        },
        clearStateCategories: (state) => {
            state.categories = [];
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(getCategoriesAsync.fulfilled, (state, action) => {
                state.categories = action.payload
            })
            .addCase(getCategoriesForCreatorAsync.fulfilled, (state, action) => {
                state.categories = action.payload
            })
            .addCase(getCategoryAsync.fulfilled, (state, action) => {
                state.category = action.payload
            })
            .addCase(updateCategoryAsync.fulfilled, (state, action) => {
                state.category = action.payload
            })
    }
})

export const {clearStateCategory, clearStateCategories} = categorySlice.actions

export const selectCategories = (state: RootState) => state.category.categories
export const selectCategory = (state: RootState) => state.category.category

export const categoryReducer = categorySlice.reducer;
