import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import {API} from "../../../utils/api";
import {RootState} from "../../store";
import {toast} from "react-toastify";

export interface ContestantInterface {
    name: string;
    link: string;
    contestId?: number;
    categoryId?: number;
    id?: number;
    description?: string;
    totalNoOfVotes?: number;
    totalNoOfVoters?: number;
    displayPicture?: string;
}

interface State {
    contestants: { [categoryLink: string]: ContestantInterface[] },
    contestant: ContestantInterface,
}

export const contestantInitialState: State = {
    contestant: {
        name: '',
        link: '',
    },
    contestants: {},
}

export const getContestantsAsync = createAsyncThunk(
    'contestant/get-all',
    async (payload: { username: string, contestLink: string, categoryLink: string }, {rejectWithValue}) => {
        try {
            const response = await API.getContestants(payload.username, payload.contestLink, payload.categoryLink);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const getContestantsForCreatorAsync = createAsyncThunk(
    'contestant/get-all-for-creator',
    async (payload: { username: string, contestLink: string, categoryLink: string }, {rejectWithValue}) => {
        try {
            const response = await API.getContestantsForCreator(payload.username, payload.contestLink, payload.categoryLink);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const getContestantAsync = createAsyncThunk(
    'contestant/get',
    async (payload: { username: string, contestLink: string, categoryLink: string, contestantLink: string }, {rejectWithValue}) => {
        try {
            const response = await API.getContestant(payload.username, payload.contestLink, payload.categoryLink, payload.contestantLink);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const createContestantAsync = createAsyncThunk(
    'contestant/create',
    async (payload: ContestantInterface, {rejectWithValue}) => {
        try {
            const response = await API.createContestant(payload);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const updateContestantAsync = createAsyncThunk(
    'contestant/update',
    async (payload: ContestantInterface, {rejectWithValue}) => {
        try {
            const response = await API.updateContestant(payload);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const deleteContestantAsync = createAsyncThunk(
    'contestant/delete',
    async (payload: { eventId: number, contestantId: number }, {rejectWithValue}) => {
        try {
            const response = await API.deleteContestant(payload.eventId, payload.contestantId);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

const contestantSlice = createSlice({
    name: 'contestant',
    initialState: contestantInitialState,
    reducers: {
        clearStateContestant: (state) => {
            state.contestant = {
                name: '',
                link: '',
            };
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(getContestantAsync.fulfilled, (state, action) => {
                state.contestant = action.payload
            })
            .addCase(getContestantsForCreatorAsync.fulfilled, (state, action) => {
                state.contestants[action.meta.arg.categoryLink] = action.payload
            })
            .addCase(getContestantsAsync.fulfilled, (state, action) => {
                state.contestants[action.meta.arg.categoryLink] = action.payload
            })
            .addCase(updateContestantAsync.fulfilled, (state, action) => {
                state.contestant = action.payload
            })
    }
})

export const {clearStateContestant} = contestantSlice.actions

export const selectContestants = (state: RootState) => state.contestant.contestants
export const selectContestant = (state: RootState) => state.contestant.contestant

export const getContestantFromState = (categoryLink: string) => (state: RootState) => {
    return state.contestant.contestants[categoryLink];
}

export const contestantReducer = contestantSlice.reducer;
