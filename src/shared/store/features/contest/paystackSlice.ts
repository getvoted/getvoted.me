import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import {API} from "../../../utils/api";
import {RootState} from "../../store";
import {toast} from "react-toastify";

export interface BankInterface {
    active: boolean
    code: string
    country: string
    createdAt: string
    currency: string
    gateway: string
    id: number
    isDeleted: boolean | null
    longcode: string
    name: string
    slug: string
    type: string
    updatedAt: string
}

export interface AccountDetailsInterface {
    AccountNumber: string;
    BankCode: string;
}

interface State {
    banks: BankInterface[],
    transactionDetails: any,
}

export const paystackInitialState: State = {
    banks: [],
    transactionDetails: {},
}

export const getBanksAsync = createAsyncThunk(
    'paystack/bank/get-all',
    async (_, {rejectWithValue}) => {
        try {
            const response = await API.getBanks();
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const validateAccountAsync = createAsyncThunk(
    'paystack/validate/account',
    async (payload: AccountDetailsInterface, {rejectWithValue}) => {
        try {
            const response = await API.validateAccountNumber(payload);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

const paystackSlice = createSlice({
    name: 'paystack',
    initialState: paystackInitialState,
    reducers: {
        clearStatePaystack: (state) => {
            state.banks = []
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(getBanksAsync.fulfilled, (state, action) => {
                state.banks = action.payload
            })
    }
})

export const {clearStatePaystack} = paystackSlice.actions

export const selectBanks = (state: RootState) => state.bank.banks

export const paystackReducer = paystackSlice.reducer;
