import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import {API} from "../../../utils/api";
import {RootState} from "../../store";
import {toast} from "react-toastify";
import {UserInterface} from "../userSlice";

export interface ContestInterface {
    name: string;
    description: string;
    link: string;
    status?: string;
    visibility?: string;
    showLiveUpdates?: string;
    displayPicture?: string;
    id?: number;
    type?: string;
    totalNoOfVotes?: number;
    totalNoOfVoters?: number;
    user?: UserInterface;

    amount?: string;
    bank?: string;
    bankCode?: string;
    accountName?: string;
    accountNumber?: string;

    isWithdrawn?: boolean;
    withdrawRequested?: boolean;
    dateWithdrawRequested?: string;
}

export interface ContestSettingsInterface {
    id: number
    visibility?: string
    accessibility?: string
    status?: string
    showLiveUpdates?: string
}

export interface ContestWithdrawInterface {
    contestId: number | null;
}

interface State {
    contest: ContestInterface,
    contests: ContestInterface[],
}

export const contestInitialState: State = {
    contest: {
        name: '',
        description: '',
        type: '',
        link: '',
        amount: '',
        bank: '',
        bankCode: '',
        accountName: '',
        accountNumber: '',
    },
    contests: []
}

export const getMyContestsAsync = createAsyncThunk(
    'contest/get-my',
    async (_, {rejectWithValue}) => {
        try {
            const response = await API.getMyContests();
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const getProfileContestAsync = createAsyncThunk(
    'contest/get-profile',
    async (userName: string, {rejectWithValue}) => {
        try {
            const response = await API.getProfileContests(userName);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const getContestAsync = createAsyncThunk(
    'contest/get',
    async (payload: { username: string, contestLink: string }, {rejectWithValue}) => {
        try {
            const response = await API.getContest(payload.username, payload.contestLink);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const getContestAnonymouslyAsync = createAsyncThunk(
    'contest/get-anon',
    async (payload: { username: string, contestLink: string }, {rejectWithValue}) => {
        try {
            const response = await API.getContestAnonymously(payload.username, payload.contestLink);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const getContestForCreatorAsync = createAsyncThunk(
    'contest/get-for-creator',
    async (payload: { username: string, contestLink: string }, {rejectWithValue}) => {
        try {
            const response = await API.getContestForCreator(payload.username, payload.contestLink);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const createUserContestAsync = createAsyncThunk(
    'contest/create-user',
    async (payload: ContestInterface, {rejectWithValue}) => {
        try {
            const response = await API.createUserContest(payload);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const createUniqueContestAsync = createAsyncThunk(
    'contest/create-unique',
    async (payload: ContestInterface, {rejectWithValue}) => {
        try {
            const response = await API.createUniqueContest(payload);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const createPaidContestAsync = createAsyncThunk(
    'contest/create-paid',
    async (payload: ContestInterface, {rejectWithValue}) => {
        try {
            const response = await API.createPaidContest(payload);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const updateUserContestAsync = createAsyncThunk(
    'contest/update-user',
    async (payload: ContestInterface, {rejectWithValue}) => {
        try {
            const response = await API.updateUserContest(payload);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const updateUniqueContestAsync = createAsyncThunk(
    'contest/update-unique',
    async (payload: ContestInterface, {rejectWithValue}) => {
        try {
            const response = await API.updateUniqueContest(payload);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const updatePaidContestAsync = createAsyncThunk(
    'contest/update',
    async (payload: ContestInterface, {rejectWithValue}) => {
        try {
            const response = await API.updatePaidContest(payload);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const updateContestSettingsAsync = createAsyncThunk(
    'contest/settings',
    async (payload: ContestSettingsInterface, {rejectWithValue}) => {
        try {
            const response = await API.updateContestSettings(payload);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const deleteContestAsync = createAsyncThunk(
    'contest/delete',
    async (contestId: number, {rejectWithValue}) => {
        try {
            const response = await API.deleteContest(contestId);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const startContestAsync = createAsyncThunk(
    'contest/start',
    async (contestId: number, {rejectWithValue}) => {
        try {
            const response = await API.startContest(contestId);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const endContestAsync = createAsyncThunk(
    'contest/end',
    async (contestId: number, {rejectWithValue}) => {
        try {
            const response = await API.endContest(contestId);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const uploadDisplayPictureAsync = createAsyncThunk(
    'contest/upload-display-picture',
    async (payload: any, {rejectWithValue}) => {
        try {
            const response = await API.uploadDisplayPicture(payload);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const withdrawContestAsync = createAsyncThunk(
    'contest/withdraw',
    async (payload: ContestWithdrawInterface, {rejectWithValue}) => {
        try {
            const response = await API.withdrawPaidContest(payload);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

const contestSlice = createSlice({
    name: 'contest',
    initialState: contestInitialState,
    reducers: {
        clearStateContest: (state) => {
            state.contest = {
                name: '',
                description: '',
                link: '',
                type: '',
                amount: '',
                bank: '',
                accountName: '',
                accountNumber: '',
            };
        },
        updateContest: (state, newContest) => {
            state.contest = newContest.payload;
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(getMyContestsAsync.fulfilled, (state, action) => {
                state.contests = action.payload
            })
            .addCase(getContestAsync.fulfilled, (state, action) => {
                state.contest = action.payload
            })
            .addCase(getContestForCreatorAsync.fulfilled, (state, action) => {
                state.contest = action.payload
            })
            .addCase(getContestAnonymouslyAsync.fulfilled, (state, action) => {
                state.contest = action.payload
            })
            .addCase(startContestAsync.fulfilled, (state, action) => {
                state.contest = action.payload
            })
           .addCase(endContestAsync.fulfilled, (state, action) => {
                state.contest = action.payload
            })
            .addCase(updateContestSettingsAsync.fulfilled, (state, action) => {
                state.contest = action.payload
            })
            .addCase(withdrawContestAsync.fulfilled, (state, action) => {
                state.contest = action.payload
            })
            .addCase(getContestAsync.pending, () => {
                clearStateContest()
            })
    }
})

export const {clearStateContest, updateContest} = contestSlice.actions

export const selectContests = (state: RootState) => state.contest.contests
export const selectContest = (state: RootState) => state.contest.contest

export const contestReducer = contestSlice.reducer;
