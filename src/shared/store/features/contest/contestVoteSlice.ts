import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import {API} from "../../../utils/api";
import {RootState} from "../../store";
import {toast} from "react-toastify";
import {UserInterface} from "../userSlice";
import {updateContest} from "./contestSlice";

export interface ContestVoteInterface {
    contestId: number | null;
    categoryId: number | null;
    contestantId: number | null;
    createdAt?: string;
    identifier?: string;
    user?: UserInterface;
    id?: number;
}

export interface ContestResultInterface {
    totalNoOfVotes: number
    totalAmountForUs: number
    totalAmountForYou: number
    totalAmountGotten: number
    votes: ContestVoteInterface[]
}

export interface ContestVotePaidInterface {
    contestId: number | null;
    categoryId: number | null;
    contestantId: number | null;
    identifier?: string;
    reference: string;
    amount: number;
    noOfVotes: number;
}

interface State {
    contestVotes: { [categoryLink: string]: ContestResultInterface },
    categoryVotes: { [categoryLink: string]: ContestVoteInterface[] },
    contestantVotes: { [contestantLink: string]: ContestVoteInterface[] }
    vote: { [contestantLink: string]: ContestVoteInterface }
    votedInCategory: any
    votedForContestant: any
}

export const contestVoteInitialState: State = {
    contestVotes: {},
    categoryVotes: {},
    contestantVotes: {},
    vote: {},
    votedInCategory: {},
    votedForContestant: {},
}

export const getContestVotesAsync = createAsyncThunk(
    'contest/get-event-votes',
    async (payload: {username: string, contestLink: string}, {rejectWithValue}) => {
        try {
            const response = await API.getContestVotes(payload.username, payload.contestLink);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const getCategoryVotesAsync = createAsyncThunk(
    'contest/get-category-votes',
    async (payload: {username: string, contestLink: string, categoryLink: string}, {rejectWithValue}) => {
        try {
            const response = await API.getCategoryVotes(payload.username, payload.contestLink, payload.categoryLink);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const getContestantVotesAsync = createAsyncThunk(
    'contest/get-contestant-votes',
    async (payload: {username: string, contestLink: string, categoryLink: string, contestantLink: string}, {rejectWithValue}) => {
        try {
            const response = await API.getContestantVotes(payload.username, payload.contestLink, payload.categoryLink, payload.contestantLink);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const getMyVoteForContestantAsync = createAsyncThunk(
    'contest/get-my-contestant-vote',
    async (payload: {username: string, contestLink: string, categoryLink: string, contestantLink: string, identifier: string}, {rejectWithValue}) => {
        try {
            const response = await API.getMyVoteForContestant(payload.username, payload.contestLink, payload.categoryLink, payload.contestantLink, payload.identifier);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const getMyVoteForCategoryAsync = createAsyncThunk(
    'contest/get-my-category-vote',
    async (payload: {username: string, contestLink: string, categoryLink: string, identifier: string}, {rejectWithValue}) => {
        try {
            const response = await API.getMyVoteForCategory(payload.username, payload.contestLink, payload.categoryLink, payload.identifier);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const getMyVoteForContestantLoggedInAsync = createAsyncThunk(
    'contest/get-my-contestant-vote-logged-in',
    async (payload: {username: string, contestLink: string, categoryLink: string, contestantLink: string}, {rejectWithValue}) => {
        try {
            const response = await API.getMyVoteForContestantLoggedIn(payload.username, payload.contestLink, payload.categoryLink, payload.contestantLink);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const getMyVoteForCategoryLoggedInAsync = createAsyncThunk(
    'contest/get-my-category-vote-logged-in',
    async (payload: {username: string, contestLink: string, categoryLink: string}, {rejectWithValue}) => {
        try {
            const response = await API.getMyVoteForCategoryLoggedIn(payload.username, payload.contestLink, payload.categoryLink);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const voteUserContestAsync = createAsyncThunk(
    'contest/vote-user',
    async (payload: ContestVoteInterface, {rejectWithValue}) => {
        try {
            const response = await API.voteUserContest(payload);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const voteUniqueContestAsync = createAsyncThunk(
    'contest/vote-unique',
    async (payload: ContestVoteInterface, {rejectWithValue}) => {
        try {
            const response = await API.voteUniqueContest(payload);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

export const votePaidContestAsync = createAsyncThunk(
    'contest/vote-paid',
    async (payload: ContestVotePaidInterface, {rejectWithValue}) => {
        try {
            const response = await API.votePaidContest(payload);
            return response.data.data;
        } catch (error: any) {
            toast.error(error.response.data.message)
            return rejectWithValue(error.response.data)
        }
    }
);

const contestVoteSlice = createSlice({
    name: 'contestVote',
    initialState: contestVoteInitialState,
    reducers: {
    },
    extraReducers: (builder) => {
        builder
            .addCase(getContestVotesAsync.fulfilled, (state, action) => {
                state.contestVotes[action.meta.arg.contestLink] = action.payload
            })
            .addCase(getCategoryVotesAsync.fulfilled, (state, action) => {
                state.categoryVotes[action.meta.arg.categoryLink] = action.payload
            })
            .addCase(getContestantVotesAsync.fulfilled, (state, action) => {
                state.contestantVotes[action.meta.arg.contestantLink] = action.payload
            })
            .addCase(getMyVoteForContestantAsync.fulfilled, (state, action) => {
                // state.vote[action.meta.arg.contestantLink] = action.payload
                state.votedForContestant = action.payload
            })
            .addCase(getMyVoteForCategoryAsync.fulfilled, (state, action) => {
                // state.vote[action.meta.arg.contestantLink] = action.payload
                state.votedInCategory = action.payload
            })
            .addCase(getMyVoteForContestantLoggedInAsync.fulfilled, (state, action) => {
                // state.vote[action.meta.arg.contestantLink] = action.payload
                state.votedForContestant = action.payload
            })
            .addCase(getMyVoteForCategoryLoggedInAsync.fulfilled, (state, action) => {
                // state.vote[action.meta.arg.contestantLink] = action.payload
                state.votedInCategory = action.payload
            })
            .addCase(voteUserContestAsync.fulfilled, (state, action) => {
                updateContest(action.payload);
            })
            .addCase(votePaidContestAsync.fulfilled, (state, action) => {
                updateContest(action.payload);
            })
    }
})

// export const {} = voteSlice.actions

export const selectVotedForContestant = (state: RootState) => state.contestVote.votedForContestant && !!state.contestVote.votedForContestant.identifier
export const selectVotedInCategory = (state: RootState) => state.contestVote.votedInCategory && !!state.contestVote.votedInCategory.identifier

export const getEventVotesFromState = (contestLink: string) => (state: RootState) => {
    return state.contestVote.contestVotes[contestLink];
}

export const getCategoryVotesFromState = (categoryLink: string) => (state: RootState) => {
    return state.contestVote.categoryVotes[categoryLink];
}

export const getContestantVotesFromState = (contestantLink: string) => (state: RootState) => {
    return state.contestVote.contestantVotes[contestantLink];
}

export const getUserContestantVoteCountFromState = (contestantLink: string) => (state: RootState) => {
    return state.contestVote.vote[contestantLink];
}

export const contestVoteReducer = contestVoteSlice.reducer;
