import {useEffect} from "react";
import {hotjar} from "react-hotjar";
import {Environment} from "../../utils/environment";
import {App} from "../../utils/app";
import {useSelector} from "react-redux";
import {selectUser} from "../../store/features/userSlice";

const useHotjarTracking = () => {

    const user$ = useSelector(selectUser)

    useEffect(() => {
        if (Environment.isProduction) {
            hotjar.initialize(2953185, 6);

            if (App.isLoggedIn()) {
                // Identify the user
                hotjar.identify('USER_ID', { userProperty: user$.userName });
            }

            // // Add an event
            // hotjar.event('button-click');
            //
            // // Update SPA state
            // hotjar.stateChange('/my/page');
        }
    }, []);
};

export default useHotjarTracking;
