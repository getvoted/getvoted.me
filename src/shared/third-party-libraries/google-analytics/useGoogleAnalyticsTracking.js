import {useEffect, useState} from "react";
import {useLocation} from "react-router-dom";
import GA4React from "ga-4-react";
import {Environment} from "../../utils/environment";
import {useSelector} from "react-redux";
import {selectUser} from "../../store/features/userSlice";
import {App} from "../../utils/app";

const useGoogleAnalyticsTracking = () => {
    const location = useLocation();
    const [initialized, setInitialized] = useState(false);
    const [trackingObject, setTrackingObject] = useState({});

    const ga4react = new GA4React("G-6BZMJ0K86F");

    const user$ = useSelector(selectUser)

    useEffect(() => {
        if (Environment.isProduction) {

            try {
                setTimeout(_ => {
                    ga4react.initialize().then(tracker => {
                        setTrackingObject(tracker);

                        setInitialized(true);
                    }).catch(err => console.error(err));
                }, 4000);
            } catch (err) {
                console.error(err);
            }

            // const gaConfig = {
            //     debug: true,
            //     titleCase: false,
            //     gaOptions: {}
            // };
            //
            // if (Auth.isLoggedIn()) {
            //     gaConfig.gaOptions['userId'] = user$.id;
            // }
            //
            // ReactGA.initialize("G-6BZMJ0K86F", gaConfig);

        }
    }, []);

    useEffect(() => {
        if (initialized) {
            trackingObject.pageview(location.pathname + location.search);
        }
    }, [initialized, location]);
};

export default useGoogleAnalyticsTracking;
