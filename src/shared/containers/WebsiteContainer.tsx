import {Outlet} from "react-router-dom";
import {useEffect, useState} from "react";
import {Interceptor} from "../utils/Interceptors";
import {useAppDispatch} from "../store/hooks";
import {getUserAsync} from "../store/features/userSlice";
import {Navbar} from "../components/layout/Navbar";
import {App} from "../utils/app";

export default function WebsiteContainer() {
    const dispatch = useAppDispatch()

    const [, setLoading] = useState(true);
    const [, setHasError] = useState(false);

    useEffect(() => {
        Interceptor(dispatch);

        if (App.isLoggedIn()) getUser();
    }, []);

    const getUser = () => {
        setLoading(true)
        setHasError(false)

        dispatch(getUserAsync())
            .unwrap()
            .catch(() => setHasError(true))
            .finally(() => setLoading(false));
    };

    return (
        <main>
            <main className="app-container">
                <Navbar />
                <div className="main-container">
                    <Outlet/>
                </div>
            </main>
        </main>
    );
}

