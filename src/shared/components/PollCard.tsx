import {Link} from "react-router-dom";
import {PollInterface} from "../store/features/poll/pollSlice";

export const PollCard = (props: { poll: PollInterface }) => {
    return <Link to={`/${props.poll.user?.userName}/poll/${props.poll.link}`}>
        <div className="card card-shadowed">
            <div className="card-body">
                <h4>{props.poll.name}</h4>

                <div className="text-muted mt-3">
                    <span>
                        <i className="lni lni-checkmark"/> {props.poll.totalNoOfEntries} Vote{props.poll.totalNoOfEntries === 1 ? "" : "s"}
                    </span>
                    <span className="ml-4">
                        <i className="lni lni-timer"/> {props.poll.status}
                    </span>
                </div>
            </div>
        </div>
    </Link>
}
