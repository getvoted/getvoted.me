import {Link} from "react-router-dom";
import {ContestInterface} from "../store/features/contest/contestSlice";

export const ContestCard = (props: { contest: ContestInterface }) => {
    return <Link to={`/${props.contest.user?.userName}/${props.contest.link}`}>
        <div className="card card-shadowed">
            <div className="card-body">
                <h4>{props.contest.name}</h4>

                <div className="text-muted mt-3">
                   <span>
                        <i className="lni lni-checkmark"/> {props.contest.totalNoOfVotes} Vote{props.contest.totalNoOfVotes === 1 ? '' : 's'}
                    </span>
                    <span className="ml-4">
                        <i className="lni lni-timer"/> {props.contest.status}
                    </span>
                    <span className="ml-4">
                        <i className="lni lni-money-envelope"/> {props.contest.type === 'PayPerVote' ? `₦${props.contest?.amount} Per Vote` : 'Free Voting'}
                    </span>
                </div>
            </div>
        </div>
    </Link>
}
