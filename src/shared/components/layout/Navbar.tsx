import GetVotedLogo from '../../../assets/images/getvoted-logo.svg'
import ProfilePicturePlaceholder from '../../../assets/images/pp-placeholder.svg'
import {Link, NavLink} from "react-router-dom";
import {App} from "../../utils/app";
import classNames from "classnames";
import {Button} from "../form/Button";
import {Modal} from "../ui/Modal";
import {useState} from "react";
import {Icon} from "../ui/icon";

export const Navbar = () => {

    const isLoggedIn = App.isLoggedIn();

    const logout = () => App.logout()

    const [isCreateModalOpen, setCreateModalOpen] = useState(false)

    const createModal = {
        header: <><h3 className="title font-weight-normal">Create New...</h3></>,
        body: <>
                <div className="my-3">
                    <Link to={'/create/unique-contest'} className="createModal_card" onClick={() => setCreateModalOpen(false)}>
                        <div className="description">
                            <span className="icon-circle icon-container--primary">
                                <span className="lni">
                                    <Icon name='free-events' viewBox="0 0 36 36" />
                                </span>
                            </span>
                            <span className="ml-2 font-weight-normal">Free Contest</span>
                        </div>
                        <div className="link">
                            <Button loading={false} variant='light-primary' disabled={false} withIcon="open-in-tab" btnClass="p-0" body={''} size='md' onClick={() => {}}/>
                        </div>
                    </Link>
                    <Link to={'/create/paid-contest'} className="createModal_card" onClick={() => setCreateModalOpen(false)}>
                        <div className="description">
                            <span className="icon-circle icon-container--yellow">
                                <span className="lni">
                                    <Icon name='money'/>
                                </span>
                            </span>
                            <span className="ml-2 font-weight-normal">Paid Contest</span>
                        </div>
                        <div className="link">
                            <Button loading={false} variant='light-primary' disabled={false} withIcon="open-in-tab" btnClass="p-0" body={''} size='md' onClick={() => {}}/>
                        </div>
                    </Link>
                    <Link to={'/create/poll'} className="createModal_card" onClick={() => setCreateModalOpen(false)}>
                        <div className="description">
                            <span className="icon-circle icon-container--danger">
                                <span className="lni">
                                    <Icon name='analytics' viewBox="0 0 36 36" />
                                </span>
                            </span>
                            <span className="ml-2 font-weight-normal">Poll</span>
                        </div>
                        <div className="link">
                            <Button loading={false} variant='light-primary' disabled={false} withIcon="open-in-tab" btnClass="p-0" body={''} size='md' onClick={() => {}}/>
                        </div>
                    </Link>
                </div>
            </>
    }

    return (
        <header id="header" className="page-header box-shadow animate fadeInDown sticky">
            <div className="navbar navbar-expand-lg">
                <div className="container">

                    <div className="navbar-brand">
                        <Link to={'/'}>
                            <section className="hidden-folded d-inline">
                                <img src={GetVotedLogo} alt="Logo"/>
                            </section>
                        </Link>
                    </div>

                    <div className="collapse navbar-collapse order-2 order-lg-1 ml-sm-0 ml-md-1" id="navbarToggler">
                        <ul className="navbar-nav navbar-expand-lg">
                            {
                                isLoggedIn &&
                                <>
                                    <li className="nav-item">
                                        <NavLink to={'/'} className={({ isActive }) =>
                                            isActive ? classNames('nav-link', { 'nav-active': isActive }) : classNames('nav-link')
                                        } data-toggle="collapse" data-target="#navbarToggler">
                                            <i className="lni lni-grid" /> Dashboard
                                        </NavLink>
                                    </li>

                                    <li className="nav-item">
                                        <NavLink to={'/contests'} className={({ isActive }) =>
                                            isActive ? classNames('nav-link', { 'nav-active': isActive }) : classNames('nav-link')
                                        } data-toggle="collapse" data-target="#navbarToggler">
                                            <i className="lni lni-users-2" /> Contests
                                        </NavLink>
                                    </li>

                                    <li className="nav-item">
                                        <NavLink to={'/polls'} className={({ isActive }) =>
                                            isActive ? classNames('nav-link', { 'nav-active': isActive }) : classNames('nav-link')
                                        } data-toggle="collapse" data-target="#navbarToggler">
                                            <i className="lni lni-bar-chart" /> Polls
                                        </NavLink>
                                    </li>

                                    <li className="nav-item d-lg-none" data-toggle="collapse" data-target="#navbarToggler">
                                        <Button loading={false}
                                                variant='primary'
                                                disabled={false}
                                                withIcon="plus"
                                                body={'Create'}
                                                size='md'

                                                onClick={() => setCreateModalOpen(true)}/>
                                    </li>
                                </>
                            }

                            {
                                !isLoggedIn &&
                                <>
                                    <li className="nav-item">
                                        <Link to={'/about'} className="nav-link" data-toggle="collapse" data-target="#navbarToggler">
                                            About
                                        </Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link to={'/contact'} className="nav-link" data-toggle="collapse" data-target="#navbarToggler">
                                            Contact
                                        </Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link to={'/pricing'} className="nav-link" data-toggle="collapse" data-target="#navbarToggler">
                                            Pricing
                                        </Link>
                                    </li>
                                    <li className="nav-item d-lg-none">
                                        <Link to={'/auth/login'}>
                                            <Button loading={false} variant='light-primary' disabled={false} body={'Log in'} size='md'/>
                                        </Link>
                                        <Link to={'/auth/register'} className="ml-md-2">
                                            <Button loading={false} variant='primary' btnClass="mt-2" disabled={false} body={'Sign up'} size='md'/>
                                        </Link>
                                    </li>
                                </>
                            }
                        </ul>
                    </div>

                    <ul className="nav navbar-menu order-1 order-lg-2">
                        {isLoggedIn &&
                            <>
                                <li className="nav-item dropdown">
                                    <a data-toggle="dropdown"
                                       className="nav-link d-flex align-items-center py-0 px-lg-0 px-2 text-color">
                                        <section className="avatar w-36">
                                            <img src={ProfilePicturePlaceholder} alt="ProfilePicturePlaceholder"/>
                                        </section>
                                    </a>

                                    <div className="dropdown-menu dropdown-menu-right w pt-0 mt-3 animate fadeIn">

                                        {/*<a className="dropdown-item" href="/settings" target="_blank">*/}
                                        {/*    <span>Account Settings</span>*/}
                                        {/*</a>*/}

                                        {/*<div className="dropdown-divider"/>*/}

                                        <a className="dropdown-item" onClick={() => logout()}>Log out</a>
                                    </div>
                                </li>

                                <li className="nav-item d-none d-sm-block px-2">
                                    <Button loading={false} variant='primary' disabled={false} withIcon="plus" body={'Create'} size='md' onClick={() => setCreateModalOpen(true)}/>
                                </li>
                            </>
                        }

                        {
                            !isLoggedIn &&
                            <>
                                <li className="nav-item d-none d-sm-block px-2">
                                    <Link to={'/auth/login'}>
                                        <Button loading={false} variant='light-primary' disabled={false} body={'Log in'} size='md' onClick={() => setCreateModalOpen(true)}/>
                                    </Link>
                                </li>
                                <li className="nav-item d-none d-sm-block px-2">
                                    <Link to={'/auth/register'}>
                                        <Button loading={false} variant='primary' disabled={false} body={'Sign up'} size='md' onClick={() => setCreateModalOpen(true)}/>
                                    </Link>
                                </li>
                            </>
                        }

                        <li className="nav-item d-lg-none">
                            <a className="d-lg-none i-con-h-a px-1" data-toggle="collapse"
                               data-target="#navbarToggler">
                                <i className="lni lni lni-vertical-line text-muted"/>
                            </a>

                            {/*<a className="nav-link i-con-h-a px-1" data-toggle="modal" data-target="#aside">*/}
                            {/*    <i className="i-con i-con-nav text-muted">*/}
                            {/*        <i></i>*/}
                            {/*    </i>*/}
                            {/*</a>*/}
                        </li>
                    </ul>
                </div>
            </div>
            {/*creat new modal*/}
            <Modal header={createModal.header} body={createModal.body} open={isCreateModalOpen} onClose={() => setCreateModalOpen(false)}/>
        </header>
    );
};
