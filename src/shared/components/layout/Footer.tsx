import {Link} from "react-router-dom";
import {Icon} from "../ui/icon";
import Terms_Of_Use_PDF from './Getvoted-Terms_of_Use.pdf'

export const Footer = () => {
    const links = [
        // {link: '/about', isExternal: false, content: 'About'},
        {link: '/pricing', isExternal: false, content: 'Pricing'},
        // {link: '/contact', isExternal: false, content: 'Contact'},
        {link: '/privacy', isExternal: false, content: 'Privacy Policy'},
        {link: Terms_Of_Use_PDF, isExternal: true, content: 'Terms Of Use'},
        {
            link: 'https://390labs.com',
            isExternal: true,
            content: <>A <b>390labs'</b> Product</>
        },
    ]
    return (
        <>
            <footer className="page-footer">
                <div className="container">
                    <ul className="nav justify-content-center">
                        {
                            links.map((link: any, key) =>
                                <li className="nav-item" key={key}>
                                    {
                                        link.isExternal ?

                                            <a href={link.link}
                                               className="nav-link px-2"
                                               rel="noreferrer"
                                               target="_blank">
                                                {link.content}
                                            </a>

                                            :

                                            <Link to={link.link}
                                                  className="nav-link px-2">
                                                {link.content}
                                            </Link>
                                    }
                                </li>
                            )
                        }
                    </ul>
                </div>
            </footer>
        </>
    )
}
