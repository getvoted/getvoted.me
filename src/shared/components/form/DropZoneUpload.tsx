import {useCallback, useState} from "react";
import {useAppDispatch} from "../../store/hooks";
import {uploadDisplayPictureAsync,} from "../../store/features/contest/contestSlice";
import {useDropzone} from 'react-dropzone'
import {Button} from "./Button";

export const DropZoneUpload = (props: {label: string, setFileUploading: any, setFileUploaded: any}): JSX.Element => {

    const {label, setFileUploading, setFileUploaded} = props;

    const dispatch = useAppDispatch()

    const [uploading, setUploading] = useState(false);
    const [uploaded, setUploaded] = useState(false);
    const [fileUploaded, setFile] = useState<any>({});
    const [, setUploadError] = useState("");

    const onDrop = useCallback(acceptedFiles => {
        for (let i = 0; i < acceptedFiles.length; i++) {
            const formData = new FormData();

            const uniqueTime = new Date().toISOString();

            formData.append('file', acceptedFiles[i]);
            formData.append('path', `voting-event-images-${uniqueTime}`);

            resetImageUpload();

            setUploading(true);
            setFileUploading(true);

            dispatch(uploadDisplayPictureAsync(formData))
                .unwrap()
                .then((res) => {
                    setFile(res)
                    setFileUploaded(res)
                    setUploaded(true)
                })
                .catch((error: any) => setUploadError(error.errors))
                .finally(() => {
                    setUploading(false)
                    setFileUploading(false)
                });
        }
    }, [])

    const {acceptedFiles, fileRejections, getRootProps, getInputProps, isDragActive} = useDropzone({
        onDrop,
        accept: 'image/jpeg, image/jpg, image/png, image/heic, image/heif',
        maxFiles: 1
    })

    const resetImageUpload = () => {
        setUploading(false);
        setUploaded(false);
        setFile("");

        setFileUploading(false);
        setFileUploaded("");
    }

    return (
        <div className="mb-30">
            <label>{label}</label>
            {
                !uploaded && !uploading &&
                <>
                    <div {...getRootProps()}>
                        <div className="dropzone p-a p-4 rounded text-center mb-3">
                            <input type="file" {...getInputProps()} />
                            {
                                isDragActive ?
                                    <p className="fs-14 text-muted mb-0">Drop the image here
                                        ...</p> :
                                    <div>
                                        <h4><i className="lni lni-file-upload"/></h4>
                                        <h5>Upload Cover Photo</h5>
                                        <p className="fs-12 text-muted mb-0">
                                            Drag and drop an image here, or click to choose an
                                            image</p>
                                    </div>
                            }
                        </div>
                    </div>
                </>
            }

            {
                uploaded &&
                <>
                    <div style={{height: '150px', width: '150px'}}>
                        <img src={fileUploaded?.url} alt="File Uploaded"
                             style={{height: '100%', width: '100%', objectFit: 'cover'}}/>
                    </div>

                    <p>
                        <Button loading={false} disabled={false} body="Remove Image" btnClass='button--small mt-3' onClick={resetImageUpload} size='md'/>
                    </p>
                </>
            }

            {
                uploading && acceptedFiles && acceptedFiles.map((file: File) => (
                    <p key={file.name} className="text-primary">
                        <small><i className="lni lni-spinner lni-is-spinning "/> Uploading {file.name}</small>
                    </p>
                ))
            }

            {
                !uploaded && fileRejections && fileRejections.map(({file, errors}) => (
                    <p key={file.name}>
                        {file.name} - {file.size} bytes
                        <ul>
                            {errors.map(e => (
                                <li key={e.code}>{e.message}</li>
                            ))}
                        </ul>
                    </p>
                ))
            }
        </div>
    );
}
