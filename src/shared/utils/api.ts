import axios, {AxiosRequestConfig} from 'axios';
import {
    ForgotPasswordInterface,
    LoginInterface,
    RegisterInterface,
    ResendEmailVerificationLinkInterface, ResetPasswordInterface,
    VerifyEmailInterface
} from "../store/features/authSlice";
import {
    ContestInterface, ContestWithdrawInterface,
    ContestSettingsInterface
} from "../store/features/contest/contestSlice";
import {CategoryInterface} from "../store/features/contest/categorySlice";
import {ContestantInterface} from "../store/features/contest/contestantSlice";
import {
    ContestVoteInterface,
    ContestVotePaidInterface
} from "../store/features/contest/contestVoteSlice";
import {EnvironmentUrls} from "./environment";
import {PollInterface} from "../store/features/poll/pollSlice";
import {PollVoteInterface} from "../store/features/poll/pollVoteSlice";
import {AccountDetailsInterface} from "../store/features/contest/paystackSlice";
import {Tokens} from './tokens';

export const baseAxios = axios.create({
    baseURL: `${EnvironmentUrls.BACKEND_URL}/api/`
});

export const cleanAxios = axios.create({
    baseURL: `${EnvironmentUrls.BACKEND_URL}/api/`
});

baseAxios.interceptors.request.use(function (config: AxiosRequestConfig) {
    if (config.headers && Tokens.getAuthToken()) config.headers.Authorization = "Bearer " + Tokens.getAuthToken();

    return config;
});

export class API {
    static login = (payload: LoginInterface) => cleanAxios.post(`Auth/Login`, payload);
    static refreshToken = (payload: any) => cleanAxios.post(`Auth/RefreshToken`, payload);
    static forgotPassword = (payload: ForgotPasswordInterface) => cleanAxios.post(`Auth/ForgotPassword`, payload);
    static register = (payload: RegisterInterface) => cleanAxios.post(`Auth/Register`, payload);
    static verifyEmail = (payload: VerifyEmailInterface) => cleanAxios.post(`Auth/VerifyEmail`, payload);
    static resendEmailVerificationLink = (payload: ResendEmailVerificationLinkInterface) => cleanAxios.post(`Auth/ResendVerifyEmail`, payload);
    static resetPassword = (payload: ResetPasswordInterface) => cleanAxios.post(`Auth/ResetPassword`, payload);
    static logout = () => cleanAxios.post(`Auth/ResetPassword`, null);

    static getUser = () => baseAxios.get(`/User`);
    static getDashboardSummary = () => baseAxios.get(`/Dashboard/GetSummary`);

    static getMyContests = () => baseAxios.get(`Contest/GetMyContests`);
    static getProfileContests = (userName: string) => baseAxios.get(`Contest/GetProfileContests/${userName}`);
    static getContest = (username: string, contestLink: string) => baseAxios.get(`Contest/GetContest/${username}/${contestLink}`);
    static getContestAnonymously = (username: string, contestLink: string) => baseAxios.get(`Contest/GetContestAnonymously/${username}/${contestLink}`);
    static getContestBankDetails = (username: string, contestLink: string) => baseAxios.get(`Contest/GetContestBankInformation/${username}/${contestLink}`);
    static getContestForCreator = (username: string, contestLink: string) => baseAxios.get(`Contest/GetContestForCreator/${username}/${contestLink}`);
    static createUserContest = (payload: ContestInterface) => baseAxios.post(`Contest/CreateUserContest`, payload);
    static createUniqueContest = (payload: ContestInterface) => baseAxios.post(`Contest/CreateUniqueContest`, payload);
    static createPaidContest = (payload: ContestInterface) => baseAxios.post(`Contest/CreatePaidContest`, payload);
    static updateUserContest = (payload: ContestInterface) => baseAxios.put(`Contest/UpdateUserContest`, payload);
    static updateUniqueContest = (payload: ContestInterface) => baseAxios.put(`Contest/UpdateUniqueContest`, payload);
    static updatePaidContest = (payload: ContestInterface) => baseAxios.put(`Contest/UpdatePaidContest`, payload);
    static updateContestSettings = (payload: ContestSettingsInterface) => baseAxios.put(`Contest/UpdateContestSettings`, payload);
    static deleteContest = (contestId: number) => baseAxios.delete(`Contest/DeleteContest/${contestId}`);
    static startContest = (contestId: number) => baseAxios.post(`Contest/StartContest/${contestId}`);
    static endContest = (contestId: number) => baseAxios.post(`Contest/EndContest/${contestId}`);
    static withdrawPaidContest = (payload: ContestWithdrawInterface) => baseAxios.post(`Contest/RequestWithdraw`, payload);

    static getCategories = (username: string, contestLink: string) => baseAxios.get(`Category/GetCategories/${username}/${contestLink}`);
    static getCategoriesForCreator = (username: string, contestLink: string) => baseAxios.get(`Category/GetCategoriesForCreator/${username}/${contestLink}`);
    static getCategory = (username: string, contestLink: string, categoryLink: string) => baseAxios.get(`Category/GetCategory/${username}/${contestLink}/${categoryLink}`);
    static getCategoryForCreator = (username: string, contestLink: string, categoryLink: string) => baseAxios.get(`Category/GetCategoryForCreator/${username}/${contestLink}/${categoryLink}`);
    static createCategory = (payload: CategoryInterface) => baseAxios.post(`Category/CreateCategory`, payload);
    static updateCategory = (payload: CategoryInterface) => baseAxios.put(`Category/UpdateCategory`, payload);
    static deleteCategory = (eventId: number, categoryId: number) => baseAxios.delete(`Category/DeleteCategory/${eventId}/${categoryId}`);

    static getContestants = (username: string, contestLink: string, categoryLink: string) => baseAxios.get(`Contestant/GetContestants/${username}/${contestLink}/${categoryLink}`);
    static getContestantsForCreator = (username: string, contestLink: string, categoryLink: string) => baseAxios.get(`Contestant/GetContestantsForCreator/${username}/${contestLink}/${categoryLink}`);
    static getContestant = (username: string, contestLink: string, categoryLink: string, contestantLink: string) => baseAxios.get(`Contestant/GetContestant/${username}/${contestLink}/${categoryLink}/${contestantLink}`);
    static getContestantForCreator = (username: string, contestLink: string, categoryLink: string, contestantLink: string) => baseAxios.get(`Contestant/GetContestantForCreator/${username}/${contestLink}/${categoryLink}/${contestantLink}`);
    static createContestant = (payload: ContestantInterface) => baseAxios.post(`Contestant/CreateContestant`, payload);
    static updateContestant = (payload: ContestantInterface) => baseAxios.put(`Contestant/UpdateContestant`, payload);
    static deleteContestant = (eventId: number, contestantId: number) => baseAxios.delete(`Contestant/DeleteContestant/${eventId}/${contestantId}`);

    static uploadDisplayPicture = (payload: any) => baseAxios.post(`File/UploadFile`, payload);

    static getContestVotes = (username: string, contestLink: string) => baseAxios.get(`Vote/GetContestVotesForCreator/${username}/${contestLink}`);
    static getCategoryVotes = (username: string, contestLink: string, categoryLink: string) => baseAxios.get(`Vote/GetCategoryVotes/${username}/${contestLink}/${categoryLink}`);
    static getContestantVotes = (username: string, contestLink: string, categoryLink: string, contestantLink: string) => baseAxios.get(`Vote/GetContestantVotes/${username}/${contestLink}/${categoryLink}/${contestantLink}`);
    static getMyVoteForContestant = (username: string, contestLink: string, categoryLink: string, contestantLink: string, identifier: string) => baseAxios.get(`Vote/IdentifierContestantVote/${username}/${contestLink}/${categoryLink}/${contestantLink}/${identifier}`);
    static getMyVoteForCategory = (username: string, contestLink: string, categoryLink: string, identifier: string) => baseAxios.get(`Vote/IdentifierCategoryVote/${username}/${contestLink}/${categoryLink}/${identifier}`);
    static getMyVoteForContestantLoggedIn = (username: string, contestLink: string, categoryLink: string, contestantLink: string) => baseAxios.get(`Vote/UserContestantVote/${username}/${contestLink}/${categoryLink}/${contestantLink}`);
    static getMyVoteForCategoryLoggedIn = (username: string, contestLink: string, categoryLink: string) => baseAxios.get(`Vote/UserCategoryVote/${username}/${contestLink}/${categoryLink}`);
    static voteUserContest = (payload: ContestVoteInterface) => baseAxios.post(`Vote/UserVote`, payload);
    static voteUniqueContest = (payload: ContestVoteInterface) => baseAxios.post(`Vote/UniqueVote`, payload);
    static votePaidContest = (payload: ContestVotePaidInterface) => baseAxios.post(`Vote/PaidVote`, payload);

    static getBanks = () => baseAxios.get(`Paystack/GetBanks`);
    static validateAccountNumber = (payload: AccountDetailsInterface) => baseAxios.get(`Paystack/ResolveAccountNumber?accountNumber=${payload.AccountNumber}&bankCode=${payload.BankCode}`);
    static initializeTransaction = (payload: { Amount: Number }) => baseAxios.post(`Paystack/InitializeTransaction`, payload);

    static getMyPolls = () => baseAxios.get(`Poll/GetMyPolls`);
    static getProfilePolls = (userName: string) => baseAxios.get(`Poll/GetProfilePolls/${userName}`);
    static getPoll = (username: string, pollLink: string) => baseAxios.get(`Poll/GetPoll/${username}/${pollLink}`);
    static getPollResults = (username: string, pollLink: string) => baseAxios.get(`Poll/GetPoll/${username}/${pollLink}`);
    static createPoll = (payload: PollInterface) => baseAxios.post(`Poll/CreatePoll`, payload);
    static deletePoll = (pollId: number) => baseAxios.delete(`Poll/DeletePoll/${pollId}`);
    static startPoll = (pollId: number) => baseAxios.post(`Poll/StartPoll/${pollId}`);
    static endPoll = (pollId: number) => baseAxios.post(`Poll/EndPoll/${pollId}`);

    static getPollVotes = (username: string, pollLink: string) => baseAxios.get(`PollEntry/GetPollEntries/${username}/${pollLink}`);
    static getMyVoteForPoll = (username: string, pollLink: string, userIdentifier: string) => baseAxios.get(`PollEntry/UserPollEntry/${username}/${pollLink}/${userIdentifier}`);
    static voteFreePoll = (payload: PollVoteInterface) => baseAxios.post(`PollEntry/VoteWithIpAddress`, payload);
}
