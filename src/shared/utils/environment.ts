export class Environment {
    static isDevelopment = process.env.NODE_ENV === 'development';
    static isProduction = process.env.NODE_ENV === 'production';
    static PAYSTACK_PUBLIC_KEY = process.env.REACT_APP_PAYSTACK_PUBLIC_KEY
}

export class EnvironmentUrls {
    static FRONTEND_URL = process.env.REACT_APP_FRONTEND_URL
    static BACKEND_URL = process.env.REACT_APP_BACKEND_URL
}
