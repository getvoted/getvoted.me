import {App} from "./app";
import {Tokens} from "./tokens";
import {toast} from "react-toastify";
import {baseAxios} from "./api";
import {refreshTokenAsync} from "../store/features/authSlice";

export const Interceptor = (dispatch) => {

    let isRefreshing = false;

    baseAxios.interceptors['request'].use(function (config) {
        if (App.isLoggedIn()) config.headers['Authorization'] = "Bearer " + Tokens.getAuthToken();

        return config;
    });

    baseAxios.interceptors['response'].use(function (response) {
        response.headers["Access-Control-Allow-Origin"] = "*";
        return response;
    }, function (error) {

        if (error.response) {
            if (error.response.status === 401 || (error.response.status === 419 && !Tokens.getRefreshToken())) {
                toast.error("You will be required to log in again.")

                App.logout(true);
            } else if (error.response.status === 419 && Tokens.getRefreshToken()) {
                toast.error("Renewing Session");

                if (!isRefreshing) {
                    isRefreshing = true;

                    dispatch(refreshTokenAsync())
                        .unwrap()
                        .then(() => window.location.reload())
                        .catch(() => App.logout(true))
                        .finally(() => isRefreshing = false)
                }
            }
        }

        return Promise.reject(error);
    });
};
