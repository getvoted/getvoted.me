export function sluggify (word) {
    if (!word) return "";

    let words = word.replace(/\s+/g, '').split(" ");

    for (let i = 0; i < words.length; i++) {
        let word = words[i];
        words[i] = word.charAt(0).toUpperCase() + word.slice(1);
    }

    return words.join("-");
}

export function sluggifyFirstLetters (word, separateWith = '') {
    if (!word) return "";

    let words = word.split(" ");

    for (let i = 0; i < words.length; i++) {
        let word = words[i];
        words[i] = word.charAt(0).toUpperCase();
    }

    return words.join(separateWith);
}

export function onlyAlphanumeric (word) {
    return word.replace(/\W/g, '');
}

export function deSlug (slug) {
    if (!slug) return "";

    let words = slug.replace(/\s+/g, '').split("_");

    for (let i = 0; i < words.length; i++) {
        let word = words[i];
        words[i] = word.charAt(0).toUpperCase() + word.slice(1);
    }

    return words.join(" ");
}
