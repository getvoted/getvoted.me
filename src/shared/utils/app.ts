import {Tokens} from "./tokens";

export class App {

    static setAuthToken(cName: string, cValue: string, exDays: number): void {
        // Tokens.setToken(cName, cValue, exDays, EnvironmentService.isProduction() ? EnvironmentService.get(EnvironmentKey.CookieURLKey) : null)
        Tokens.setToken(cName, cValue, exDays, '')
    }

    static storeAuthTokens(jwt: string, rt?: string) {
        App.setAuthToken(Tokens.getJWT_TOKEN, jwt, 1000);

        if (rt) {
            App.setAuthToken(Tokens.getREFRESH_TOKEN, rt, 1000);
        }
    }

    static clearTokens() {
        App.deleteCookie(Tokens.getREFRESH_TOKEN);
        App.deleteCookie(Tokens.getJWT_TOKEN);
    }

    static setIntendedURL(url: string) {
        App.setAuthToken(Tokens.getAUTH_FROM, url, 3);
    }

    static clearIntendedURL() {
        App.deleteCookie(Tokens.getAUTH_FROM);
    }

    static deleteCookie(name: string) {
        // Tokens.deleteToken(name, '/', EnvironmentService.get(EnvironmentKey.CookieURLKey), EnvironmentService.isProduction());
        Tokens.deleteToken(name, '/', '', false);
    }

    static isLoggedIn(): boolean {
        return !!Tokens.getAuthToken();
    }

    static logout(rememberCurrentLocation = false) {
        App.clearTokens();

        if (rememberCurrentLocation) {
            App.setIntendedURL(window.location.href);
        } else {
            App.clearIntendedURL();
        }

        App.clearTokens();

        localStorage.clear();

        window.location.href = `/auth/login`;
    }
}
