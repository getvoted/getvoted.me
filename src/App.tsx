import React, {useLayoutEffect} from 'react';
import {Routes, Route, useLocation} from 'react-router-dom';
import './App.scss';
import AppContainer from './shared/containers/AppContainer';
import AuthContainer from './shared/containers/AuthContainer';
import {Login} from "./views/auth/Login";
import {Register} from "./views/auth/Register";
import {VerifyEmail} from "./views/auth/VerifyEmail";
import {ForgotPassword} from "./views/auth/ForgotPassword";
import {ResetPassword} from "./views/auth/ResetPassword";
import {LoggedInGuard, LoggedOutGuard} from "./shared/guards/AuthGuard";
import {UserContestForm} from "./views/app/new/UserContestForm";
import {Contest} from "./views/app/contest/Contest";
import {Dashboard} from "./views/app/Dashboard";
import {CategoryForm} from "./views/app/contest/forms/CategoryForm";
import {ContestantForm} from "./views/app/contest/forms/ContestantForm";
import {ContestCategory} from "./views/app/contest/ContestCategory";
import {ContestContestant} from "./views/app/contest/ContestContestant";
import MixedContainer from "./shared/containers/MixedContainer";
import useGoogleAnalyticsTracking from './shared/third-party-libraries/google-analytics/useGoogleAnalyticsTracking';
import useHotjarTracking from "./shared/third-party-libraries/hotjar/useHotjarTracking";
import {Contests} from "./views/app/Contests";
import {Polls} from "./views/app/Polls";
import {CreatePoll} from "./views/app/new/CreatePoll";
import {Poll} from "./views/app/poll/Poll";
import {PaidContestForm} from "./views/app/new/PaidContestForm";
import {ContestResults} from "./views/app/contest/ContestResults";
import {Pricing} from "./views/website/Pricing";
import {Privacy} from "./views/website/Privacy";
import WebsiteContainer from "./shared/containers/WebsiteContainer";
import {About} from "./views/website/About";
import {Contact} from "./views/website/Contact";
import {ContestCreatorResults} from "./views/app/contest/ContestCreatorResults";
import {UniqueContestForm} from "./views/app/new/UniqueContestForm";
import {NotFound} from "./views/NotFound";

function App() {

    const location = useLocation();

    useLayoutEffect(() => {
        document.documentElement.scrollTo(0, 0);
    }, [location.pathname]);

    useGoogleAnalyticsTracking();
    useHotjarTracking();

    return (
        <Routes>
            <Route path="/auth" element={<LoggedOutGuard><AuthContainer/></LoggedOutGuard>}>
                <Route path="login" element={<Login/>}/>
                <Route path="register" element={<Register/>}/>
                <Route path="verify-email/:newUser/:email" element={<VerifyEmail/>}/>
                <Route path="forgot-password" element={<ForgotPassword/>}/>
                <Route path="reset-password/:token" element={<ResetPassword/>}/>
            </Route>
            <Route path="/" element={<LoggedInGuard><AppContainer/></LoggedInGuard>}>
                <Route path=":username/:contestLink/edit-user-contest" element={<UserContestForm/>}/>
                <Route path=":username/:contestLink/edit-unique-contest" element={<UniqueContestForm/>}/>
                <Route path=":username/:contestLink/edit-paid-contest" element={<PaidContestForm/>}/>

                <Route path=":username/:contestLink/results" element={<ContestResults/>}/>
                <Route path=":username/:contestLink/full-results" element={<ContestCreatorResults/>}/>

                <Route path=":username/:contestLink/add-category" element={<CategoryForm/>}/>
                <Route path=":username/:contestLink/:categoryLink/edit" element={<CategoryForm/>}/>

                <Route path=":username/:contestLink/:categoryLink/add-contestant" element={<ContestantForm/>}/>
                <Route path=":username/:contestLink/:categoryLink/:contestantLink/edit" element={<ContestantForm/>}/>

                <Route path="create/user-contest" element={<UserContestForm/>}/>
                <Route path="create/unique-contest" element={<UniqueContestForm/>}/>
                <Route path="create/paid-contest" element={<PaidContestForm/>}/>
                <Route path="create/poll" element={<CreatePoll/>}/>

                <Route path="contests" element={<Contests/>}/>
                <Route path="polls" element={<Polls/>}/>
                <Route index element={<Dashboard/>}/>
            </Route>
            <Route path="/" element={<WebsiteContainer/>}>
                <Route path="/pricing" element={<Pricing/>}/>
                <Route path="/about" element={<About/>}/>
                <Route path="/contact" element={<Contact/>}/>
                <Route path="/privacy" element={<Privacy/>}/>
            </Route>
            <Route path="/" element={<MixedContainer/>}>
                <Route path=":username/:contestLink" element={<Contest/>}/>
                <Route path=":username/:contestLink/:categoryLink" element={<ContestCategory/>}/>
                <Route path=":username/:contestLink/:categoryLink/:contestantLink" element={<ContestContestant/>}/>

                <Route path=":username/poll/:pollLink" element={<Poll/>}/>
            </Route>
            <Route path="*" element={<NotFound/>}/>
        </Routes>
    );
}

export default App;
