import {useEffect, useState} from 'react';
import DashboardHero from '../../assets/images/dashboard-hero.svg'
import NoPollImage from '../../assets/images/no-poll.svg'
import {useAppDispatch} from "../../shared/store/hooks";
import {Loader} from "../../shared/components/loaders/Loader";
import {LoadingError} from "../../shared/components/loaders/LoadingError";
import {useSelector} from "react-redux";
import {Link} from "react-router-dom";
import {getMyPollsAsync, PollInterface, selectPolls} from "../../shared/store/features/poll/pollSlice";
import {Button} from "../../shared/components/form/Button";
import {PollCard} from "../../shared/components/PollCard";

export const Polls = (): JSX.Element => {

    const dispatch = useAppDispatch()

    const polls$ = useSelector(selectPolls)

    const [loading, setLoading] = useState(false);
    const [hasError, setHasError] = useState(false);

    useEffect(() => getMyPolls(), []);

    const getMyPolls = () => {
        setLoading(true)
        setHasError(false)

        dispatch(getMyPollsAsync())
            .unwrap()
            .catch(() => setHasError(true))
            .finally(() => setLoading(false));
    };

    return (
        <div>
            <div className="card card-colored mb-40">
                <div className="card-body">
                    <div className="home-welcome">
                        <div className="text-section">
                            <div>
                                <h2>Your Polls ({polls$?.length})</h2>
                                <p className="mb-0">
                                    Create polls, share link and see results in real-time.
                                </p>
                            </div>
                        </div>

                        <div className="image-section">
                            <img src={DashboardHero} alt="DashboardHero" width="70%"/>
                        </div>
                    </div>
                </div>
            </div>

            {loading && <><Loader display="Polls"/></>}
            {hasError && <><LoadingError display="There was a problem loading the polls!"/></>}

            {(!loading && !hasError) &&
                <>
                    <div className="row">
                        {polls$.map((poll: PollInterface) =>
                            <div className="col-md-6" key={poll.id}>
                                <PollCard poll={poll}/>
                            </div>
                        )}

                        {
                            polls$.length === 0 &&
                            <>
                                <div className="col-md-12">
                                    <div className="mt-40 text-center">
                                        <img src={NoPollImage} alt="No Polls"/>
                                        <h3 className="text-faint mt-20">You Have No Poll Yet</h3>

                                        <p className="mt-30">
                                            <Link to={'/create/poll'}>
                                                <Button loading={false} variant='primary' disabled={false}
                                                        withIcon="plus" body={'Create Poll'} size='md'/>
                                            </Link>
                                        </p>
                                    </div>
                                </div>
                            </>
                        }
                    </div>
                </>
            }
        </div>
    );
}
