import {FormEvent, useEffect, useState} from "react";
import {useLocation, useNavigate, useParams} from "react-router-dom";
import {Form, Formik, FormikHelpers} from "formik";
import * as Yup from "yup";
import {useAppDispatch} from "../../../shared/store/hooks";
import {FormValidationError} from "../../../shared/components/form/FormValidationError";
import {TextInput} from "../../../shared/components/form/TextInput";
import {TextArea} from "../../../shared/components/form/TextArea";
import {
    createUserContestAsync, getContestForCreatorAsync,
    selectContest, updateUserContestAsync,
    ContestInterface, contestInitialState, clearStateContest
} from "../../../shared/store/features/contest/contestSlice";
import {useSelector} from "react-redux";
import {TextVanityURLInput} from "../../../shared/components/form/TextVanityURLInput";
import {onlyAlphanumeric, sluggifyFirstLetters} from "../../../shared/utils/sluggify";
import {EnvironmentUrls} from "../../../shared/utils/environment";
import {selectUser} from "../../../shared/store/features/userSlice";
import {DropZoneUpload} from "../../../shared/components/form/DropZoneUpload";

export const UserContestForm = (): JSX.Element => {

    const dispatch = useAppDispatch()
    const navigate = useNavigate();
    const location = useLocation();

    const user$ = useSelector(selectUser)
    const contest$ = useSelector(selectContest)

    const {username, contestLink} = useParams();
    const [, setLoading] = useState(false);
    const [, setHasError] = useState(false);
    const [formInitialValues, setFormInitialValues] = useState(contestInitialState.contest);
    const [formError, setFormError] = useState("");
    const [fileUploading, setFileUploading] = useState(false);
    const [fileUploaded, setFileUploaded] = useState<any>({});

    useEffect(() => getContest(), [username, contestLink]);

    useEffect(() => {
        dispatch(clearStateContest())
    }, [location]);

    useEffect(() => setFormInitialValues(contest$), [contest$]);

    const getContest = () => {
        if (username && contestLink) {
            setLoading(true)
            setHasError(false)

            dispatch(getContestForCreatorAsync({username, contestLink}))
                .unwrap()
                .then(() => setFormInitialValues(contest$))
                .catch(() => setHasError(true))
                .finally(() => setLoading(false));
        }
    };

    const formValidation = () => Yup.object({
        name: Yup.string().required('Required').matches(
            /^[a-z0-9]+$/i,
            "Only letters and numbers allowed"
        ),
        description: Yup.string().required('Required'),
        link: Yup.string().required('Required').matches(
            /^[a-z0-9]+$/i,
            "Only letters and numbers allowed"
        ),
    });

    const persistContest = (formValues: ContestInterface, actions: FormikHelpers<ContestInterface>) => {

        setFormError("")

        actions.setSubmitting(true);

        const payload: ContestInterface = {
            name: formValues.name,
            description: formValues.description,
            link: formValues.link,
        };

        if (Object.keys(fileUploaded).length) {
            payload.displayPicture = fileUploaded;
        }

        if (username && contestLink) {
            payload.id = contest$.id;
        }

        dispatch(contestLink ? updateUserContestAsync(payload) : createUserContestAsync(payload))
            .unwrap()
            .then((contest: ContestInterface) => navigate(`/${user$?.userName}/${contest.link}`, {replace: true}))
            .catch((error: any) => setFormError(error.errors))
            .finally(() => actions.setSubmitting(false));
    };

    const onEventNameChange = (e: any, setFieldValue: (field: string, value: any, shouldValidate?: boolean) => void) => {
        setFieldValue('link', onlyAlphanumeric(sluggifyFirstLetters(e.target.value)));
    }

    return (
        <div className="row">
            <div className="col-md-6 col-12 offset-md-3 offset-lg-3">

                <div className="mb-5">
                    <h2>{contestLink ? 'Edit' : 'Create'} Free Contest</h2>
                    <p className="small text-faint-1">A contest allows you add contestants and allow people vote
                        for
                        them.</p>
                </div>

                <Formik
                    initialValues={formInitialValues}
                    validationSchema={formValidation}
                    enableReinitialize
                    onSubmit={(formValues: ContestInterface, actions: FormikHelpers<ContestInterface>) => persistContest(formValues, actions)}>
                    {({isSubmitting, setFieldValue}) => (
                        <Form>
                            <FormValidationError formError={formError}/>

                            <TextInput
                                label="Contest name"
                                name="name"
                                type="text"
                                placeholder="Enter Contest name"
                                onKeyUp={(e: FormEvent) => onEventNameChange(e, setFieldValue)}
                            />

                            <TextVanityURLInput
                                label="Custom Link"
                                name="link"
                                url={`${EnvironmentUrls.FRONTEND_URL}/${user$.userName}/`}
                                placeholder="Contest link"
                            />

                            <TextArea
                                label="About Contest"
                                name="description"
                                rows={3}
                                placeholder="Tell your voters about this Contest"
                            />

                            {
                                contestLink &&
                                <DropZoneUpload label="Display Picture"
                                                setFileUploading={setFileUploading}
                                                setFileUploaded={setFileUploaded}
                                />
                            }

                            <div className="text-center">
                                <button type="submit"
                                        className="button button--secondary button--flat"
                                        disabled={isSubmitting || fileUploading}>
                                    {isSubmitting ? 'Working on it...' : contestLink ? 'Save Changes' : 'Create Contest'}
                                </button>
                            </div>
                        </Form>
                    )}
                </Formik>
            </div>
        </div>
    );
}
