import {FormEvent, useEffect, useState} from "react";
import {useLocation, useNavigate, useParams} from "react-router-dom";
import {Form, Formik, FormikHelpers} from "formik";
import * as Yup from "yup";
import {useAppDispatch} from "../../../shared/store/hooks";
import {FormValidationError} from "../../../shared/components/form/FormValidationError";
import {TextInput} from "../../../shared/components/form/TextInput";
import {TextArea} from "../../../shared/components/form/TextArea";
import {
    createPaidContestAsync, contestInitialState, getContestForCreatorAsync,
    selectContest, updatePaidContestAsync,
    ContestInterface, clearStateContest
} from "../../../shared/store/features/contest/contestSlice";
import {useSelector} from "react-redux";
import {TextVanityURLInput} from "../../../shared/components/form/TextVanityURLInput";
import {onlyAlphanumeric, sluggifyFirstLetters} from "../../../shared/utils/sluggify";
import {EnvironmentUrls} from "../../../shared/utils/environment";
import {selectUser} from "../../../shared/store/features/userSlice";
import {DropZoneUpload} from "../../../shared/components/form/DropZoneUpload";
import {CurrencyInput} from "../../../shared/components/form/CurrencyInput";
import {Select} from "../../../shared/components/form/Select";
import {
    getBanksAsync,
    selectBanks,
    validateAccountAsync
} from "../../../shared/store/features/contest/paystackSlice";
import {Loader} from "../../../shared/components/loaders/Loader";
import {LoadingError} from "../../../shared/components/loaders/LoadingError";
import {Button} from "../../../shared/components/form/Button";

export const PaidContestForm = (): JSX.Element => {

    const dispatch = useAppDispatch()
    const navigate = useNavigate();
    const location = useLocation();
    const {username, contestLink} = useParams();

    const user$ = useSelector(selectUser)
    const banks$ = useSelector(selectBanks)
    const contest$ = useSelector(selectContest)

    const [loading, setLoading] = useState(false);
    const [validationLoading, setValidationLoading] = useState(false);
    const [hasError, setHasError] = useState(false);
    const [isChecked, setChecked] = useState(false);
    const [formInitialValues, setFormInitialValues] = useState(contestInitialState.contest);
    const [formError, setFormError] = useState("");
    const [fileUploading, setFileUploading] = useState(false);
    const [fileUploaded, setFileUploaded] = useState<any>({});
    const [hasValidatedAccount, setHasValidatedAccount] = useState(false)
    const [listOfBanks, setListOfBanks] = useState<any>([])

    useEffect(() => {
        dispatch(clearStateContest())
    }, [location]);

    useEffect(() => {
        getBanks();
    }, []);

    useEffect(() => {
        setListOfBanks(banks$.filter((e:any) => e.code !== ""))
    }, [banks$]);

    useEffect(() => getContest(), [username, contestLink]);

    useEffect(() => {
        setFormInitialValues(contest$);
    }, [contest$]);

    const getContest = () => {
        if (username && contestLink) {
            setLoading(true)
            setHasError(false)

            dispatch(getContestForCreatorAsync({username, contestLink}))
                .unwrap()
                .then(() => setFormInitialValues(contest$))
                .catch(() => setHasError(true))
                .finally(() => setLoading(false));
        }
    };

    const getBanks = () => {
        setLoading(true)
        setHasError(false)
        dispatch(getBanksAsync())
            .unwrap()
            .catch(() => setHasError(true))
            .finally(() => setLoading(false));
    };

    const formValidation = () => Yup.object({
        name: Yup.string().required('Required'),
        description: Yup.string().required('Required'),
        link: Yup.string().required('Required'),
        amount: Yup.string().required('Required'),
        bank: Yup.string().required('Required'),
        bankCode: Yup.string().required('Required'),
        accountName: Yup.string().required('Required'),
        accountNumber: Yup.number().required('Required'),
    });

    const validateAccount = (values: ContestInterface, setFieldValue: (field: string, value: any, shouldValidate?: boolean) => void) => {
        setFieldValue('accountName', '')
        if (values.accountNumber && values.bankCode && !isChecked) {
            setHasValidatedAccount(false)
            setValidationLoading(true)
            dispatch(validateAccountAsync({AccountNumber: values.accountNumber, BankCode: values.bankCode}))
                .unwrap()
                .then((response: any) => {
                    setValidationLoading(false)
                    setChecked(true)
                    setFieldValue('accountName', response.accountName);
                    setHasValidatedAccount(true)
                })
                .catch((error: any) => {
                    setValidationLoading(false)
                    setFormError(error.errors)
                })
        } else {
            setHasValidatedAccount(false)
            setChecked(false)
        }

    }

    const persistContest = (formValues: ContestInterface, actions: FormikHelpers<ContestInterface>) => {

        let finalAmount = formValues.amount!;

        setFormError("")
        actions.setSubmitting(true);

        const payload: ContestInterface = {
            name: formValues.name,
            description: formValues.description,
            link: formValues.link,
            amount: finalAmount.toString().replace(/[^\d.-]/g, ''),
            bank: formValues.bank,
            bankCode: formValues.bankCode,
            accountName: formValues.accountName,
            accountNumber: formValues.accountNumber,
        };

        if (Object.keys(fileUploaded).length) {
            payload.displayPicture = fileUploaded;
        }

        if (username && contestLink) {
            payload.id = contest$.id;
        }

        dispatch(contestLink ? updatePaidContestAsync(payload) : createPaidContestAsync(payload))
            .unwrap()
            .then((contest: ContestInterface) => navigate(`/${user$?.userName}/${contest.link}`, {replace: true}))
            .catch((error: any) => setFormError(error.errors))
            .finally(() => actions.setSubmitting(false));
    };

    const unSetAccountName = (setFieldValue: (field: string, value: any, shouldValidate?: boolean) => void) => {
        setChecked(false)
        setHasValidatedAccount(false)
        setFieldValue('accountName', '');
    }

    const onEventNameChange = (e: any, setFieldValue: (field: string, value: any, shouldValidate?: boolean) => void) => {
        setFieldValue('link', onlyAlphanumeric(sluggifyFirstLetters(e.target.value)));
    }

    const setBank = (e: any, setFieldValue: (field: string, value: any, shouldValidate?: boolean) => void) => {
        setFieldValue('bankId', e.target.value)
        setFieldValue('bankCode', listOfBanks[e.target.value].code)
        setFieldValue('bank', listOfBanks[e.target.value].name)
        unSetAccountName(setFieldValue)
    }

    return (
        <div>
            {loading && <><Loader display="Contest"/></>}
            {hasError && <><LoadingError display="There was a problem loading the contest!"/></>}

            {(!loading && !hasError) &&
                <div className="row">
                    <div className="col-md-6 col-12 offset-md-3 offset-lg-3">

                        <div className="mb-5">
                            <h2>{contestLink ? 'Edit' : 'Create'} Paid Contest</h2>
                            <p className="small text-muted">A contest allows you add contestants and allow people
                                vote for them.</p>
                        </div>

                        <Formik
                            initialValues={formInitialValues}
                            validationSchema={formValidation}
                            enableReinitialize
                            onSubmit={(formValues: ContestInterface, actions: FormikHelpers<ContestInterface>) => persistContest(formValues, actions)}>
                            {({isSubmitting, setFieldValue, values, isValid, dirty}) => (
                                <Form>
                                    <FormValidationError formError={formError}/>

                                    <TextInput
                                        label="Contest name"
                                        placeholder="Enter Contest name"
                                        name="name"
                                        type="text"
                                        onKeyUp={(e: FormEvent) => onEventNameChange(e, setFieldValue)}
                                    />

                                    <TextVanityURLInput
                                        label="Custom Link"
                                        name="link"
                                        placeholder="Contest link"
                                        url={`${EnvironmentUrls.FRONTEND_URL}/${user$.userName}/`}
                                    />

                                    <TextArea
                                        label="About Contest"
                                        placeholder="Tell your voters about this Contest"
                                        name="description"
                                        rows={3}
                                    />

                                    {
                                        contestLink &&
                                        <DropZoneUpload label="Display Picture"
                                                        setFileUploading={setFileUploading}
                                                        setFileUploaded={setFileUploaded}
                                        />
                                    }

                                    <CurrencyInput
                                        label="Voting fee"
                                        name="amount"
                                    />

                                    <TextInput
                                        label="Account Number"
                                        placeholder="Enter a account number"
                                        name="accountNumber"
                                        onKeyUp={() => unSetAccountName(setFieldValue)}
                                        type="tel"
                                    />

                                    <Select label="Bank Name"
                                            placeholder="Select a bank"
                                            name="bankId"
                                            options={listOfBanks}
                                            onChange={(e: any) => setBank(e, setFieldValue)}
                                            optionkey={null}
                                            optionvalue="name"/>

                                    {
                                        hasValidatedAccount &&
                                        <TextInput
                                            label="Account Name"
                                            placeholder="Enter a account name"
                                            name="accountName"
                                            type="text"
                                            disabled={true}
                                        />
                                    }

                                    <div className="mt-20 mb-30">
                                        <label>
                                            <input type="checkbox" className="mr-10" checked={isChecked}
                                                   disabled={validationLoading}
                                                   onChange={() => validateAccount(values, setFieldValue)}/>

                                            <span>
                                        {validationLoading ? 'Validating...' : 'The bank account details I entered are correct'}
                                    </span>

                                        </label>
                                    </div>

                                    <div className="text-center">
                                        <Button loading={isSubmitting || fileUploading} btnClass="w-100" type='submit'
                                                variant='secondary'
                                                disabled={contestLink ? false : !dirty || !isValid || !hasValidatedAccount}
                                                body={contestLink ? 'Save Changes' : 'Create Contest'} size='md'/>
                                    </div>
                                </Form>
                            )}
                        </Formik>
                    </div>
                </div>
            }
        </div>
    );
}
