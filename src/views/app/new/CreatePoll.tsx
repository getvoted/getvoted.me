import {FormEvent, useEffect, useState} from "react";
import {useLocation, useNavigate} from "react-router-dom";
import {FieldArray, Form, Formik, FormikHelpers} from "formik";
import * as Yup from "yup";
import {useAppDispatch} from "../../../shared/store/hooks";
import {FormValidationError} from "../../../shared/components/form/FormValidationError";
import {TextInput} from "../../../shared/components/form/TextInput";
import {TextArea} from "../../../shared/components/form/TextArea";
import {
    clearStatePoll,
    createPollAsync, pollInitialState,
    PollInterface
} from "../../../shared/store/features/poll/pollSlice";
import {useSelector} from "react-redux";
import {onlyAlphanumeric, sluggifyFirstLetters} from "../../../shared/utils/sluggify";
import {selectUser} from "../../../shared/store/features/userSlice";
import {toast} from "react-toastify";

export const CreatePoll = (): JSX.Element => {

    const dispatch = useAppDispatch()
    const navigate = useNavigate();
    const location = useLocation();

    const user$ = useSelector(selectUser)

    const [formInitialValues,] = useState(pollInitialState.poll);
    const [formError, setFormError] = useState("");

    useEffect(() => {
        dispatch(clearStatePoll())
    }, [location]);

    const formValidation = () => Yup.object({
        name: Yup.string().required('Required'),
        link: Yup.string().required('Required'),
    });

    const persistPoll = (formValues: PollInterface, actions: FormikHelpers<PollInterface>) => {

        const isEmptyPoll = !!formValues.options?.filter(option => option === "").length;

        if (isEmptyPoll) {
            toast.warn("There's an empty poll option");

            actions.setSubmitting(false);
        } else {
            setFormError("")

            actions.setSubmitting(true);

            const payload: PollInterface = {
                name: formValues.name,
                description: formValues.description,
                link: formValues.link,
                options: formValues.options,
            };

            dispatch(createPollAsync(payload))
                .unwrap()
                .then((poll: PollInterface) => navigate(`/${user$?.userName}/poll/${poll.link}`, {replace: true}))
                .catch((error: any) => setFormError(error.errors))
                .finally(() => actions.setSubmitting(false));
        }
    };

    const onEventNameChange = (e: any, setFieldValue: (field: string, value: any, shouldValidate?: boolean) => void) => {
        setFieldValue('link', onlyAlphanumeric(sluggifyFirstLetters(e.target.value)));
    }

    return (
        <div className="row">
            <div className="col-md-6 col-12 offset-md-3 offset-lg-3">

                <div className="mb-5">
                    <h2>Create Poll</h2>
                    <p className="small text-muted">
                        Polls allow you to gather public opinion on any subject matter
                    </p>
                </div>

                <Formik
                    initialValues={formInitialValues}
                    validationSchema={formValidation}
                    enableReinitialize
                    onSubmit={(formValues: PollInterface, actions: FormikHelpers<PollInterface>) => persistPoll(formValues, actions)}>
                    {({isSubmitting, setFieldValue, values}) => (
                        <Form>
                            <FormValidationError formError={formError}/>

                            <TextInput
                                label="Title"
                                name="name"
                                type="text"
                                onKeyUp={(e: FormEvent) => onEventNameChange(e, setFieldValue)}
                                placeholder="Catchy Title"
                            />

                            <TextArea
                                label="Description (optional)"
                                name="description"
                                rows={2}
                                placeholder="What is this poll about?"
                            />

                            <FieldArray
                                name="options"
                                render={arrayHelpers => (
                                    <div>
                                        {
                                            values.options && values.options.length > 0 &&
                                            <>
                                                {
                                                    values.options.map((option, index) => (
                                                        <div key={index} className="grid-space-between-6-1">
                                                            <div>
                                                                <TextInput
                                                                    label={`Option ${index + 1}`}
                                                                    name={`options.${index}`}
                                                                    type="text"
                                                                    placeholder="Type here"
                                                                />
                                                            </div>
                                                            <div>
                                                                <label>&nbsp;</label>
                                                                <button
                                                                    type="button"
                                                                    className="button button--gray d-block button--flat"
                                                                    onClick={() => arrayHelpers.remove(index)}>
                                                                    <i className="lni lni-trash-can"/>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    ))
                                                }
                                            </>
                                        }

                                        <div className="mb-30">
                                            <a className="text-link" onClick={() => arrayHelpers.push("")}>
                                                <i className="lni lni-plus"/> Add another option
                                            </a>
                                        </div>
                                    </div>
                                )}
                            />

                            <div className="text-center">
                                <button type="submit"
                                        className="button button--secondary button--flat"
                                        disabled={isSubmitting}>
                                    {isSubmitting ? 'Creating...' : 'Create Poll'}
                                </button>
                            </div>
                        </Form>
                    )}
                </Formik>
            </div>
        </div>
    );
}
