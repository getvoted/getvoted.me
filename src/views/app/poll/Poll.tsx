import {useEffect, useState} from "react";
import {useAppDispatch} from "../../../shared/store/hooks";
import {
    deletePollAsync,
    endPollAsync,
    getPollAsync,
    selectPoll
} from "../../../shared/store/features/poll/pollSlice";
import {useNavigate, useParams} from "react-router-dom";
import {Loader} from "../../../shared/components/loaders/Loader";
import {LoadingError} from "../../../shared/components/loaders/LoadingError";
import {useSelector} from "react-redux";
import nl2br from "react-nl2br";
import {CopyToClipboard} from "react-copy-to-clipboard";
import {toast} from "react-toastify";
import {EnvironmentUrls} from "../../../shared/utils/environment";
import {selectUser} from "../../../shared/store/features/userSlice";
import {Button} from "../../../shared/components/form/Button";
import {PollVoteOptions} from "./components/PollVoteOptions";
import {
    getMyVoteForPollAsync,
    PollVoteInterface,
    selectMyPollVote,
    voteFreePollAsync,
    getPollVotesFromState, getPollVotesAsync,
} from "../../../shared/store/features/poll/pollVoteSlice";
import {Modal} from "../../../shared/components/ui/Modal";
import SubmittedVoteImg from "../../../assets/images/submitted-vote.svg";
import LoginImage from '../../../assets/images/login-image-1.svg'
import {Icon} from "../../../shared/components/ui/icon";
import Moment from 'react-moment';
import 'moment-timezone';
import {useVisitorData} from "@fingerprintjs/fingerprintjs-pro-react";
import EndPollImage from '../../../assets/images/end-poll.svg'
import PollEndedImage from '../../../assets/images/poll-ended.svg'
import {LoadingNotFound} from "../../../shared/components/loaders/LoadingNotFound";

export const Poll = (): JSX.Element => {

    const dispatch = useAppDispatch()
    const navigate = useNavigate();
    const {username, pollLink} = useParams();

    const user$ = useSelector(selectUser)
    const poll$ = useSelector(selectPoll)
    const myVote$ = useSelector(selectMyPollVote);
    const results$ = useSelector(getPollVotesFromState(pollLink!));

    const [isCreator, setIsCreator] = useState(false);
    const [, setShowResults] = useState(false);
    const [loading, setLoading] = useState(false);
    const [hasError, setHasError] = useState(false);
    const [, setHasVoteError] = useState(false);
    const [notFound, setNotFound] = useState(false);
    const [isEndVotingModalOpen, setEndVotingModalOpen] = useState(false)
    const [ending, setEnding] = useState(false);
    const [, setEndingHasError] = useState(false);
    const [confirmEndPoll, setConfirmEndPoll] = useState(false);
    const [deleting, setDeleting] = useState(false);
    const [, setDeletingHasError] = useState(false);
    const [confirmDeletePoll, setConfirmDeletePoll] = useState(false);
    const [myVoteLoading, setMyVoteLoading] = useState(false);
    const [myVoteHasError, setMyVoteHasError] = useState(false);
    const [voting, setVoting] = useState(false);
    const [voted, setVoted] = useState(false);
    const [selectedOption, setSelectedOption] = useState<string>();

    const {data} = useVisitorData();

    const pollIsUpcoming = poll$.status === 'Upcoming';
    const pollIsInProgress = poll$.status === 'Ongoing';
    const pollHasEnded = poll$.status === 'Ended';

    useEffect(() => {
        if (user$.userName && poll$.user) {
            setIsCreator(poll$?.user?.id === user$.id);
        }
    }, [user$, poll$]);

    useEffect(() => getMyVoteForPoll(), [username, pollLink, data]);

    useEffect(() => {
        getPoll();
        getPollResults();
    }, [username, pollLink]);

    useEffect(() => setShowResults(!!(myVote$ && myVote$.pollOption) || isCreator), [myVote$, isCreator]);

    const getPoll = () => {
        if (username && pollLink) {
            setLoading(true)
            setNotFound(false)
            setHasError(false)

            dispatch(getPollAsync({username, pollLink}))
                .unwrap()
                .catch((error) => {
                    if (error.statusCode === 404) setNotFound(true)
                    if (error.statusCode !== 404) setHasError(true)
                })
                .finally(() => setLoading(false));
        }
    };

    const endPoll = () => {
        if (poll$.id) {
            setEnding(true)
            setEndingHasError(false)

            dispatch(endPollAsync(poll$.id))
                .then(() => setEndVotingModalOpen(true))
                .catch(() => setEndingHasError(true))
                .finally(() => {
                    setConfirmEndPoll(false)
                    setEnding(false)
                });
        }
    };

    const deletePoll = () => {
        if (poll$.id) {
            setDeleting(true)
            setDeletingHasError(false)

            dispatch(deletePollAsync(poll$.id))
                .then(() => {
                    setDeleting(false)
                    setConfirmDeletePoll(false)
                    navigate(`/polls`, {replace: true})
                })
                .catch(() => {
                    setDeleting(false)
                    setDeletingHasError(true)
                })
        }
    };

    const getMyVoteForPoll = () => {
        if (username && pollLink && data?.visitorId) {
            setMyVoteLoading(true)
            setMyVoteHasError(false)

            dispatch(getMyVoteForPollAsync({username, pollLink, userIdentifier: data?.visitorId}))
                .catch(() => setMyVoteHasError(true))
                .finally(() => setMyVoteLoading(false));
        }
    };

    const getPollResults = () => {
        if (username && pollLink) {
            dispatch(getPollVotesAsync({username, pollLink}));
        }
    };

    const vote = () => {
        if (username && poll$.id && selectedOption) {
            setVoting(true)
            setHasVoteError(false)

            if (!data?.visitorId) {
                return false
            }
            const payload: PollVoteInterface = {
                pollId: poll$.id,
                pollOption: selectedOption,
                userIdentifier: data?.visitorId,
            };

            dispatch(voteFreePollAsync(payload))
                .unwrap()
                .then(() => {
                    setVoted(true)
                    setShowResults(true)
                    getMyVoteForPoll();
                    getPollResults();
                })
                .catch(() => setHasVoteError(true))
                .finally(() => setVoting(false));
        }
    };

    const chooseOption = (option: string) => setSelectedOption(option);

    const linkCopied = () => toast.success("Poll Link Copied");

    const votedModalBody = () => {
        return <><img src={SubmittedVoteImg} className="modal__img" alt='warning_img'/><h3 className="title">Your vote
            has been submitted</h3></>
    }

    const votedModalFooter = () => {
        return <>
            <Button loading={false}
                    variant='gray'
                    disabled={false}
                    body="Dismiss"
                    btnClass='w-100'
                    size='md'
                    onClick={() => setVoted(false)}/></>
    }

    const endPollModalBody = () => {
        return <>
            <img src={EndPollImage} className="modal__img" alt='warning_img'/>
            <h3 className="title">End Poll?</h3>
            This action is irreversible. Once voting has been terminated, you cannot accept votes again.
        </>
    }

    const endPollModalFooter = () => {
        return <div className="text-center">
            <Button loading={false}
                    variant='gray'
                    disabled={false}
                    body="Cancel"
                    btnClass='mr-20'
                    size='md'
                    onClick={() => setConfirmEndPoll(false)}/>
            <Button loading={ending}
                    variant='secondary'
                    disabled={ending}
                    body="Confirm"
                    btnClass=''
                    size='md'
                    onClick={endPoll}/>
        </div>
    }

    const endedPollModal = {
        body: <>
            <img src={PollEndedImage} className="modal__img" alt='warning_img'/>
            <h3 className="title">Poll Ended Successfully</h3>
        </>,
        footer: <>
            <div className="d-flex align-items-center justify-content-between">
                <Button loading={false}
                        variant='gray'
                        disabled={false}
                        body={'Dismiss'}
                        onClick={() => setEndVotingModalOpen(false)}
                        btnClass='w-100' size='md'/>
            </div>
        </>
    }

    const deletePollModalBody = () => {
        return <>
            <img src={LoginImage} className="modal__img" alt='warning_img'/>
            <h3 className="title">Delete Poll?</h3>
            Are you sure you want to delete “<strong>{poll$.name}</strong>”
        </>
    }

    const deletePollModalFooter = () => {
        return <div className="text-center">
            <Button loading={false}
                    variant='gray'
                    disabled={false}
                    body="Cancel"
                    btnClass='mr-20'
                    size='md'
                    onClick={() => setConfirmDeletePoll(false)}/>
            <Button loading={ending}
                    variant='secondary'
                    disabled={deleting}
                    body="Confirm"
                    btnClass=''
                    size='md'
                    onClick={deletePoll}/>
        </div>
    }

    return (
        <div>
            {loading && <><Loader display="Poll"/></>}
            {hasError && <><LoadingError display="There was a problem loading the poll!"/></>}
            {notFound && <><LoadingNotFound display="Poll"/></>}

            {
                !loading && !hasError && !notFound && !myVoteLoading && !myVoteHasError && poll$ && results$ &&
                <>
                    <div className="row">
                        <div className="col-md-8">
                            <div className="card">
                                <div className="card-body">

                                    <h2>{poll$.name}</h2>

                                    <p className="mb-4">{nl2br(poll$.description)}</p>

                                    {
                                        poll$.options &&
                                        <PollVoteOptions
                                            isCreator={isCreator}
                                            results$={results$}
                                            myVote$={myVote$}
                                            options={poll$.options}
                                            selectedOption={selectedOption}
                                            chooseOption={(option: string) => chooseOption(option)}
                                        />
                                    }

                                    <div className="vote-buttons-section">
                                        <div className="button-group">
                                            {
                                                !myVote$ && pollIsInProgress &&
                                                <>
                                                    <Button loading={false}
                                                            variant='secondary'
                                                            withIcon="checkmark"
                                                            disabled={voting}
                                                            body={'Vote'}
                                                            onClick={vote}
                                                            size='large'/>
                                                </>
                                            }
                                            {
                                                myVote$ &&
                                                <>
                                                    <Button loading={false}
                                                            variant='gray'
                                                            withIcon="checkmark"
                                                            disabled={true}
                                                            body={'Voted'}
                                                            size='md'/>
                                                </>
                                            }

                                            {
                                                isCreator && pollIsUpcoming &&
                                                <>
                                                    <Button loading={false}
                                                            variant='secondary'
                                                            disabled={false}
                                                            body={'Start Poll'}
                                                            size='md'
                                                            onClick={() => null}/>
                                                </>
                                            }

                                            <CopyToClipboard
                                                text={`${EnvironmentUrls.FRONTEND_URL}/${poll$?.user?.userName}/poll/${poll$.link}`}
                                                onCopy={() => linkCopied()}>
                                                <Button loading={false}
                                                        variant='light-secondary'
                                                        withIcon="link"
                                                        disabled={false}
                                                        body={'Copy Poll Link'}
                                                        size='md'/>
                                            </CopyToClipboard>
                                        </div>
                                        <div className="button-group">
                                            {
                                                isCreator &&
                                                <>
                                                    {!pollIsInProgress &&
                                                        <>
                                                            <Button loading={false}
                                                                    variant='gray'
                                                                    withIcon="trash"
                                                                    btnClass="px-3"
                                                                    disabled={false}
                                                                    body={''}
                                                                    size='md'
                                                                    onClick={() => setConfirmDeletePoll(true)}
                                                            />
                                                        </>
                                                    }

                                                    {pollIsInProgress &&
                                                        <>
                                                            <Button loading={false}
                                                                    variant='danger'
                                                                    disabled={ending}
                                                                    onClick={() => setConfirmEndPoll(true)}
                                                                    body={'End Poll'}
                                                                    size='md'/>
                                                        </>
                                                    }
                                                </>
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="card">
                                <div className="card-body">
                                    <div className="d-flex align-items-center">
                                        <div className="result-icon cl-green mr-3">
                                            <Icon name='checkmark'/>
                                        </div>
                                        <h5 className="mb-0 font-weight-normal">
                                            {results$.length} Total Vote{(results$.length === 1) ? '' : 's'}
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <div className="card">
                                <div className="card-body">
                                    <div className="d-flex align-items-center">
                                        <div className="result-icon cl-red mr-3">
                                            <Icon name='timer'/>
                                        </div>
                                        <h5 className="mb-0 font-weight-normal">
                                            {pollIsUpcoming && "Not Started"}
                                            {pollIsInProgress && "Currently Accepting Votes"}
                                            {pollHasEnded && "Poll Ended"}
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <div className="card">
                                <div className="card-body">
                                    <div className="d-flex align-items-center">
                                        <div className="result-icon cl-blue mr-3">
                                            <Icon name='user'/>
                                        </div>
                                        <h5 className="mb-0 font-weight-normal">
                                            Created by {isCreator ? 'You' : poll$?.user?.userName}
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <div className="card">
                                <div className="card-body">
                                    <div className="d-flex align-items-center">
                                        <div className="result-icon cl-black mr-3">
                                            <Icon name='calendar'/>
                                        </div>
                                        <h5 className="mb-0 font-weight-normal">
                                            <Moment date={poll$?.createdAt} format="D MMMM, YYYY | h:mm A"/>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            }

            <Modal body={votedModalBody()}
                   footer={votedModalFooter()}
                   open={voted}
                   onClose={() => setVoted(false)}/>

            <Modal body={endPollModalBody()}
                   footer={endPollModalFooter()}
                   open={confirmEndPoll}
                   onClose={() => setConfirmEndPoll(false)}/>

            <Modal body={deletePollModalBody()}
                   footer={deletePollModalFooter()}
                   open={confirmDeletePoll}
                   onClose={() => setConfirmDeletePoll(false)}/>

            <Modal body={endedPollModal.body}
                   footer={endedPollModal.footer}
                   open={isEndVotingModalOpen}
                   onClose={() => setEndVotingModalOpen(false)}/>
        </div>
    );
}
