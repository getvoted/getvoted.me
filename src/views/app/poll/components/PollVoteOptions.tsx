import classNames from "classnames";
import {useEffect, useState} from "react";

export const PollVoteOptions = (props: { isCreator: boolean, options: string[], selectedOption?: string, chooseOption: any, results$: any, myVote$: any }) => {

    const {isCreator, options, selectedOption, chooseOption, myVote$, results$} = props;

    const [mappedResults, setMappedResults] = useState<{ [pollOption: string]: number }>();

    const showMoreInfo = isCreator || myVote$?.createdAt;

    useEffect(() => {
        if (options && results$) {
            const mappedResults: { [pollOption: string]: number } = {};

            options.forEach((option: string) => {
                if (!mappedResults[option!])
                    mappedResults[option] = 0;
            })

            for (const result in results$) {
                mappedResults[results$[result].pollOption!] += 1;
            }

            setMappedResults(mappedResults);
        }
    }, [options, results$]);

    const calcPercentage = (number: number) => {
        if (number === 0) return 0;

        const calc = ((number / results$.length) * 100);

        return +calc.toString().split(".")[1] > 0 ? calc.toFixed(2) : calc.toString().split(".")[0];
    }

    return <>
        {
            mappedResults && options.map((option, index) =>
                <div key={index} className="card pointer" onClick={() => !myVote$ ? chooseOption(option) : ''}>
                    <div className={
                        classNames('card-body rounded', {
                                'card-bordered--answer': selectedOption === option || (myVote$?.pollOption === option),
                                'card-bordered--default': selectedOption !== option
                            }
                        )}>
                        <div className="d-flex">
                            {
                                !myVote$?.createdAt &&
                                <div className="mr-4">
                                    <input className="form-check-label form-check-label--secondary"
                                           type="radio"
                                           checked={selectedOption === option}
                                           onChange={() => null}
                                    />
                                </div>
                            }
                            <div className="w-100">
                                <div className="d-flex justify-content-between">
                                    <div>
                                        <h4 className="mb-3">{option}</h4>
                                    </div>

                                    {
                                        showMoreInfo &&
                                        <>
                                            <h4 className={classNames({
                                                'text-success': calcPercentage(mappedResults[option]) > 0,
                                                'text-faint-1': calcPercentage(mappedResults[option]) === 0
                                            })}>
                                                {calcPercentage(mappedResults[option])}%
                                            </h4>
                                        </>
                                    }
                                </div>

                                {
                                    showMoreInfo &&
                                    <>
                                        <div className="progress mb-3">
                                            <div className="progress--secondary"
                                                 style={{width: calcPercentage(mappedResults[option]) + '%'}}/>
                                        </div>

                                        <p>
                                            <span className="dot dot--secondary"/>
                                            {mappedResults[option]} Vote{mappedResults[option] === 1 ? '' : 's'}
                                        </p>
                                    </>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
    </>;
};
