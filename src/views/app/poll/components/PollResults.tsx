import {useEffect, useState} from "react";
import {useAppDispatch} from "../../../../shared/store/hooks";
import {getPollVotesAsync, getPollVotesFromState} from "../../../../shared/store/features/poll/pollVoteSlice";
import {Loader} from "../../../../shared/components/loaders/Loader";
import {LoadingError} from "../../../../shared/components/loaders/LoadingError";
import {useSelector} from "react-redux";

export const PollResults = (props: { options: string[], username: string, pollLink: string }) => {

    const {options, username, pollLink} = props;

    const dispatch = useAppDispatch()

    const results$ = useSelector(getPollVotesFromState(pollLink!));

    const [mappedResults, setMappedResults] = useState<{ [pollOption: string]: number }>();
    const [loading, setLoading] = useState(false);
    const [hasError, setHasError] = useState(false);

    useEffect(() => {
        if (options && results$) {
            const mappedResults: { [pollOption: string]: number } = {};

            options.forEach((option: string) => {
                if (!mappedResults[option!])
                    mappedResults[option] = 0;
            })

            for (const result in results$) {
                mappedResults[results$[result].pollOption!] += 1;
            }

            setMappedResults(mappedResults);
        }
    }, [options, results$]);

    useEffect(() => {
        if (username && pollLink) {
            getPollResults();
        }
    }, [username, pollLink]);

    const getPollResults = () => {
        if (username && pollLink) {
            setLoading(true)
            setHasError(false)

            dispatch(getPollVotesAsync({username, pollLink}))
                .catch(() => setHasError(true))
                .finally(() => setLoading(false));
        }
    };

    const calcPercentage = (number: number) => {
        if (number === 0) return 0;

        return (number / results$.length) * 100;
    }

    return <>
        {loading && <><Loader display="Results"/></>}
        {hasError && <><LoadingError display="There was a problem loading the results!"/></>}

        {
            !loading && !hasError &&
            mappedResults && Object.keys(mappedResults).map((option: string, index: number) => {
                return (
                    <div key={option} className="card">
                        <div className="card-body card-bordered--default">
                            <div className="flex-space-between">
                                <div>
                                    <h4>{option}</h4>
                                </div>
                                <h4>
                                    {parseFloat(calcPercentage(mappedResults[option]).toFixed(2))}%
                                </h4>
                            </div>

                            <div className="progress mb-5">
                                <div className="progress--secondary"
                                     style={{width: calcPercentage(mappedResults[option]) + '%'}}/>
                            </div>

                            <p>
                                <span className="dot dot--secondary"/>
                                {mappedResults[option]} Vote{mappedResults[option] === 1 ? '' : 's'}
                            </p>
                        </div>
                    </div>
                )
            })
        }
    </>;
};
