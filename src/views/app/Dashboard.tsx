import {useEffect, useState} from 'react';
import DashboardHero from '../../assets/images/dashboard-hero.svg'
import {useAppDispatch} from "../../shared/store/hooks";
import {Loader} from "../../shared/components/loaders/Loader";
import {LoadingError} from "../../shared/components/loaders/LoadingError";
import {useSelector} from "react-redux";
import {selectUser} from "../../shared/store/features/userSlice";
import {Link} from "react-router-dom";
import {
    getDashboardSummaryAsync,
    selectDashboardEvents,
    selectDashboardNoOfFreeContests,
    selectDashboardNoOfPaidContests,
    selectDashboardNoOfPolls,
    selectDashboardPolls
} from "../../shared/store/features/dashboardSlice";
import {ContestInterface} from "../../shared/store/features/contest/contestSlice";
import {PollInterface} from "../../shared/store/features/poll/pollSlice";
import {Icon} from "../../shared/components/ui/icon"
import {PollCard} from "../../shared/components/PollCard";
import {ContestCard} from "../../shared/components/ContestCard";

export const Dashboard = (): JSX.Element => {

    const dispatch = useAppDispatch()

    const [loading, setLoading] = useState(false);
    const [hasError, setHasError] = useState(false);

    const user$ = useSelector(selectUser)
    const polls$ = useSelector(selectDashboardPolls)
    const contests$ = useSelector(selectDashboardEvents)
    const noOfFreeContests$ = useSelector(selectDashboardNoOfFreeContests)
    const noOfPaidContests$ = useSelector(selectDashboardNoOfPaidContests)
    const noOfPolls$ = useSelector(selectDashboardNoOfPolls)

    useEffect(() => getDashboardSummary(), []);

    const getDashboardSummary = () => {
        setLoading(true)
        setHasError(false)

        dispatch(getDashboardSummaryAsync())
            .unwrap()
            .catch(() => setHasError(true))
            .finally(() => setLoading(false));
    };

    return (
        <div>
            <div className="card card-colored mb-40">
                <div className="card-body">
                    <div className="home-welcome">
                        <div className="text-section">
                            <div>
                                <h2>Hello, {user$.userName}</h2>
                                <p className="mb-0">This is your space. Create contests or polls and see results in
                                    real-time</p>
                            </div>
                        </div>

                        <div className="image-section">
                            <img src={DashboardHero} alt="DashboardHero" width="70%"/>
                        </div>
                    </div>
                </div>
            </div>

            {loading && <><Loader display="Dashboard"/></>}
            {hasError && <><LoadingError display="There was a problem loading your dashboard!"/></>}

            {(!loading && !hasError) &&
                <>
                    <div className="row">
                        <div className="col-md-4">
                            <div className="card">
                                <Link to={'/contests'}>
                                    <div className="card-body d-flex">
                                        <div className="mr-3">
                                        <span className="icon-circle icon-container--primary">
                                            <span className="lni">
                                                <Icon name='free-events' viewBox="0 0 36 36"/>
                                            </span>
                                        </span>
                                        </div>
                                        <div>
                                            <h2 className="mb-0">{noOfFreeContests$}</h2>
                                            <p className="text-primary-deep">Total Free Contest{noOfFreeContests$ === 1 ? '' : 's'}</p>
                                        </div>
                                    </div>
                                </Link>
                                <div className="card-footer text-primary">
                                    <Link to={'/create/unique-contest'}>
                                        <Icon name='plus'/> <span>Create Free Contest</span>
                                    </Link>
                                </div>
                            </div>
                        </div>

                        <div className="col-md-4">
                            <div className="card">
                                <Link to={'/contests'}>
                                    <div className="card-body d-flex">
                                        <div className="mr-3">
                                        <span className="icon-circle icon-container--yellow">
                                            <span className="lni">
                                                <Icon name='money'/>
                                            </span>
                                        </span>
                                        </div>
                                        <div>
                                            <h2 className="mb-0">{noOfPaidContests$}</h2>
                                            <p className="text-primary-deep">Total Paid Contest{noOfPaidContests$ === 1 ? '' : 's'}</p>
                                        </div>
                                    </div>
                                </Link>
                                <div className="card-footer text-muted">
                                    <Link to={'/create/paid-contest'}>
                                        <Icon name='plus'/> <span>Create Paid Contest</span>
                                    </Link>
                                </div>
                            </div>
                        </div>

                        <div className="col-md-4">
                            <div className="card">
                                <Link to={'/polls'}>
                                    <div className="card-body d-flex">
                                        <div className="mr-3">
                                        <span className="icon-circle icon-container--danger">
                                            <span className="lni">
                                                <Icon name='analytics' viewBox="0 0 36 36"/>
                                            </span>
                                        </span>
                                        </div>
                                        <div>
                                            <h2 className="mb-0">{noOfPolls$}</h2>
                                            <p className="text-danger-deep">Total Poll{noOfPolls$ === 1 ? '' : 's'}</p>
                                        </div>
                                    </div>
                                </Link>
                                <div className="card-footer text-primary">
                                    <Link to={'/create/poll'}>
                                        <Icon name='plus'/> <span>Create Poll</span>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h3 className="mt-4 mb-4 text-muted">Your Recent Contests</h3>

                    <div className="row">
                        {contests$ && contests$.map((contest: ContestInterface) =>
                            <div className="col-md-6" key={contest.id}>
                                <ContestCard contest={contest}/>
                            </div>
                        )}

                        {contests$.length === 0 && <p className="text-faint-1">You have no Contest</p>}
                    </div>

                    <h3 className="mt-4 mb-4 text-muted">Your Recent Polls</h3>

                    <div className="row">
                        {polls$ && polls$.map((poll: PollInterface) =>
                            <div className="col-md-6" key={poll.id}>
                                <PollCard poll={poll}/>
                            </div>
                        )}

                        {polls$.length === 0 && <p className="text-faint-1">You have no Poll</p>}
                    </div>
                </>
            }
        </div>
    );
}
