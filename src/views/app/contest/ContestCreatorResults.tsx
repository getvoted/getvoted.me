import {useEffect, useState} from "react";
import {useAppDispatch} from "../../../shared/store/hooks";
import {
    getContestForCreatorAsync,
    selectContest, withdrawContestAsync
} from "../../../shared/store/features/contest/contestSlice";
import {useNavigate, useParams} from "react-router-dom";
import {Loader} from "../../../shared/components/loaders/Loader";
import {LoadingError} from "../../../shared/components/loaders/LoadingError";
import {useSelector} from "react-redux";
import {selectUser} from "../../../shared/store/features/userSlice";
import {getEventVotesFromState, getContestVotesAsync} from "../../../shared/store/features/contest/contestVoteSlice";
import {getCategoriesAsync, selectCategories} from "../../../shared/store/features/contest/categorySlice";
import {ContestResultCategoriesListing} from "./components/ContestResultCategoriesListing";
import {Icon} from "../../../shared/components/ui/icon";
import CurrencyFormat from "react-currency-format";
import {Button} from "../../../shared/components/form/Button";
import WithdrawalImg from "../../../assets/images/withdrawal.svg";
import WithdrawalSuccessImg from "../../../assets/images/withdrawal-success.svg";
import {Modal} from "../../../shared/components/ui/Modal";
import {MiniContestShowcase} from "./components/MiniContestShowcase";
import Moment from "react-moment";


export const ContestCreatorResults = (): JSX.Element => {

    const dispatch = useAppDispatch()
    const navigate = useNavigate();
    const {username, contestLink} = useParams();

    const user$ = useSelector(selectUser)
    const contest$ = useSelector(selectContest)
    const categories$ = useSelector(selectCategories)
    const results$ = useSelector(getEventVotesFromState(contestLink!))

    const [loading, setLoading] = useState(false);
    const [hasError, setHasError] = useState(false);
    const [withdrawing, setWithdrawing] = useState(false)
    const [isWithdrawalModalOpen, setWithdrawalModalOpen] = useState(false)
    const [isWithdrawalSuccessModalOpen, setWithdrawalSuccessModalOpen] = useState(false)

    const eventHasEnded = contest$.status === 'Ended';

    const isUserContest = contest$?.type === 'VoterLogsIn';
    const isUniqueContest = contest$?.type === 'UniqueIdentifier';
    const isPaidContest = contest$?.type === 'PayPerVote';

    useEffect(() => {
        getContest();
        getCategories();
        getVotes();
    }, [username, contestLink]);

    useEffect(() => {
        if (contest$.id && contest$?.user?.id && contest$?.user?.id !== user$.id) {
            navigate('/');
        }
    }, [contest$, user$])

    const getContest = () => {
        if (username && contestLink) {
            setLoading(true)
            setHasError(false)

            dispatch(getContestForCreatorAsync({username, contestLink}))
                .catch(() => setHasError(true))
                .finally(() => setLoading(false));
        }
    };

    const getCategories = () => {
        if (username && contestLink) {
            dispatch(getCategoriesAsync({username, contestLink}))
        }
    }

    const getVotes = () => {
        if (username && contestLink) {
            setLoading(true)
            setHasError(false)

            dispatch(getContestVotesAsync({username, contestLink}))
                .catch(() => setHasError(true))
                .finally(() => setLoading(false));
        }
    };

    const withdrawMoney = () => {
        if (contest$.id) {
            setWithdrawing(true)

            dispatch(withdrawContestAsync({contestId: contest$.id}))
                .unwrap()
                .then(() => {
                    setWithdrawing(false)
                    setWithdrawalModalOpen(false)
                    setWithdrawalSuccessModalOpen(true)
                })
                .finally(() => setWithdrawing(false));
        }
    };

    const withdrawalModal = {
        body: <>
            <img src={WithdrawalImg} className="modal__img" alt='warning_img'/>
            <h3 className="title">Withdraw Earnings</h3>
            <p className="description text-faint-1 font-weight-normal">
                Your earnings of <span className="font-weight-bold">
                    ₦{<CurrencyFormat value={results$?.totalAmountForYou}
                                      displayType={'text'}
                                      thousandSeparator={true}/>}
                </span> will be sent to bank account &nbsp;
                <span
                    className="font-weight-bold">{contest$?.accountNumber} / {contest$?.accountName} / {contest$?.bank}</span>
            </p>
        </>,
        footer: <>
            <div className="d-flex align-items-center justify-content-between">
                <Button loading={false}
                        variant='gray'
                        disabled={false}
                        body={'Cancel'}
                        onClick={() => setWithdrawalModalOpen(false)}
                        btnClass='w-100 mr-4'
                        size='md'/>
                <Button loading={withdrawing}
                        variant='primary'
                        disabled={false}
                        btnClass='w-100'
                        withIcon='withdrawal'
                        onClick={() => withdrawMoney()}
                        body={'Withdraw'}
                        size='md'/>
            </div>
        </>
    }

    const withdrawalSuccessModal = {
        body: <>
            <img src={WithdrawalSuccessImg} className="modal__img" alt='warning_img'/>
            <h3 className="title">Withdrawal Successful!</h3>
            <p className="description">You should receive your earnings within 2-4 business days.</p>
        </>,
        footer: <>
            <Button loading={false}
                    variant='gray'
                    disabled={false}
                    body={'Dismiss'}
                    onClick={() => setWithdrawalSuccessModalOpen(false)}
                    btnClass='w-100 mr-4'
                    size='md'/>
        </>
    }

    return (
        <div>
            {loading && <><Loader display="Results"/></>}
            {hasError && <><LoadingError display="There was a problem loading the results!"/></>}

            {(!loading && !hasError) && contest$.name && results$ &&
                <>
                    <div className="row">
                        <div className="col-md-8">

                            <div className="alert alert-info">
                                <i className="lni lni-bulb"/> Only you can see this information.
                            </div>

                            {
                                contest$?.withdrawRequested && !contest$?.isWithdrawn &&
                                <>
                                    <div className="alert alert-primary">
                                        <i className="lni lni-money-envelope"/> Withdrawal request has been received.
                                        Sent on <Moment date={contest$?.dateWithdrawRequested}
                                                        format="D MMMM, YYYY | h:mm A"/>
                                    </div>
                                </>
                            }

                            {
                                contest$?.isWithdrawn &&
                                <>
                                    <div className="alert alert-success">
                                        <i className="lni lni-money-envelope"/> Withdrawal has been made
                                        to {contest$?.accountName}.
                                    </div>
                                </>
                            }

                            {
                                isPaidContest &&
                                <div className="card">
                                    <div className="card-header">
                                        <div className="row">
                                            <div className="col-md-6">
                                                <div className="mr-3">
                                                    <div className="d-flex align-items-center">
                                                        <div className="result-icon cl-yellow">
                                                            <Icon name='coins'/>
                                                        </div>
                                                        <h2 className="title ml-2">
                                                            ₦{
                                                            <CurrencyFormat
                                                                value={results$.totalAmountForYou}
                                                                displayType={'text'}
                                                                thousandSeparator={true}/>}
                                                            <span className="font-weight-light-bold"> Earned</span>
                                                        </h2>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-6 text-md-end mt-md-0 mt-lg-0 mt-3">
                                                {
                                                    !contest$?.isWithdrawn && !contest$?.withdrawRequested &&
                                                    <>
                                                        <Button loading={false}
                                                                variant='primary'
                                                                withIcon='withdrawal'
                                                                disabled={!eventHasEnded}
                                                                body={'Withdraw'}
                                                                btnClass=""
                                                                size='md'
                                                                onClick={() => setWithdrawalModalOpen(true)}/>
                                                    </>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-body px-4">
                                        <div className="row">
                                            <div className="col-md-6">
                                                <span className="font-weight-light">Total Earnings</span>
                                                <h2>₦{<CurrencyFormat value={results$.totalAmountGotten.toFixed(2)}
                                                                      displayType={'text'}
                                                                      thousandSeparator={true}/>}</h2>
                                            </div>
                                            <div className="col-md-6">
                                                <span className="font-weight-light">GetVoted's Commission</span>
                                                <h2>₦{<CurrencyFormat value={results$.totalAmountForUs.toFixed(2)}
                                                                      displayType={'text'}
                                                                      thousandSeparator={true}/>}</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            }

                            <div className="card">
                                <div className="card-header d-flex">
                                    <div className="mr-3">
                                        <div className="d-flex align-items-center">
                                            <div className="result-icon cl-green">
                                                <i className="lni lni-checkmark"/>
                                            </div>
                                            <h2 className="title ml-2">
                                                {contest$.totalNoOfVotes} Vote{contest$.totalNoOfVotes === 1 ? "" : "s"}
                                            </h2>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-body mx-4">
                                    {
                                        categories$.map(category =>
                                            <ContestResultCategoriesListing category={category}
                                                                            contest={contest$}
                                                                            key={category.id}/>
                                        )
                                    }
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <MiniContestShowcase contest$={contest$}/>

                            {
                                isUserContest &&
                                <>
                                    <div className="card mt-30">
                                        <div className="card-header d-flex">
                                            <div className="mr-2">
                                        <span className="icon-container icon-container--primary">
                                            <i className="lni lni-users-2"/>
                                        </span>
                                            </div>
                                            <div style={{position: "relative", width: "100%"}}>
                                                <h3 className="perfect-center-vertical">
                                                    {contest$.totalNoOfVoters} Voter{contest$.totalNoOfVoters === 1 ? "" : "s"}
                                                </h3>
                                            </div>
                                        </div>
                                        <div className="card-body">
                                            {
                                                results$ && results$.votes?.length > 0 &&
                                                <>
                                                    <ul className="pl-0 mb-0">
                                                        {
                                                            results$.votes.map((result, index) =>
                                                                <li key={result.user?.userName + '' + index}
                                                                    className="flex-space-between card-body--item-list">
                                                                    <span>{result.user?.userName}</span>
                                                                    <span
                                                                        className="small text-muted">{result.user?.email}</span>
                                                                </li>
                                                            )
                                                        }
                                                    </ul>
                                                </>
                                            }

                                            {results$.votes?.length === 0 &&
                                                <p className="text-faint-1">No voters yet</p>}
                                        </div>
                                    </div>
                                </>
                            }
                        </div>
                    </div>


                    <Modal body={withdrawalModal.body}
                           footer={withdrawalModal.footer}
                           open={isWithdrawalModalOpen}
                           onClose={() => setWithdrawalModalOpen(false)}/>

                    <Modal body={withdrawalSuccessModal.body}
                           footer={withdrawalSuccessModal.footer}
                           open={isWithdrawalSuccessModalOpen}
                           onClose={() => setWithdrawalSuccessModalOpen(false)}/>
                </>
            }
        </div>
    );
}
