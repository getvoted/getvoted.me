import {FormEvent, useEffect, useState} from "react";
import {useAppDispatch} from "../../../../shared/store/hooks";
import {getContestAsync, selectContest,} from "../../../../shared/store/features/contest/contestSlice";
import {useLocation, useNavigate, useParams} from "react-router-dom";
import {Loader} from "../../../../shared/components/loaders/Loader";
import {LoadingError} from "../../../../shared/components/loaders/LoadingError";
import {useSelector} from "react-redux";
import {Form, Formik, FormikHelpers} from "formik";
import {FormValidationError} from "../../../../shared/components/form/FormValidationError";
import {TextInput} from "../../../../shared/components/form/TextInput";
import * as Yup from "yup";
import {
    categoryInitialState,
    CategoryInterface, clearStateCategory,
    createCategoryAsync, getCategoryAsync, selectCategory, updateCategoryAsync,
} from "../../../../shared/store/features/contest/categorySlice";
import {TextVanityURLInput} from "../../../../shared/components/form/TextVanityURLInput";
import {EnvironmentUrls} from "../../../../shared/utils/environment";
import {onlyAlphanumeric, sluggify, sluggifyFirstLetters} from "../../../../shared/utils/sluggify";
import {selectUser} from "../../../../shared/store/features/userSlice";

export const CategoryForm = (): JSX.Element => {

    const dispatch = useAppDispatch()
    const navigate = useNavigate();
    const location = useLocation();
    const {username, contestLink} = useParams();
    const {categoryLink} = useParams();

    const user$ = useSelector(selectUser)
    const contest$ = useSelector(selectContest)
    const category$ = useSelector(selectCategory)

    const [, setContestLoading] = useState(false);
    const [, setContestHasError] = useState(false);
    const [categoryLoading, setCategoryLoading] = useState(false);
    const [categoryHasError, setCategoryHasError] = useState(false);
    const [formInitialValues, setFormInitialValues] = useState(categoryInitialState.category);
    const [formError, setFormError] = useState("");

    useEffect(() => {
        dispatch(clearStateCategory())
    }, [location]);

    useEffect(() => getContest(), [username, contestLink]);

    useEffect(() => getCategory(), [categoryLink]);

    useEffect(() => setFormInitialValues(category$), [category$]);

    const getContest = () => {
        if (username && contestLink) {
            setContestLoading(true)
            setContestHasError(false)

            dispatch(getContestAsync({username, contestLink}))
                .unwrap()
                .catch((error: any) => setContestHasError(true))
                .finally(() => setContestLoading(false));
        }
    };

    const getCategory = () => {
        if (categoryLink && username && contestLink) {
            setCategoryLoading(true)
            setCategoryHasError(false)

            dispatch(getCategoryAsync({username, contestLink, categoryLink}))
                .unwrap()
                .catch((error: any) => setCategoryHasError(true))
                .finally(() => setCategoryLoading(false));
        }
    };

    const formValidation = () => Yup.object({
        name: Yup.string().required('Required'),
        link: Yup.string().required('Required'),
    });

    const persistCategory = (formValues: CategoryInterface, actions: FormikHelpers<CategoryInterface>) => {

        setFormError("")

        actions.setSubmitting(true);

        const payload: CategoryInterface = {
            name: formValues.name,
            link: formValues.link,
            contestId: contest$.id,
        };

        if (categoryLink) {
            payload.id = category$.id
        }

        dispatch(categoryLink ? updateCategoryAsync(payload) : createCategoryAsync(payload))
            .unwrap()
            .then((res: CategoryInterface) => {
                if (categoryLink) {
                    navigate(`/${user$?.userName}/${contestLink}/${res.link}`, {replace: true})
                } else {
                    navigate(`/${user$?.userName}/${contestLink}`, {replace: true})
                }
            })
            .catch((error: any) => setFormError(error.errors))
            .finally(() => actions.setSubmitting(false));
    };

    const onEventNameChange = (e: any, setFieldValue: (field: string, value: any, shouldValidate?: boolean) => void) => {
        setFieldValue('link', onlyAlphanumeric(sluggify(e.target.value)));
    }

    return (
        <div>
            {categoryLoading && <><Loader display="Category"/></>}
            {categoryHasError && <><LoadingError display="There was a problem loading the Category!"/></>}

            {(!categoryLoading && !categoryHasError) &&
                <>
                    <div className="row">
                        <div className="col-md-6 col-12 offset-md-3 offset-lg-3">

                            <div className="mb-5">
                                <h2>{categoryLink ? 'Edit' : 'Add'} Category</h2>
                                <p className="small text-muted">{contest$.name}</p>
                            </div>

                            <Formik
                                initialValues={formInitialValues}
                                validationSchema={formValidation}
                                onSubmit={(formValues: CategoryInterface, actions: FormikHelpers<CategoryInterface>) => persistCategory(formValues, actions)}>
                                {({isSubmitting, setFieldValue}) => (
                                    <Form>
                                        <FormValidationError formError={formError}/>

                                        <TextInput
                                            label="Category name"
                                            name="name"
                                            type="text"
                                            placeholder="Enter Category name"
                                            onKeyUp={(e: FormEvent) => onEventNameChange(e, setFieldValue)}
                                        />

                                        <TextVanityURLInput
                                            label="Link"
                                            name="link"
                                            url={`/${contest$?.user?.userName}/${contest$.link}/`}
                                            placeholder="Link"
                                        />

                                        <div className="text-center">
                                            <button type="submit"
                                                    className="button button--secondary button--flat"
                                                    disabled={isSubmitting}>
                                                {isSubmitting ? 'Working on it...' : categoryLink ? 'Edit Category' : 'Add Category'}
                                            </button>
                                        </div>
                                    </Form>
                                )}
                            </Formik>
                        </div>
                    </div>
                </>
            }
        </div>
    );
}
