import {FormEvent, useEffect, useState} from "react";
import {useAppDispatch} from "../../../../shared/store/hooks";
import {getContestAsync, selectContest,} from "../../../../shared/store/features/contest/contestSlice";
import {useLocation, useNavigate, useParams} from "react-router-dom";
import {Loader} from "../../../../shared/components/loaders/Loader";
import {LoadingError} from "../../../../shared/components/loaders/LoadingError";
import {useSelector} from "react-redux";
import {Form, Formik, FormikHelpers} from "formik";
import {FormValidationError} from "../../../../shared/components/form/FormValidationError";
import {TextInput} from "../../../../shared/components/form/TextInput";
import * as Yup from "yup";
import {
    clearStateContestant,
    contestantInitialState,
    ContestantInterface,
    createContestantAsync, getContestantAsync, selectContestant, updateContestantAsync
} from "../../../../shared/store/features/contest/contestantSlice";
import {
    getCategoryAsync,
    selectCategory
} from "../../../../shared/store/features/contest/categorySlice";
import {TextArea} from "../../../../shared/components/form/TextArea";
import {selectUser} from "../../../../shared/store/features/userSlice";
import {TextVanityURLInput} from "../../../../shared/components/form/TextVanityURLInput";
import {EnvironmentUrls} from "../../../../shared/utils/environment";
import {onlyAlphanumeric, sluggify} from "../../../../shared/utils/sluggify";
import {DropZoneUpload} from "../../../../shared/components/form/DropZoneUpload";
import {toast} from "react-toastify";

export const ContestantForm = (): JSX.Element => {

    const dispatch = useAppDispatch()
    const location = useLocation();
    const navigate = useNavigate();
    const {username, contestLink} = useParams();
    const {categoryLink} = useParams();
    const {contestantLink} = useParams();

    const user$ = useSelector(selectUser)
    const contest$ = useSelector(selectContest)
    const category$ = useSelector(selectCategory)
    const contestant$ = useSelector(selectContestant)

    const [, setContestLoading] = useState(false);
    const [, setContestHasError] = useState(false);
    const [, setCategoryLoading] = useState(false);
    const [, setCategoryHasError] = useState(false);
    const [contestantLoading, setContestantLoading] = useState(false);
    const [contestantHasError, setContestantHasError] = useState(false);
    const [formInitialValues, setFormInitialValues] = useState(contestantInitialState.contestant);
    const [formError, setFormError] = useState("");
    const [fileUploading, setFileUploading] = useState(false);
    const [fileUploaded, setFileUploaded] = useState<any>({});

    useEffect(() => {
        dispatch(clearStateContestant())
    }, [location]);

    useEffect(() => getContest(), [username, contestLink]);

    useEffect(() => getCategory(), [categoryLink]);

    useEffect(() => getContestant(), [contestantLink]);

    useEffect(() => setFormInitialValues(contestant$), [contestant$]);

    const getContest = () => {
        if (username && contestLink) {
            setContestLoading(true)
            setContestHasError(false)

            dispatch(getContestAsync({username, contestLink}))
                .unwrap()
                .catch((error: any) => setContestHasError(true))
                .finally(() => setContestLoading(false));
        }
    };

    const getCategory = () => {
        if (categoryLink && username && contestLink) {
            setCategoryLoading(true)
            setCategoryHasError(false)

            dispatch(getCategoryAsync({username, contestLink, categoryLink}))
                .unwrap()
                .catch((error: any) => setCategoryHasError(true))
                .finally(() => setCategoryLoading(false));
        }
    };

    const getContestant = () => {
        if (categoryLink && username && contestLink && contestantLink) {
            setContestantLoading(true)
            setContestantHasError(false)

            dispatch(getContestantAsync({username, contestLink, categoryLink, contestantLink}))
                .unwrap()
                .catch((error: any) => setContestantHasError(true))
                .finally(() => setContestantLoading(false));
        }
    };

    const formValidation = () => Yup.object({
        name: Yup.string().required('Required'),
        link: Yup.string().required('Required'),
    });

    const persistContestant = (formValues: ContestantInterface, actions: FormikHelpers<ContestantInterface>) => {

        setFormError("")

        actions.setSubmitting(true);

        const payload: ContestantInterface = {
            name: formValues.name,
            description: formValues.description,
            link: formValues.link,
            contestId: contest$.id,
            categoryId: category$.id,
        };

        if (Object.keys(fileUploaded).length) {
            payload.displayPicture = fileUploaded;
        }

        if (contestantLink) {
            payload.id = contestant$.id;
        }

        dispatch(contestantLink ? updateContestantAsync(payload) : createContestantAsync(payload))
            .unwrap()
            .then((res: ContestantInterface) => navigate(`/${user$?.userName}/${contestLink}`))
            .catch((error: any) => setFormError(error.errors))
            .finally(() => actions.setSubmitting(false));
    };

    const onEventNameChange = (e: any, setFieldValue: (field: string, value: any, shouldValidate?: boolean) => void) => {
        setFieldValue('link', onlyAlphanumeric(sluggify(e.target.value)));
    }

    return (
        <div>
            {contestantLoading && <><Loader display="Contestant"/></>}
            {contestantHasError && <><LoadingError display="There was a problem loading the contestant!"/></>}

            {(!contestantLoading && !contestantHasError) &&
                <>
                    <div className="row">
                        <div className="col-md-6 col-12 offset-md-3 offset-lg-3">

                            <div className="mb-5">
                                <h2>{contestantLink ? 'Edit' : 'Add'} Contestant</h2>
                                <p className="text-muted">{category$.name}</p>
                            </div>

                            <Formik
                                initialValues={formInitialValues}
                                validationSchema={formValidation}
                                onSubmit={(formValues: ContestantInterface, actions: FormikHelpers<ContestantInterface>) => persistContestant(formValues, actions)}>
                                {({isSubmitting, setFieldValue}) => (
                                    <Form>
                                        <FormValidationError formError={formError}/>

                                        <TextInput
                                            label="Contestant name"
                                            name="name"
                                            type="text"
                                            placeholder="Enter Contestant name"
                                            onKeyUp={(e: FormEvent) => onEventNameChange(e, setFieldValue)}
                                        />

                                        <TextVanityURLInput
                                            label="Link"
                                            name="link"
                                            placeholder="Link"
                                            url={`/${contest$.link}/${category$.link}/`}
                                        />

                                        <TextArea
                                            label="About contestant (optional)"
                                            name="description"
                                            placeholder="Tell your voters about this contestant"
                                            rows={2}
                                        />

                                        <DropZoneUpload label="Display Picture"
                                                        setFileUploading={setFileUploading}
                                                        setFileUploaded={setFileUploaded}
                                        />

                                        <div className="text-center">
                                            <button type="submit"
                                                    className="button button--secondary button--flat"
                                                    disabled={isSubmitting || fileUploading || (!contestantLink && !Object.keys(fileUploaded).length)}>
                                            {isSubmitting ? 'Working on it...' : contestantLink ? 'Save Changes' : 'Add Contestant'}
                                            </button>
                                        </div>
                                    </Form>
                                )}
                            </Formik>
                        </div>
                    </div>
                </>
            }
        </div>
    );
}
