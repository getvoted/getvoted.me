import {useEffect, useState} from "react";
import {useAppDispatch} from "../../../shared/store/hooks";
import {
    endContestAsync,
    getContestAsync,
    selectContest,
    startContestAsync,
    updateContestSettingsAsync, getContestAnonymouslyAsync, deleteContestAsync, clearStateContest,
} from "../../../shared/store/features/contest/contestSlice";
import {Link, useParams, useNavigate, useLocation} from "react-router-dom";
import {Loader} from "../../../shared/components/loaders/Loader";
import {LoadingError} from "../../../shared/components/loaders/LoadingError";
import ImageUploadPlaceholder from "../../../assets/images/image-upload-placeholder.svg";
import {useSelector} from "react-redux";
import {
    clearStateCategories,
    clearStateCategory,
    getCategoriesAsync,
    selectCategories
} from "../../../shared/store/features/contest/categorySlice";
import {ContestCategoriesListing} from "./components/ContestCategoriesListing";
import {NoContestCategoryPlaceholder} from "./components/NoContestCategoryPlaceholder";
import nl2br from "react-nl2br";
import {CopyToClipboard} from "react-copy-to-clipboard";
import {toast} from "react-toastify";
import {EnvironmentUrls} from "../../../shared/utils/environment";
import {selectUser} from "../../../shared/store/features/userSlice";
import {Modal} from "../../../shared/components/ui/Modal";
import StartEventImg from "../../../assets/images/start-event.svg";
import ShareEventImg from "../../../assets/images/share-link.svg";
import DeleteImg from "../../../assets/images/delete.svg";
import {Button} from "../../../shared/components/form/Button";
import {Icon} from "../../../shared/components/ui/icon";
import {Switch} from "../../../shared/components/ui/Switch";
import {App} from "../../../shared/utils/app";
import {clearStateContestant} from "../../../shared/store/features/contest/contestantSlice";
import {LoadingNotFound} from "../../../shared/components/loaders/LoadingNotFound";

export const Contest = (): JSX.Element => {

    const dispatch = useAppDispatch()
    const navigate = useNavigate();
    const {username, contestLink} = useParams();

    const user$ = useSelector(selectUser)
    const contest$ = useSelector(selectContest)
    const categories$ = useSelector(selectCategories)

    const [loading, setLoading] = useState(false);
    const [hasError, setHasError] = useState(false);
    const [notFound, setNotFound] = useState(false);
    const [isStartedModalOpen, setStartedModalOpen] = useState(false)
    const [isSharedModalOpen, setSharedModalOpen] = useState(false)
    const [isDeleteModalOpen, setDeleteModalOpen] = useState(false)
    const [isEventCoverModal, setEventCoverModal] = useState(false)
    const [isEndContestModalOpen, setEndContestModalOpen] = useState(false)
    const [showLiveUpdates, setShowLiveUpdates] = useState(false)
    const [toggling, setToggling] = useState(false)
    const [starting, setStarting] = useState(false);
    const [ending, setEnding] = useState(false);
    const [isCreator, setIsCreator] = useState(false);
    const [deleting, setDeleting] = useState(false);

    const eventIsUpcoming = contest$?.status === 'Upcoming';
    const eventIsInProgress = contest$?.status === 'Ongoing';
    const eventHasEnded = contest$?.status === 'Ended';

    const isUserContest = contest$?.type === 'VoterLogsIn';
    const isPaidContest = contest$?.type === 'PayPerVote';

    useEffect(() => {
        getContest()
        getCategories()

        return () => {
            dispatch(clearStateContest())
            dispatch(clearStateCategory())
            dispatch(clearStateContestant())
            dispatch(clearStateCategories())
        };
    }, [username, contestLink]);

    useEffect(() => {
        setShowLiveUpdates(contest$?.showLiveUpdates?.toLowerCase() === 'everyone');
        setIsCreator(contest$?.user?.id === user$.id);
    }, [contest$, user$])

    const getContest = () => {
        if (username && contestLink) {
            setLoading(true)
            setHasError(false)
            setNotFound(false)

            const actionContest = App.isLoggedIn() ? getContestAsync : getContestAnonymouslyAsync;

            dispatch(actionContest({username, contestLink}))
                .unwrap()
                .catch((error) => {
                    if (error.statusCode === 404) setNotFound(true)
                    if (error.statusCode !== 404) setHasError(true)
                })
                .finally(() => setLoading(false));
        }
    };

    const deleteContest = () => {
        if (contest$.id) {
            setDeleting(true)
            dispatch(deleteContestAsync(contest$.id))
                .unwrap()
                .then(() => {
                    toast.success('Contest Deleted');
                    setDeleting(false)
                    navigate(`/`, { replace: true })
                })
                .catch(() => setDeleting(false));
        }
    }

    const getCategories = () => {
        if (username && contestLink) {
            dispatch(getCategoriesAsync({username, contestLink}))
        }
    }

    const linkCopied = () => toast.success("Contest Link Copied");

    const startContest = () => {
        if (contest$.id) {
            setStarting(true)

            dispatch(startContestAsync(contest$.id))
                .unwrap()
                .then(() => triggerContestModals())
                .finally(() => setStarting(false));
        }
    };

    const endContest = () => {
        if (contest$.id) {
            setEnding(true)

            dispatch(endContestAsync(contest$.id))
                .unwrap()
                .then(() => {
                    setEndContestModalOpen(false)
                    toast.success('Contest Ended');
                })
                .finally(() => setEnding(false));
        }
    };

    const triggerContestModals = () => {
        setStartedModalOpen(false)
        setSharedModalOpen(true)
    }

    const updateLiveUpdatesVisibility = () => {
        setToggling(true)

        let payload: any = {
            id: contest$.id,
            showLiveUpdates: showLiveUpdates ? 1 : 0
        }

        dispatch(updateContestSettingsAsync(payload))
            .unwrap()
            .catch(() => setToggling(false))
            .finally(() => setToggling(false));
    }

    const startedContestModal = {
        body: <>
            <img src={StartEventImg} className="modal__img" alt='warning_img'/>
            <h3 className="title">Start Contest</h3>
            <p className="description text-left">Once Contest starts, you will no longer be able to add categories or
                contestants to this event. </p></>,
        footer: <>
            <div className="d-flex align-items-center justify-content-between">
                <Button loading={false}
                        variant='gray'
                        disabled={false}
                        body={'Cancel'}
                        onClick={() => setStartedModalOpen(false)}
                        btnClass='w-100 mr-4'
                        size='md'/>
                <Button loading={starting}
                        variant='secondary'
                        disabled={false}
                        btnClass='w-100'
                        onClick={() => startContest()}
                        body={'Start Contest'}
                        size='md'/>
            </div>
        </>
    }

    const deleteEventModal = {
        body: <>
            <img src={DeleteImg} className="modal__img" alt='warning_img'/>
            <h3 className="title">Delete Contest</h3>
            <p className="description">
                Are you sure you want to delete this
                Contest <b>“{contest$?.name}”</b> with {categories$.length} categories?
            </p>
        </>,
        footer: <>
            <div className="d-flex align-items-center justify-content-between">
                <Button loading={false} variant='gray'
                        disabled={false}
                        body={'Cancel'}
                        onClick={() => setDeleteModalOpen(false)}
                        btnClass='w-100 mr-4'
                        size='md'/>
                <Button loading={deleting}
                        variant='danger'
                        disabled={false}
                        btnClass='w-100'
                        withIcon='trash'
                        onClick={() => deleteContest()}
                        body={'Delete'}
                        size='md'/></div>
        </>
    }

    const shareContestModal = {
        body: <>
            <img src={ShareEventImg} className="modal__img" alt='warning_img'/>
            <h3 className="title">Contest has started!</h3>
            <p className="description">Share your Contest link to invite voters.</p></>,
        footer: <>
            <CopyToClipboard
                text={`${EnvironmentUrls.FRONTEND_URL}/${contest$?.user?.userName}/${contest$?.link}`}
                onCopy={() => linkCopied()}>
                <Button loading={false}
                        variant='light-secondary'
                        withIcon="link"
                        disabled={false}
                        btnClass='w-100'
                        body={'Copy Contest Link'}
                        size='md'/></CopyToClipboard></>
    }

    const endContestModal = {
        body: <>
            <img src={DeleteImg} className="modal__img" alt='warning_img'/>
            <h3 className="title">Stop Contest</h3>
            <p className="description">This action is irreversible. Once Contest is terminated, you cannot start Contest
                again.</p>
        </>,
        footer: <>
            <div className="d-flex align-items-center justify-content-between">
                <Button loading={false}
                        variant='gray'
                        disabled={false}
                        body={'Cancel'}
                        onClick={() => setEndContestModalOpen(false)}
                        btnClass='w-100 mr-4'
                        size='md'/>
                <Button loading={ending}
                        variant='danger'
                        disabled={false}
                        btnClass='w-100'
                        onClick={() => endContest()}
                        body={'End Contest'}
                        size='md'/></div>
        </>
    }

    const eventCoverModal = {
        body: <><img src={DeleteImg} className="modal__img" alt='warning_img'/><h3 className="title">Add event cover photo</h3><p className="description text-left">You need to add a cover photo to your event before you can start accepting votes</p></>,
        footer: <>
            <div className="d-flex align-items-center justify-content-between"><Button loading={false} variant='gray'
                                                                                       disabled={false} body={'Cancel'}
                                                                                       onClick={() => setEventCoverModal(false)}
                                                                                       btnClass='w-100 mr-4' size='md'/>
                <Button loading={false} variant='secondary' disabled={false} btnClass='w-100'
                        withIcon="plus"
                        onClick={() => {
                            navigate(isUserContest ? 'edit-user-contest' : isPaidContest ? 'edit-paid-contest' : 'edit-unique-contest')
                        }} body={'Add photo'} size='md'/></div>
        </>
    }

    return (
        <div>
            {loading && <><Loader display="Contest"/></>}
            {hasError && <><LoadingError display="There was a problem loading the contest!"/></>}
            {notFound && <><LoadingNotFound display="Contest"/></>}

            {(!loading && !hasError && !notFound) && contest$ &&
                <>
                    <div className="row">
                        <div className="col-md-4">
                            <div className="card">
                                <div className="card-body">
                                    { contest$.displayPicture ?
                                        <div className="voting-image--contestant">
                                            <div className="img-container">
                                                <img src={contest$.displayPicture} alt="Contest"/>
                                            </div>
                                            <div style={{backgroundImage: `url(${contest$.displayPicture})`}}
                                                 className="bg-withBlur">
                                            </div>
                                        </div>
                                        :
                                        <>
                                            <div className="voting-image--event">
                                                <Link
                                                    to={isUserContest ? 'edit-user-contest' : isPaidContest ? 'edit-paid-contest' : 'edit-unique-contest'}>
                                                    <img src={ImageUploadPlaceholder} alt="Contest"/>
                                                </Link>
                                            </div>
                                        </>
                                    }

                                    <h2 className="mt-4">{contest$.name}</h2>

                                    <p>{nl2br(contest$.description)}</p>

                                    <div className="mt-3">
                                        <span>
                                            <Icon name="free-poll"/>
                                            <span className="ml-2">
                                             {isPaidContest ? `₦${contest$?.amount} Per Vote` : 'Free Contest'}
                                            </span>
                                        </span>

                                        <span className="ml-4">
                                            <i className="lni lni-timer"/> {contest$.status}
                                        </span>
                                    </div>

                                    <div className="vote-buttons-section">
                                        <div className="button-group">
                                            {
                                                isCreator &&
                                                <>
                                                    {
                                                        eventIsUpcoming &&
                                                        <>
                                                            <Button loading={false}
                                                                    variant='secondary'
                                                                    disabled={!categories$.length}
                                                                    body={'Start Contest'}
                                                                    btnClass="w-100"
                                                                    size='md'
                                                                    onClick={() => contest$.displayPicture ? setStartedModalOpen(true): setEventCoverModal(true)}/>
                                                        </>
                                                    }

                                                    {
                                                        eventIsInProgress &&
                                                        <>
                                                            <Button loading={false}
                                                                    variant='danger'
                                                                    disabled={false}
                                                                    body={'Stop Contest'}
                                                                    btnClass="w-100"
                                                                    size='md'
                                                                    onClick={() => setEndContestModalOpen(true)}/>
                                                        </>
                                                    }

                                                    {
                                                        eventHasEnded &&
                                                        <>
                                                            <Button loading={false}
                                                                    variant='gray'
                                                                    disabled={true}
                                                                    btnClass="w-100"
                                                                    body={'Contest Ended'}
                                                                    size='md'/>
                                                        </>
                                                    }
                                                    {
                                                        !eventHasEnded &&
                                                        <>
                                                            <CopyToClipboard
                                                                text={`${EnvironmentUrls.FRONTEND_URL}/${contest$?.user?.userName}/${contest$.link}`}
                                                                onCopy={() => linkCopied()}>
                                                                <Button loading={false}
                                                                        variant='light-secondary'
                                                                        withIcon="link"
                                                                        disabled={false}
                                                                        btnClass="w-100"
                                                                        body={'Copy Contest Link'}
                                                                        size='md'/>
                                                            </CopyToClipboard>
                                                        </>
                                                    }
                                                    <Button loading={false}
                                                            variant='gray'
                                                            disabled={false}
                                                            body={'View Results'}
                                                            btnClass="w-100"
                                                            size='md'
                                                            onClick={() => navigate('full-results')}/>
                                                </>
                                            }

                                            {
                                                !isCreator && <>
                                                    {
                                                        !eventHasEnded &&
                                                        <>
                                                            <CopyToClipboard
                                                                text={`${EnvironmentUrls.FRONTEND_URL}/${contest$?.user?.userName}/${contest$.link}`}
                                                                onCopy={() => linkCopied()}>
                                                                <Button loading={false}
                                                                        variant='light-secondary'
                                                                        withIcon="link"
                                                                        disabled={false}
                                                                        btnClass="w-100"
                                                                        body={'Copy Contest Link'}
                                                                        size='md'/>
                                                            </CopyToClipboard>
                                                        </>
                                                    }
                                                    {
                                                        showLiveUpdates &&
                                                        <>
                                                            <Button loading={false}
                                                                    variant='gray'
                                                                    disabled={false}
                                                                    body={'View Results'}
                                                                    btnClass="w-100"
                                                                    size='md'
                                                                    onClick={() => navigate('results')}/>
                                                        </>
                                                    }
                                                </>
                                            }
                                        </div>

                                        <div>
                                            {
                                                isCreator &&
                                                <>
                                                    <div className="row mt-3">
                                                        <div className="col-md-7">
                                                            <Button loading={false}
                                                                    variant='gray'
                                                                    withIcon="edit"
                                                                    btnClass="w-100"
                                                                    disabled={eventIsInProgress}
                                                                    body={'Edit'}
                                                                    title="Edit Contest"
                                                                    size='md'
                                                                    onClick={() => {
                                                                        navigate(isUserContest ? 'edit-user-contest' : isPaidContest ? 'edit-paid-contest' : 'edit-unique-contest')
                                                                    }}/>
                                                        </div>
                                                        <div className="col-md-5">
                                                            <Button loading={false}
                                                                    variant='gray'
                                                                    withIcon="trash"
                                                                    btnClass="w-100"
                                                                    disabled={eventIsInProgress}
                                                                    body={'Delete'}
                                                                    title="Delete Contest"
                                                                    size='md'
                                                                    onClick={() => setDeleteModalOpen(true)}/>
                                                        </div>
                                                    </div>
                                                </>
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {
                                isCreator &&
                                <div className="card">
                                    <div className="card-footer d-flex justify-content-between py-3">
                                        <p className="mb-0">Allow voters to see live results</p>

                                        <Switch loading={toggling}
                                                isToggled={showLiveUpdates}
                                                onChange={() => updateLiveUpdatesVisibility()}/>
                                    </div>
                                </div>
                            }
                        </div>
                        <div className="col-md-8">
                            <div className="flex-space-between mb-4">
                                <h4>Categories <span>({categories$.length})</span></h4>

                                {
                                    isCreator && eventIsUpcoming &&
                                    <>
                                        <Link to={`add-category`}>
                                            <Icon name="plus"/> Add Category
                                        </Link>
                                    </>
                                }
                            </div>

                            {!categories$.length && <NoContestCategoryPlaceholder/>}

                            {
                                categories$.map(category =>
                                    <ContestCategoriesListing category={category}
                                                              contest={contest$}
                                                              key={category.id}/>
                                )
                            }
                        </div>
                    </div>


                    <Modal body={startedContestModal.body}
                           footer={startedContestModal.footer}
                           open={isStartedModalOpen}
                           onClose={() => setStartedModalOpen(false)}/>

                    {/*add cover Modal*/}
                    <Modal body={eventCoverModal.body} footer={eventCoverModal.footer} open={isEventCoverModal}
                           onClose={() => setEventCoverModal(false)}/>

                    <Modal body={shareContestModal.body}
                           footer={shareContestModal.footer}
                           open={isSharedModalOpen}
                           onClose={() => setSharedModalOpen(false)}/>

                    <Modal body={deleteEventModal.body}
                           footer={deleteEventModal.footer}
                           open={isDeleteModalOpen}
                           onClose={() => setDeleteModalOpen(false)}/>

                    <Modal body={endContestModal.body}
                           footer={endContestModal.footer}
                           open={isEndContestModalOpen}
                           onClose={() => setEndContestModalOpen(false)}/>
                </>
            }
        </div>
    );
}
