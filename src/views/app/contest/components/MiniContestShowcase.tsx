import ImageUploadPlaceholder from "../../../../assets/images/image-upload-placeholder.svg";
import nl2br from "react-nl2br";
import {Link} from "react-router-dom";
import {ContestInterface} from "../../../../shared/store/features/contest/contestSlice";
import {Button} from "../../../../shared/components/form/Button";

export const MiniContestShowcase = (props: { contest$: ContestInterface }): JSX.Element => {

    const {contest$} = props;

    return (
        <div>
            <div className="voting-image--mini">
                <img src={contest$.displayPicture || ImageUploadPlaceholder} alt="Contest"/>
            </div>

            <h4 className="mt-4">{contest$.name}</h4>

            <p className="text-faint-1">{nl2br(contest$.description)}</p>

            <div className="mt-3">
                <Link to={`/${contest$.user?.userName}/${contest$.link}`}>
                    <Button loading={false}
                            variant='light-primary'
                            withIcon="open-in-tab"
                            btnClass="w-100"
                            disabled={false}
                            body={'View Contest'}
                            size='md'/>
                </Link>
            </div>
        </div>
    );
}
