import {CategoryInterface} from "../../../../shared/store/features/contest/categorySlice";
import {
    ContestantInterface,
    getContestantsForCreatorAsync
} from "../../../../shared/store/features/contest/contestantSlice";
import {ContestInterface} from "../../../../shared/store/features/contest/contestSlice";
import {useEffect, useState} from "react";
import {
    getContestantFromState,
    getContestantsAsync,
} from "../../../../shared/store/features/contest/contestantSlice";
import {useAppDispatch} from "../../../../shared/store/hooks";
import {useSelector} from "react-redux";
import ProfilePicturePlaceholder from "../../../../assets/images/pp-placeholder.svg";
import {selectUser} from "../../../../shared/store/features/userSlice";

export const ContestResultCategoriesListing = (props: { category: CategoryInterface, contest: ContestInterface }) => {

    const {category, contest} = props;

    const dispatch = useAppDispatch()

    const user$ = useSelector(selectUser)
    const contestants$ = useSelector(getContestantFromState(category.link))

    const compare = (a: any, b: any) => {
        if (a.totalNoOfVotes < b.totalNoOfVotes) {
            return 1;
        }
        if (a.totalNoOfVotes > b.totalNoOfVotes) {
            return -1;
        }
        return 0;
    }
    let sort_contestant: ContestantInterface[] = []
    const [contestants, setContestants] = useState<ContestantInterface[]>([].sort(compare))
    const [, setLoading] = useState(true);
    const [, setHasError] = useState(false);
    const isCreator = contest?.user?.id === user$.id;

    const isPaidContest = contest.type === 'PayPerVote';

    useEffect(() => getContestants(), []);

    useEffect(() => {
        if (contestants$ && contestants$.length > 0) {
            sort_contestant = contestants$.slice()
            sort_contestant.sort(compare)
            setContestants(sort_contestant)
        }
    }, [contestants$])

    const getContestants = () => {
        const action = isCreator ? getContestantsForCreatorAsync : getContestantsAsync;

        dispatch(action({
            username: contest.user?.userName!,
            contestLink: contest.link,
            categoryLink: category.link

        }))
            .unwrap()
            .catch((error: any) => setHasError(true))
            .finally(() => setLoading(false));
    };

    const calcPercentage = (number: number) => {
        if (number === 0) return 0;

        const calc = ((number / category.totalNoOfVotes!) * 100);

        return +calc.toString().split(".")[1] > 0 ? calc.toFixed(2) : calc.toString().split(".")[0];
    }

    return (
        <div className="category-results card-body--item-list">
            <h3>{category.name}</h3>

            <div className="d-flex mb-5">
                <p className="mr-4">
                    <span className="dot dot--secondary"/>
                    {category.totalNoOfVotes!} Vote{category.totalNoOfVotes! === 1 ? '' : 's'}
                </p>

                {
                    isPaidContest &&
                    <>
                        <p className="mr-4">
                            <span className="dot dot--blue"/>
                            {category.totalNoOfVoters!} Voter{category.totalNoOfVoters! === 1 ? '' : 's'}
                        </p>
                    </>
                }

                {
                    isCreator && isPaidContest &&
                    <>
                        <p className="mr-4">
                            <span className="dot dot--yellow"/>
                            ₦{Number(contest.amount!) * category.totalNoOfVotes!}
                        </p>
                    </>
                }
            </div>

            {contestants.map(contestant =>
                <div key={contestant.id} className="category-contestants-results">
                    <div className="flex-space-between">
                        <div className="category--listing--image mb-3">
                            <img src={contestant.displayPicture || ProfilePicturePlaceholder} alt="Contestant"/>
                            <span className="ml-1 font-weight-bold">
                                {contestant.name}
                            </span>
                        </div>

                        <h3>
                            {calcPercentage(contestant.totalNoOfVotes!)}%
                        </h3>
                    </div>


                    <div className="progress mb-2">
                        <div className="progress--secondary"
                             style={{width: calcPercentage(contestant.totalNoOfVotes!) + '%'}}/>
                    </div>

                    <div className="d-flex">
                        <p className="mr-4">
                            <span className="dot dot--secondary"/>
                            {contestant.totalNoOfVotes!} Vote{contestant.totalNoOfVotes! === 1 ? '' : 's'}
                        </p>

                        {
                            isPaidContest &&
                            <>
                                <p className="mr-4">
                                    <span className="dot dot--blue"/>
                                    {contestant.totalNoOfVoters!} Voter{contestant.totalNoOfVoters! === 1 ? '' : 's'}
                                </p>
                            </>
                        }

                        {
                            isCreator && isPaidContest &&
                            <>
                                <p className="mr-4">
                                    <span className="dot dot--yellow"/>
                                    ₦{Number(contest.amount!) * contestant.totalNoOfVotes!}
                                </p>
                            </>
                        }
                    </div>
                </div>
            )
            }
        </div>
    );
};
