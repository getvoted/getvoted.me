import DashboardHero from "../../../../assets/images/dashboard-hero.svg";
import {Link} from "react-router-dom";

export const NoContestCategoryPlaceholder = () => {
    return (
        <div className="text-center mt-5">
            <img src={DashboardHero} alt="DashboardHero" width="40%"/>
            <h6 className="mt-3 mb-3">There are no categories here yet</h6>
            <Link to={`add-category`}>
                <button type="submit"
                        className="button button--primary">
                    Add Category
                </button>
            </Link>
        </div>
    );
};
