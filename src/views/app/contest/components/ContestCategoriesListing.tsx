import {Link} from "react-router-dom";
import {Icon} from "../../../../shared/components/ui/icon";
import {CategoryInterface} from "../../../../shared/store/features/contest/categorySlice";
import {ContestInterface} from "../../../../shared/store/features/contest/contestSlice";
import {useEffect, useState} from "react";
import {
    getContestantFromState,
    getContestantsAsync,
} from "../../../../shared/store/features/contest/contestantSlice";
import {useAppDispatch} from "../../../../shared/store/hooks";
import {useSelector} from "react-redux";
import ProfilePicturePlaceholder from "../../../../assets/images/pp-placeholder.svg";
import {selectUser} from "../../../../shared/store/features/userSlice";

export const ContestCategoriesListing = (props: { category: CategoryInterface, contest: ContestInterface }) => {

    const {category, contest} = props;

    const dispatch = useAppDispatch()

    const user$ = useSelector(selectUser)
    let contestants$ = useSelector(getContestantFromState(category.link));

    const [, setLoading] = useState(false);
    const [, setHasError] = useState(false);

    const eventIsUpcoming = contest.status === 'Upcoming';
    const isCreator = contest?.user?.id === user$.id;

    useEffect(() => getContestants(), []);

    const getContestants = () => {
        dispatch(getContestantsAsync({
            username: contest.user?.userName!,
            contestLink: contest.link,
            categoryLink: category.link
        }))
            .unwrap()
            .catch((error: any) => setHasError(true))
            .finally(() => setLoading(false));
    };

    return (
        <div className="accordion accordion-flush mb-15" id={'accordionFlushExample' + category.id}>
            {/*<div className="accordion accordion-flush open" id={'accordionFlushExample' + category.id}>*/}
            <div className="accordion-item">
                <h2 className="accordion-header" id={'flush-heading' + category.id}>
                    <button className="accordion-button collapsed font-weight-normal"
                            type="button"
                            data-bs-toggle="collapse"
                            data-bs-target={'#flush-collapse' + category.id} aria-expanded="false"
                            aria-controls={'flush-collapse' + category.id}>
                        {category.name}
                    </button>
                </h2>
                {/*<div id={'flush-collapse' + category.id} className="accordion-collapse collapse"*/}
                <div id={'flush-collapse' + category.id} className="accordion-collapse"
                     aria-labelledby={'flush-heading' + category.id}>
                    <div className="accordion-body pt-0">

                        <div className="row">
                            {contestants$ &&
                                contestants$.map(contestant =>
                                    <div className="col-md-4" key={contestant.id}>
                                        <h4 >
                                            <Link to={`${category.link}/${contestant.link}`}>
                                                <div className="category--listing">
                                                    <div className="voting-image--contestant">
                                                        <div className="img-container">
                                                            <img src={contestant.displayPicture || ProfilePicturePlaceholder}
                                                                 alt="Contestant"/>
                                                        </div>
                                                        <div style={{backgroundImage: `url(${contestant.displayPicture})`}}
                                                             className="bg-withBlur">
                                                        </div>
                                                    </div>

                                                    <div className="category--listing--name mt-3">
                                                        {contestant.name}
                                                    </div>
                                                </div>
                                            </Link>
                                        </h4>
                                    </div>
                                )
                            }

                        </div>
                        <div className="flex-space-between">
                            <p className="small mb-0 mt-4">
                                {
                                    isCreator && eventIsUpcoming &&
                                    <>
                                        <Link
                                            to={`/${contest.user?.userName}/${contest.link}/${category.link}/add-contestant`}
                                            className="mr-4">
                                            <Icon name="plus"/> Add Contestant
                                        </Link>
                                    </>
                                }

                                <Link to={`/${contest.user?.userName}/${contest.link}/${category.link}`}>
                                    <Icon name="open-in-tab"/> View Category
                                </Link>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
