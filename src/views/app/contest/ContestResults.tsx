import {useEffect, useState} from "react";
import {useAppDispatch} from "../../../shared/store/hooks";
import {getContestAsync, selectContest} from "../../../shared/store/features/contest/contestSlice";
import {useParams} from "react-router-dom";
import {Loader} from "../../../shared/components/loaders/Loader";
import {LoadingError} from "../../../shared/components/loaders/LoadingError";
import {useSelector} from "react-redux";
import {getCategoriesAsync, selectCategories} from "../../../shared/store/features/contest/categorySlice";
import {ContestResultCategoriesListing} from "./components/ContestResultCategoriesListing";
import {Icon} from "../../../shared/components/ui/icon";
import {MiniContestShowcase} from "./components/MiniContestShowcase";


export const ContestResults = (): JSX.Element => {

    const dispatch = useAppDispatch()
    const {username, contestLink} = useParams();

    const contest$ = useSelector(selectContest)
    const categories$ = useSelector(selectCategories)

    const [loading, setLoading] = useState(false);
    const [hasError, setHasError] = useState(false);
    const [showLiveUpdates, setShowLiveUpdates] = useState(false);

    useEffect(() => {
        getContest();
        getCategories();
    }, [username, contestLink]);

    useEffect(() => setShowLiveUpdates(contest$?.showLiveUpdates?.toLowerCase() === 'everyone'), [contest$]);

    const getContest = () => {
        if (username && contestLink) {
            setLoading(true)
            setHasError(false)

            dispatch(getContestAsync({username, contestLink}))
                .catch(() => setHasError(true))
                .finally(() => setLoading(false));

            dispatch(getCategoriesAsync({username, contestLink}))
        }
    };

    const getCategories = () => {
        if (username && contestLink) dispatch(getCategoriesAsync({username, contestLink}))
    }

    return (
        <div>
            {loading && <><Loader display="Contest"/></>}
            {hasError && <><LoadingError display="There was a problem loading the contest!"/></>}

            {(!loading && !hasError) && contest$.name &&
                <>
                    <div className="row mt-30">
                        <div className="col-md-8">

                            {
                                showLiveUpdates &&
                                <>
                                    <div className="card">
                                        <div className="card-header d-flex">
                                            <div className="mr-3">
                                                <div className="d-flex align-items-center">
                                                    <div className="result-icon cl-green">
                                                        <Icon name='free-poll'/>
                                                    </div>
                                                    <h2 className="title ml-2">
                                                        {contest$.totalNoOfVotes} Vote{contest$.totalNoOfVotes === 1 ? "" : "s"}
                                                    </h2>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="card-body">
                                            {
                                                categories$.map(category =>
                                                    <ContestResultCategoriesListing category={category}
                                                                                    contest={contest$}
                                                                                    key={category.id}/>
                                                )
                                            }
                                        </div>
                                    </div>
                                </>
                            }

                            {
                                !showLiveUpdates &&
                                <>
                                    <div className="card">
                                        <div className="card-header">
                                            <h4><i className="lni lni-notification-2"/> Notice</h4>
                                        </div>
                                        <div className="card-body">
                                            <p>You're not allowed to see the results</p>
                                        </div>
                                    </div>
                                </>
                            }

                        </div>
                        <div className="col-md-4">
                            <MiniContestShowcase contest$={contest$}/>
                        </div>
                    </div>
                </>
            }
        </div>
    );
}
