import {useEffect, useState} from "react";
import {Link, useNavigate, useParams} from "react-router-dom";
import {useSelector} from "react-redux";
import {useAppDispatch} from "../../../shared/store/hooks";
import {getCategoryAsync, selectCategory} from "../../../shared/store/features/contest/categorySlice";
import {
    ContestInterface,
    getContestAnonymouslyAsync,
    getContestAsync,
    selectContest
} from "../../../shared/store/features/contest/contestSlice";
import {Loader} from "../../../shared/components/loaders/Loader";
import {LoadingError} from "../../../shared/components/loaders/LoadingError";
import nl2br from "react-nl2br";
import {
    deleteContestantAsync,
    getContestantAsync,
    selectContestant
} from "../../../shared/store/features/contest/contestantSlice";
import ProfilePicturePlaceholder from "../../../assets/images/pp-placeholder.svg";
import {toast} from "react-toastify";
import {usePaystackPayment} from 'react-paystack';
import {
    getMyVoteForContestantAsync,
    voteUserContestAsync,
    ContestVoteInterface,
    selectVotedForContestant,
    votePaidContestAsync,
    ContestVotePaidInterface,
    getMyVoteForCategoryAsync,
    selectVotedInCategory,
    getMyVoteForContestantLoggedInAsync,
    getMyVoteForCategoryLoggedInAsync, voteUniqueContestAsync
} from "../../../shared/store/features/contest/contestVoteSlice";
import {CopyToClipboard} from "react-copy-to-clipboard";
import {Environment, EnvironmentUrls} from "../../../shared/utils/environment";
import {selectUser} from "../../../shared/store/features/userSlice";
import {App} from "../../../shared/utils/app";
import {MiniContestShowcase} from "./components/MiniContestShowcase";
import {Button} from "../../../shared/components/form/Button";
import {Modal} from "../../../shared/components/ui/Modal";
import DeleteImg from "../../../assets/images/delete.svg";
import VoteImg from "../../../assets/images/vote-image.svg";
import CurrencyFormat from 'react-currency-format';
import WithdrawalSuccessImg from "../../../assets/images/withdrawal-success.svg";
import {useVisitorData} from "@fingerprintjs/fingerprintjs-pro-react";
import {LoadingNotFound} from "../../../shared/components/loaders/LoadingNotFound";

const PaystackCustomButton = ({reference, email, amount, voteAction, cancelAction, disabled}: { reference: string, email: string, amount: number, voteAction: any, cancelAction: any, disabled: boolean }) => {

    const config = {
        reference,
        email,
        amount,
        publicKey: Environment.PAYSTACK_PUBLIC_KEY!,
    };

    const initializePayment = usePaystackPayment(config);

    return (
        <div>
            <Button loading={false}
                    variant='primary'
                    disabled={disabled}
                    btnClass='w-100'
                    withIcon='money'
                    onClick={() => initializePayment(voteAction, cancelAction)}
                    body={'Pay'}
                    size='md'/>
        </div>
    );
};

export const ContestContestant = (): JSX.Element => {

    const dispatch = useAppDispatch()
    const navigate = useNavigate();
    const {username, contestLink} = useParams();
    const {categoryLink} = useParams();
    const {contestantLink} = useParams();

    const [, setContestLoading] = useState(false);
    const [, setContestHasError] = useState(false);
    const [, setCategoryLoading] = useState(false);
    const [, setCategoryHasError] = useState(false);
    const [contestantLoading, setContestantLoading] = useState(false);
    const [contestantHasError, setContestantHasError] = useState(false);
    const [notFound, setNotFound] = useState(false);
    const [deleting, setDeleting] = useState(false);
    const [voting, setVoting] = useState(false);
    const [noOfVotes, setNoOfVotes]: any = useState(1)
    const [noOfVotesEmail, setNoOfVotesEmail]: any = useState(1)
    const [finalAmount, setFinalAmount] = useState<number>(1);
    const [isDeleteModalOpen, setDeleteModalOpen] = useState(false)
    const [isVoteFreeModalOpen, setVoteFreeModalOpen] = useState(false)
    const [isVotePaidModalOpen, setVotePaidModalOpen] = useState(false)
    const [isVoteSuccessModalOpen, setVoteSuccessModalOpen] = useState(false)
    const [myVoteLoading, setMyVoteLoading] = useState(false)

    const {data} = useVisitorData();

    const user$ = useSelector(selectUser)
    const contest$ = useSelector(selectContest)
    const category$ = useSelector(selectCategory)
    const contestant$ = useSelector(selectContestant)
    const hasVotedForContestant$ = useSelector(selectVotedForContestant)
    const hasVotedInCategory$ = useSelector(selectVotedInCategory)

    const eventIsUpcoming = contest$.status === 'Upcoming';
    const eventIsInProgress = contest$.status === 'Ongoing';
    const isCreator = contest$?.user?.id === user$.id;
    const isLoggedIn = App.isLoggedIn();

    const isUserContest = contest$?.type === 'VoterLogsIn';
    const isUniqueContest = contest$?.type === 'UniqueIdentifier';
    const isPaidContest = contest$?.type === 'PayPerVote';

    const config = {
        reference: (new Date()).getTime().toString(),
        email: user$.email,
        amount: finalAmount * 100,
        publicKey: Environment.PAYSTACK_PUBLIC_KEY!,
    };

    useEffect(() => getContest(), [username, contestLink, data]);

    useEffect(() => setFinalAmount(noOfVotes * Number(contest$.amount)), [contest$]);

    useEffect(() => setNoOfVotesEmail(user$.email), [user$]);

    useEffect(() => getCategory(), [categoryLink]);

    useEffect(() => getContestant(), [contestantLink]);

    function checkMyVote(contest: ContestInterface) {
        const isUserContest = contest?.type === 'VoterLogsIn';
        const isUniqueContest = contest?.type === 'UniqueIdentifier';
        const isPaidContest = contest?.type === 'PayPerVote';

        if (isUniqueContest || isPaidContest) {
            getMyVoteForContestant();
            getMyVoteForCategory();
        } else if (isUserContest && isLoggedIn) {
            getMyVoteForContestantLoggedIn();
            getMyVoteForCategoryLoggedIn();
        }
    }

    const getContest = () => {
        if (username && contestLink && data) {
            setContestLoading(true)
            setContestHasError(false)

            const actionContest = App.isLoggedIn() ? getContestAsync : getContestAnonymouslyAsync;

            dispatch(actionContest({username, contestLink}))
                .unwrap()
                .then((contest: ContestInterface) => checkMyVote(contest))
                .catch((error: any) => setContestHasError(true))
                .finally(() => setContestLoading(false));
        }
    };

    const getCategory = () => {
        if (categoryLink && username && contestLink) {
            setCategoryLoading(true)
            setCategoryHasError(false)

            dispatch(getCategoryAsync({username, contestLink, categoryLink}))
                .unwrap()
                .catch((error: any) => setCategoryHasError(true))
                .finally(() => setCategoryLoading(false));
        }
    };

    const linkCopied = () => toast.success("Contestant Link Copied")

    const getContestant = () => {
        if (categoryLink && username && contestLink && contestantLink) {
            setContestantLoading(true)
            setContestantHasError(false)
            setNotFound(false)

            dispatch(getContestantAsync({username, contestLink, categoryLink, contestantLink}))
                .unwrap()
                .catch((error) => {
                    if (error.statusCode === 404) setNotFound(true)
                    if (error.statusCode !== 404) setContestantHasError(true)
                })
                .finally(() => setContestantLoading(false));
        }
    };

    const getMyVoteForContestant = () => {
        if (categoryLink && username && contestLink && contestantLink && data) {
            setMyVoteLoading(true)

            dispatch(getMyVoteForContestantAsync({
                username,
                contestLink,
                categoryLink,
                contestantLink,
                identifier: data?.visitorId
            }))
                .finally(() => setMyVoteLoading(false));
        }
    };

    const getMyVoteForCategory = () => {
        if (categoryLink && username && contestLink && categoryLink && data) {
            setMyVoteLoading(true)

            dispatch(getMyVoteForCategoryAsync({username, contestLink, categoryLink, identifier: data?.visitorId}))
                .finally(() => setMyVoteLoading(false));
        }
    };

    const getMyVoteForContestantLoggedIn = () => {
        if (categoryLink && username && contestLink && contestantLink) {
            setMyVoteLoading(true)

            dispatch(getMyVoteForContestantLoggedInAsync({username, contestLink, categoryLink, contestantLink}))
                .finally(() => setMyVoteLoading(false));
        }
    };

    const getMyVoteForCategoryLoggedIn = () => {
        if (categoryLink && username && contestLink && categoryLink) {
            setMyVoteLoading(true)

            dispatch(getMyVoteForCategoryLoggedInAsync({username, contestLink, categoryLink}))
                .finally(() => setMyVoteLoading(false));
        }
    };

    const deleteContestant = () => {
        if (category$.contestId && contestant$.id) {
            setDeleting(true)

            dispatch(deleteContestantAsync({eventId: category$.contestId, contestantId: contestant$.id}))
                .unwrap()
                .then(() => {
                    toast.success('Contestant Deleted');
                    navigate(`/${user$.userName}/${contestLink}`, {replace: true})
                })
                .finally(() => setDeleting(false));
        }
    };

    const calculateFinalAmount = (e: any) => {
        setNoOfVotes(e.target.value)

        setFinalAmount(e.target.value * Number(contest$.amount));
    }

    const vote = () => {
        if (isUserContest) {
            voteUser();
        }

        if (isUniqueContest) {
            voteUnique();
        }
    }

    const voteUser = () => {
        if (contest$.id && category$.id && contestant$.id) {
            setVoting(true)

            const payload: ContestVoteInterface = {
                contestId: contest$.id,
                categoryId: category$.id,
                contestantId: contestant$.id,
            };

            dispatch(voteUserContestAsync(payload))
                .unwrap()
                .then(() => {
                    setVoteFreeModalOpen(false)
                    setVoteSuccessModalOpen(true)

                    checkMyVote(contest$);
                })
                .finally(() => setVoting(false));
        }
    };

    const voteUnique = () => {
        if (contest$.id && category$.id && contestant$.id && data?.visitorId) {
            setVoting(true)

            const payload: ContestVoteInterface = {
                contestId: contest$.id,
                categoryId: category$.id,
                contestantId: contestant$.id,
                identifier: data?.visitorId,
            };

            dispatch(voteUniqueContestAsync(payload))
                .unwrap()
                .then(() => {
                    setVoteFreeModalOpen(false)
                    setVoteSuccessModalOpen(true)

                    checkMyVote(contest$);
                })
                .finally(() => setVoting(false));
        }
    };

    const votePaid = (reference: any) => {
        if (contest$.id && category$.id && contestant$.id && data?.visitorId) {
            setVoting(true)

            const payload: ContestVotePaidInterface = {
                contestId: contest$.id,
                categoryId: category$.id,
                contestantId: contestant$.id,
                identifier: data?.visitorId,
                reference: reference.reference,
                amount: finalAmount,
                noOfVotes: noOfVotes,
            };

            dispatch(votePaidContestAsync(payload))
                .unwrap()
                .then(() => {
                    setVoteSuccessModalOpen(true)
                    getContestant();

                    checkMyVote(contest$);
                })
                .finally(() => paidVoteCancelled());
        }
    };

    const paidVoteCancelled = () => {
        setVotePaidModalOpen(false);
        setVoting(false);
    };

    const voteFreeModal = {
        body: <>
            <img src={VoteImg} className="modal__img" alt='vote'/>
            <h3 className="title">
                Vote {contestant$.name}
            </h3>
            <p className="description">
                Once you vote for this contestant, it cannot be undone and you cannot vote for anyone else in this
                category.
            </p>
        </>,
        footer: <>
            <div className="d-flex align-items-center justify-content-between">
                <Button loading={false}
                        variant='gray'
                        disabled={false}
                        body={'Cancel'}
                        onClick={() => setVoteFreeModalOpen(false)}
                        btnClass='w-100 mr-4'
                        size='md'/>
                <Button loading={voting}
                        variant='secondary'
                        disabled={false}
                        btnClass='w-100'
                        withIcon='checkmark'
                        onClick={() => vote()}
                        body={'Vote'}
                        size='md'/>
            </div>
        </>
    }

    const votePaidModal = {
        header: <h3 className="title ">Vote {contestant$.name}</h3>,
        body: <>
            <div className="form-group text-left mt-3">
                <label htmlFor="number_of_votes">Number of votes</label>
                <input type="number"
                       id='number_of_votes'
                       min={1}
                       value={noOfVotes}
                       onChange={(e) => calculateFinalAmount(e)}
                       className="form-control form-control-lg"/>
                {
                    +noOfVotes > 0 &&
                    <div className="mt-3">
                        <p className="mb-2 small text-faint-1">Voting Fee</p>
                        <h4>
                            ₦{<CurrencyFormat value={finalAmount}
                                              displayType={'text'}
                                              thousandSeparator={true}/>}
                        </h4>
                    </div>
                }
            </div>

            {
                !isLoggedIn &&
                <>
                    <div className="form-group text-left mt-3">
                        <label htmlFor="number_of_votes_email">Email address</label>
                        <input type="email"
                               id='number_of_votes_email'
                               className="form-control form-control-lg"
                               onChange={(e) => setNoOfVotesEmail(e.target.value)} />
                    </div>
                </>
            }
        </>,
        footer: <>
            <PaystackCustomButton amount={finalAmount * 100}
                                 reference={config.reference}
                                 email={noOfVotesEmail}
                                 disabled={!finalAmount || +noOfVotes <= 0}
                                 voteAction={votePaid}
                                 cancelAction={paidVoteCancelled}/>

            <Button loading={false}
                    variant='gray'
                    disabled={false}
                    body={'Dismiss'}
                    onClick={() => setVotePaidModalOpen(false)}
                    btnClass='w-100 mr-4'
                    size='md'/>
        </>
    }

    const voteSuccessModal = {
        body: <>
            <img src={WithdrawalSuccessImg} className="modal__img" alt='warning_img'/>
            <h3 className="title">
                You have successfully voted for {contestant$.name} {noOfVotes} time{noOfVotes === 1 ? '' : 's'}
                (₦{finalAmount})
            </h3>
        </>,
        footer: <>
            <Button loading={false}
                    variant='gray'
                    disabled={false}
                    body={'Dismiss'}
                    onClick={() => setVoteSuccessModalOpen(false)}
                    btnClass='w-100 mr-4'
                    size='md'/>
        </>
    }

    const deleteContestantModal = {
        body: <>
            <img src={DeleteImg} className="modal__img" alt='warning_img'/>
            <h3 className="title">Delete Contestant</h3>
            <p className="description">Are you sure you want to delete the contestant <b>“{contestant$.name}”</b>?</p>
        </>,
        footer: <>
            <div className="d-flex align-items-center justify-content-between">
                <Button loading={false}
                        variant='gray'
                        disabled={false}
                        body={'Cancel'}
                        onClick={() => setDeleteModalOpen(false)}
                        btnClass='w-100 mr-4'
                        size='md'/>
                <Button loading={deleting}
                        variant='danger'
                        disabled={false}
                        btnClass='w-100'
                        withIcon='trash'
                        onClick={() => deleteContestant()}
                        body={'Delete'}
                        size='md'/>
            </div>
        </>
    }

    return (
        <div>
            {contestantLoading && <><Loader display="Contestant"/></>}
            {contestantHasError && <><LoadingError display="There was a problem loading Contestant!"/></>}
            {notFound && <><LoadingNotFound display="Contestant"/></>}

            {(!contestantLoading && !contestantHasError && !notFound) &&
                <>
                    <div className="row">
                        <div className="col-md-4">
                            <img src={contestant$.displayPicture || ProfilePicturePlaceholder}
                                 style={{width: '100%'}}
                                 alt="Contestant"/>
                        </div>
                        <div className="col-md-8">
                            <div className="card">
                                <div className="card-body">
                                    <h2 className="mb-0">{contestant$.name}</h2>

                                    <p className="mt-3">
                                        <Link
                                            to={`/${contest$.user?.userName}/${contest$.link}`}>
                                            {contest$.name}
                                        </Link>
                                    </p>

                                    <p className="mt-3">
                                        <Link
                                            to={`/${contest$.user?.userName}/${contest$.link}/${category$.link}`}>
                                            {category$.name}
                                        </Link>
                                    </p>

                                    <p className="mt-3">{nl2br(contestant$.description)}</p>

                                    <div className="vote-buttons-section mt-4">
                                        <div className="button-group">
                                            {
                                                myVoteLoading &&
                                                <>
                                                    Loading...
                                                </>
                                            }

                                            {
                                                !myVoteLoading &&
                                                <>
                                                    {
                                                        eventIsInProgress && !isPaidContest && !hasVotedInCategory$ && !hasVotedForContestant$ &&
                                                        <>
                                                            <Button loading={voting}
                                                                    variant='secondary'
                                                                    withIcon="checkmark"
                                                                    disabled={false}
                                                                    body={`Vote${isPaidContest ? ' (₦' + contest$.amount + ')' : ''}`}
                                                                    size='md'
                                                                    onClick={() => isPaidContest ? setVotePaidModalOpen(true) : setVoteFreeModalOpen(true)}/>
                                                        </>
                                                    }

                                                    {
                                                        eventIsInProgress && isPaidContest &&
                                                        <>
                                                            <Button loading={voting}
                                                                    variant='secondary'
                                                                    withIcon="checkmark"
                                                                    disabled={false}
                                                                    body={`Vote${isPaidContest ? ' (₦' + contest$.amount + ')' : ''}`}
                                                                    size='md'
                                                                    onClick={() => isPaidContest ? setVotePaidModalOpen(true) : setVoteFreeModalOpen(true)}/>
                                                        </>
                                                    }

                                                    {
                                                        eventIsInProgress && hasVotedInCategory$ && !hasVotedForContestant$ &&
                                                        <>
                                                            <Button loading={false}
                                                                    variant='gray'
                                                                    withIcon="checkmark"
                                                                    disabled={true}
                                                                    body={'Voted in this category'}
                                                                    size='md'/>
                                                        </>
                                                    }

                                                    {
                                                        eventIsInProgress && hasVotedForContestant$ && !isPaidContest &&
                                                        <>
                                                            <Button loading={false}
                                                                    variant='gray'
                                                                    withIcon="checkmark"
                                                                    disabled={true}
                                                                    body={'Voted'}
                                                                    size='md'/>
                                                        </>
                                                    }

                                                    {
                                                        isCreator &&
                                                        <>
                                                            <Button loading={false}
                                                                    variant='gray'
                                                                    disabled={false}
                                                                    body={'View Results'}
                                                                    size='md'
                                                                    onClick={() => navigate(`/${contest$.user?.userName}/${contest$.link}/full-results`)}/>
                                                        </>
                                                    }

                                                    {
                                                        !isCreator &&
                                                        <>
                                                            <Button loading={false}
                                                                    variant='gray'
                                                                    disabled={false}
                                                                    body={'View Results'}
                                                                    size='md'
                                                                    onClick={() => navigate(`/${contest$.user?.userName}/${contest$.link}/results`)}/>
                                                        </>
                                                    }
                                                </>
                                            }

                                            <CopyToClipboard
                                                text={`${EnvironmentUrls.FRONTEND_URL}/${contest$?.user?.userName}/${contest$.link}/${category$.link}/${contestant$.link}`}
                                                onCopy={() => linkCopied()}>
                                                <Button loading={false} variant='light-secondary' withIcon="link"
                                                        disabled={false} body={'Copy Contestant Link'} size='md'/>
                                            </CopyToClipboard>
                                        </div>

                                        <div>
                                            {
                                                isCreator && eventIsUpcoming &&
                                                <>
                                                    <div className="d-flex align-items-center">
                                                        <Button loading={false}
                                                                variant='gray'
                                                                withIcon="edit"
                                                                btnClass="px-3"
                                                                disabled={false}
                                                                body={'Edit'}
                                                                onClick={() => navigate('edit')}
                                                                size='md'/>
                                                        <Button loading={false}
                                                                variant='gray'
                                                                withIcon="trash"
                                                                btnClass="px-3 ml-3 w-fit_content"
                                                                disabled={false}
                                                                body={''}
                                                                size='md'
                                                                onClick={() => setDeleteModalOpen(true)}/>
                                                    </div>
                                                </>
                                            }
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <Modal body={deleteContestantModal.body}
                           footer={deleteContestantModal.footer}
                           open={isDeleteModalOpen}
                           onClose={() => setDeleteModalOpen(false)}/>
                    <Modal body={voteFreeModal.body}
                           footer={voteFreeModal.footer}
                           open={isVoteFreeModalOpen}
                           onClose={() => setVoteFreeModalOpen(false)}/>
                    <Modal header={votePaidModal.header}
                           body={votePaidModal.body}
                           footer={votePaidModal.footer}
                           open={isVotePaidModalOpen}
                           onClose={() => setVotePaidModalOpen(false)}/>
                    <Modal body={voteSuccessModal.body}
                           footer={voteSuccessModal.footer}
                           open={isVoteSuccessModalOpen}
                           onClose={() => setVoteSuccessModalOpen(false)}/>
                </>
            }
        </div>
    );
}
