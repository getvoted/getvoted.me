import {useEffect, useState} from "react";
import {Link, useNavigate, useParams} from "react-router-dom";
import {useSelector} from "react-redux";
import {useAppDispatch} from "../../../shared/store/hooks";
import {
    deleteCategoryAsync,
    getCategoryAsync,
    selectCategory
} from "../../../shared/store/features/contest/categorySlice";
import {
    ContestInterface,
    getContestAnonymouslyAsync,
    getContestAsync,
    selectContest
} from "../../../shared/store/features/contest/contestSlice";
import {Loader} from "../../../shared/components/loaders/Loader";
import {LoadingError} from "../../../shared/components/loaders/LoadingError";
import {
    getContestantFromState,
    getContestantsAsync
} from "../../../shared/store/features/contest/contestantSlice";
import ProfilePicturePlaceholder from "../../../assets/images/pp-placeholder.svg";
import {
    getMyVoteForCategoryAsync,
    getMyVoteForCategoryLoggedInAsync,
} from "../../../shared/store/features/contest/contestVoteSlice";
import {EnvironmentUrls} from "../../../shared/utils/environment";
import {CopyToClipboard} from "react-copy-to-clipboard";
import {toast} from "react-toastify";
import {selectUser} from "../../../shared/store/features/userSlice";
import {MiniContestShowcase} from "./components/MiniContestShowcase";
import {Button} from "../../../shared/components/form/Button";
import {Modal} from "../../../shared/components/ui/Modal";
import DeleteImg from "../../../assets/images/delete.svg";
import nl2br from "react-nl2br";
import {Icon} from "../../../shared/components/ui/icon";
import {App} from "../../../shared/utils/app";
import NoPollImage from "../../../assets/images/no-poll.svg";
import classNames from "classnames";
import {useVisitorData} from "@fingerprintjs/fingerprintjs-pro-react";
import {LoadingNotFound} from "../../../shared/components/loaders/LoadingNotFound";

export const ContestCategory = (): JSX.Element => {

    const dispatch = useAppDispatch()
    const navigate = useNavigate();
    const {username, contestLink} = useParams();
    const {categoryLink} = useParams();

    const {data} = useVisitorData();

    const user$ = useSelector(selectUser)
    const contest$ = useSelector(selectContest)
    const category$ = useSelector(selectCategory)
    const contestants$ = useSelector(getContestantFromState(categoryLink!));

    const [, setContestLoading] = useState(false);
    const [, setContestHasError] = useState(false);
    const [contestantsLoading, setContestantsLoading] = useState(false);
    const [contestantsHasError, setContestantsHasError] = useState(false);
    const [categoryLoading, setCategoryLoading] = useState(false);
    const [notFound, setNotFound] = useState(false);
    const [categoryHasError, setCategoryHasError] = useState(false);
    const [deleting, setDeleting] = useState(false);
    const [isDeleteModalOpen, setDeleteModalOpen] = useState(false)

    const eventIsUpcoming = contest$.status === 'Upcoming';
    const eventHasEnded = contest$.status === 'Ended';
    const isCreator = contest$?.user?.id === user$.id;

    const isUserContest = contest$?.type === 'VoterLogsIn';
    const isUniqueContest = contest$?.type === 'UniqueIdentifier';
    const isPaidContest = contest$?.type === 'PayPerVote';

    const isLoggedIn = App.isLoggedIn();

    useEffect(() => getContest(), [username, contestLink]);

    useEffect(() => getContestants(), [contestLink, categoryLink]);

    useEffect(() => getCategory(), [categoryLink]);

    const getContest = () => {
        if (username && contestLink) {
            setContestLoading(true)
            setContestHasError(false)

            const actionContest = App.isLoggedIn() ? getContestAsync : getContestAnonymouslyAsync;

            dispatch(actionContest({username, contestLink}))
                .unwrap()
                .then((contest: ContestInterface) => {
                    if (isUniqueContest || isPaidContest) {
                        getMyVoteForCategory();
                    } else if (isUserContest && isLoggedIn) {
                        getMyVoteForCategoryLoggedIn();
                    }
                })
                .catch((error: any) => setContestHasError(true))
                .finally(() => setContestLoading(false));
        }
    };

    const getCategory = () => {
        if (categoryLink && username && contestLink) {
            setCategoryLoading(true)
            setCategoryHasError(false)
            setNotFound(false)

            dispatch(getCategoryAsync({username, contestLink, categoryLink}))
                .unwrap()
                .catch((error) => {
                    if (error.statusCode === 404) setNotFound(true)
                    if (error.statusCode !== 404) setCategoryHasError(true)
                })
                .finally(() => setCategoryLoading(false));
        }
    };

    const getMyVoteForCategory = () => {
        if (categoryLink && username && contestLink && categoryLink && data) {
            dispatch(getMyVoteForCategoryAsync({username, contestLink, categoryLink, identifier: data?.visitorId}));
        }
    };

    const getMyVoteForCategoryLoggedIn = () => {
        if (categoryLink && username && contestLink && categoryLink) {
            dispatch(getMyVoteForCategoryLoggedInAsync({username, contestLink, categoryLink}));
        }
    };

    const deleteCategory = () => {
        if (category$.contestId && category$.id) {
            setDeleting(true)

            dispatch(deleteCategoryAsync({eventId: category$.contestId, categoryId: category$.id}))
                .unwrap()
                .then(() => {
                    toast.success('Category Deleted');
                    navigate(`/${user$.userName}/${contestLink}`, {replace: true})
                })
                .finally(() => setDeleting(false));
        }
    };

    const getContestants = () => {
        if (categoryLink && username && contestLink) {
            setContestantsLoading(true)
            setContestantsHasError(false)

            dispatch(getContestantsAsync({username, contestLink, categoryLink}))
                .unwrap()
                .catch((error: any) => setContestantsHasError(true))
                .finally(() => setContestantsLoading(false));
        }
    };

    const linkCopied = () => toast.success("Category Link Copied")

    const deleteCategoryModal = {
        body: <><img src={DeleteImg} className="modal__img" alt='warning_img'/><h3 className="title">Delete
            Category</h3><p className="description">Are you sure you want to delete the
            category <b>“{category$.name}”</b> with {contestants$?.length} contestants?</p></>,
        footer: <>
            <div className="d-flex align-items-center justify-content-between"><Button loading={false} variant='gray'
                                                                                       disabled={false} body={'Cancel'}
                                                                                       onClick={() => setDeleteModalOpen(false)}
                                                                                       btnClass='w-100 mr-4' size='md'/>
                <Button loading={deleting} variant='danger' disabled={false} btnClass='w-100' withIcon='trash'
                        onClick={() => deleteCategory()} body={'Delete'} size='md'/></div>
        </>
    }

    return (
        <div>
            {categoryLoading && <><Loader display="Category"/></>}
            {categoryHasError && <><LoadingError display="There was a problem loading category!"/></>}
            {notFound && <><LoadingNotFound display="Category"/></>}

            {(!categoryLoading && !categoryHasError && !notFound) && contest$ && category$ &&
                <>
                    <div className="row">
                        <div className="col-md-4 d-none d-sm-none d-md-block">
                            <MiniContestShowcase contest$={contest$}/>
                        </div>
                        <div className="col-md-8">
                            <div className="card">
                                <div className="card-body">
                                    <h2>{category$.name}</h2>

                                    <p className="mt-3">
                                        <Link to={`/${contest$.user?.userName}/${contest$.link}`}>
                                            {contest$.name}
                                        </Link>
                                    </p>

                                    <div className="vote-buttons-section mt-4">
                                        <div className="button-group">
                                            {
                                                isCreator &&
                                                <>
                                                    <Button loading={false}
                                                            variant='gray'
                                                            disabled={false}
                                                            body={'View Results'}
                                                            size='md'
                                                            btnClass="mb-2"
                                                            onClick={() => navigate(`/${contest$.user?.userName}/${contest$.link}/full-results`)}/>
                                                </>
                                            }

                                            {
                                                !isCreator &&
                                                <>
                                                    <Button loading={false}
                                                            variant='gray'
                                                            disabled={false}
                                                            body={'View Results'}
                                                            size='md'
                                                            onClick={() => navigate(`/${contest$.user?.userName}/${contest$.link}/results`)}/>
                                                </>
                                            }

                                            {
                                                !eventHasEnded &&
                                                <>
                                                    <CopyToClipboard
                                                        text={`${EnvironmentUrls.FRONTEND_URL}/${contest$?.user?.userName}/${contest$.link}/${category$.link}`}
                                                        onCopy={() => linkCopied()}>
                                                        <Button loading={false} variant='light-secondary'
                                                                withIcon="link" disabled={false}
                                                                body={'Copy Category Link'} size='md'/>
                                                    </CopyToClipboard>
                                                </>
                                            }
                                        </div>

                                        <div>
                                            {
                                                isCreator && eventIsUpcoming &&
                                                <>
                                                    <Link to={'edit'} className="mr-20">
                                                        <Button loading={false}
                                                                variant='gray'
                                                                withIcon="edit"
                                                                btnClass="px-3"
                                                                disabled={false}
                                                                body={''}
                                                                title="Edit Contest"
                                                                size='md'
                                                                onClick={() => {
                                                                }}/>
                                                    </Link>
                                                    <Button loading={false}
                                                            variant='gray'
                                                            withIcon="trash"
                                                            btnClass="px-3"
                                                            disabled={false}
                                                            body={''}
                                                            title="Delete Contest"
                                                            size='md'
                                                            onClick={() => setDeleteModalOpen(true)}/>
                                                </>
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="flex-space-between mb-3">
                                <h4>Contestants <span>({contestants$?.length})</span></h4>

                                {isCreator && eventIsUpcoming &&
                                    <>
                                        <Link to={`add-contestant`}>
                                            <Icon name="plus"/> Add Contestant
                                        </Link>
                                    </>
                                }
                            </div>

                            {contestantsLoading && <><Loader display="Contestants"/></>}
                            {contestantsHasError && <><LoadingError
                                display="There was a problem loading contestants!"/></>}

                            <div className="row">
                                {contestants$ &&
                                    contestants$.map(contestant =>
                                            <div className="col-md-4" key={contestant.id}>
                                                <h4>
                                                    <Link to={`${contestant.link}`}>
                                                        <div className="category--listing">
                                                            <div className="voting-image--contestant">
                                                                <div className="img-container">
                                                                    <img
                                                                        src={contestant.displayPicture || ProfilePicturePlaceholder}
                                                                        alt="Contestant"/>
                                                                </div>
                                                                <div
                                                                    style={{backgroundImage: `url(${contestant.displayPicture})`}}
                                                                    className="bg-withBlur">
                                                                </div>
                                                            </div>

                                                            <div className="category--listing--name mt-3">
                                                                {contestant.name}
                                                            </div>
                                                        </div>
                                                    </Link>
                                                </h4>
                                            </div>
                                    )
                                }

                                {contestants$?.length === 0 &&
                                    <>
                                        <div className="col-md-12">
                                            <div className="mt-40 text-center">
                                                <img src={NoPollImage} alt="No Polls"/>
                                                <h3 className="text-faint mt-20">No contestant in this category yet</h3>

                                                {
                                                    isCreator && eventIsUpcoming &&
                                                    <>
                                                        <p className="mt-20">
                                                            <Link to={`add-contestant`}>
                                                                <Button loading={false}
                                                                        variant='primary'
                                                                        withIcon="plus"
                                                                        disabled={false}
                                                                        body={'Add Contestant'}
                                                                        size='md'/>
                                                            </Link>
                                                        </p>
                                                    </>
                                                }
                                            </div>
                                        </div>
                                    </>}
                            </div>
                        </div>
                    </div>

                    {/*Delete Category Modal*/}
                    <Modal body={deleteCategoryModal.body}
                           footer={deleteCategoryModal.footer}
                           open={isDeleteModalOpen}
                           onClose={() => setDeleteModalOpen(false)}/>
                </>
            }
        </div>
    );
}
