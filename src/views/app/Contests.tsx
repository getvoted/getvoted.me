import {useEffect, useState} from 'react';
import DashboardHero from '../../assets/images/dashboard-hero.svg'
import {useAppDispatch} from "../../shared/store/hooks";
import {
    getMyContestsAsync,
    selectContests,
    ContestInterface
} from "../../shared/store/features/contest/contestSlice";
import {Loader} from "../../shared/components/loaders/Loader";
import {LoadingError} from "../../shared/components/loaders/LoadingError";
import {useSelector} from "react-redux";
import {Link} from "react-router-dom";
import {ContestCard} from "../../shared/components/ContestCard";
import NoPollImage from "../../assets/images/no-poll.svg";
import {Button} from "../../shared/components/form/Button";

export const Contests = (): JSX.Element => {

    const dispatch = useAppDispatch()

    const contests$ = useSelector(selectContests)

    const [loading, setLoading] = useState(false);
    const [hasError, setHasError] = useState(false);

    useEffect(() => getMyContests(), []);

    const getMyContests = () => {
        setLoading(true)
        setHasError(false)

        dispatch(getMyContestsAsync())
            .unwrap()
            .catch(() => setHasError(true))
            .finally(() => setLoading(false));
    };

    return (
        <div>
            <div className="card card-colored mb-40">
                <div className="card-body">
                    <div className="home-welcome">
                        <div className="text-section">
                            <div>
                                <h2>Your Contests ({contests$?.length})</h2>
                                <p className="mb-0">
                                    Create contests and see results in real-time.
                                </p>
                            </div>
                        </div>

                        <div className="image-section">
                            <img src={DashboardHero} alt="DashboardHero" width="70%"/>
                        </div>
                    </div>
                </div>
            </div>

            {loading && <><Loader display="Contests"/></>}
            {hasError && <><LoadingError display="There was a problem loading the contests!"/></>}

            {(!loading && !hasError) &&
                <>
                    <div className="row">
                        {contests$.map((contest: ContestInterface) =>
                            <div className="col-md-6" key={contest.id}>
                                <ContestCard contest={contest}/>
                            </div>
                        )}

                        {
                            contests$.length === 0 &&
                            <>
                                <div className="col-md-12">
                                    <div className="mt-40 text-center">
                                        <img src={NoPollImage} alt="No Events"/>
                                        <h3 className="text-faint mt-20">You Have No Contest Yet</h3>

                                        <p className="mt-30">
                                            <Link to={'/create/paid-event'}>
                                                <Button loading={false}
                                                        btnClass="mr-3"
                                                        variant='primary'
                                                        disabled={false}
                                                        withIcon="plus"
                                                        body={'Create Paid Contest'}
                                                        size='md'/>
                                            </Link>

                                            <Link to={'/create/free-event'}>
                                                <Button loading={false}
                                                        variant='light-primary'
                                                        disabled={false}
                                                        withIcon="plus"
                                                        body={'Create Free Contest'}
                                                        size='md'/>
                                            </Link>
                                        </p>
                                    </div>
                                </div>
                            </>
                        }
                    </div>
                </>
            }
        </div>
    );
}
