import {Link} from "react-router-dom";
import {Button} from "../../shared/components/form/Button";
import {Footer} from "../../shared/components/layout/Footer";
import {Icon} from "../../shared/components/ui/icon";
import HeaderImg from "../../assets/images/contact/Header-Image.svg"

export const Contact = ():JSX.Element => {
    const faqs = [
        {
            question: "How do I create an event?",
            answer: "Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum"
        },
        {
            question: "How does paid events work??",
            answer: "Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum"
        },
        {
            question: "What happens if I don’t get my withdrawal on time?",
            answer: "Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum"
        },
    ]
    return (
        <>
            <main className="page-contact">
                <div className="contact-header">
                    <div className="container">
                        <div className="row align-items-center">
                            <div className="col-md-7 mt-80">
                                <h1 className="title">
                                    Reach out to us
                                    anytime, any day
                                </h1>
                                <p className="font-weight-light">Your questions and enquiries are awaiting a warm response.</p>
                                <p className="text-2x phone"><span><Icon name="phone"/></span><a href="tel:+2348101004493">+234 8101004493</a></p>
                            </div>
                            <div className="col-md-5">
                                <img src={HeaderImg} alt=""/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="contact-faq mb-100">
                    <div className="page-faq container">
                        <h1 className="text-center text-primary-deep mb-40">Frequently Asked Questions (FAQs)</h1>
                        <div className="row">
                            <div className="col-md-12">
                                <div className="accordion" id="accordionExample">
                                    {
                                        faqs.map((faq, index) =>
                                            <div className="faq-accordion accordion-item mb-30 border-0">
                                                <h2 className="accordion-header" id={`heading${index}`}>
                                                    <button className="accordion-button p-4 collapsed" type="button" data-bs-toggle="collapse"
                                                            data-bs-target={`#collapse${index}`} aria-expanded="true"
                                                            aria-controls={`collapse${index}`}>
                                                        {faq.question}
                                                    </button>
                                                </h2>
                                                <div id={`collapse${index}`} className="accordion-collapse collapse"
                                                     aria-labelledby={`heading${index}`} data-bs-parent="#accordionExample">
                                                    <div className="accordion-body">
                                                        {faq.answer}
                                                    </div>
                                                </div>
                                            </div>
                                        )

                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="pb-50">
                    <div className="page-about__cta container">
                        <div className="row align-items-center justify-content-between">
                            <div className="col-md-7">
                                <h1 className="title text-3x">Create your first poll or contest now</h1>
                            </div>
                            <div className="col-md-3">
                                <Link to={'/auth/register'}>
                                    <Button loading={false} variant='primary' disabled={false} body={'Get Started'} btnClass="px-5" size='md' onClick={() => {}}/>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <Footer />
        </>
    )
}
