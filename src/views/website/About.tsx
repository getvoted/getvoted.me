import {Footer} from "../../shared/components/layout/Footer";
import {Button} from "../../shared/components/form/Button";
import {Link} from "react-router-dom";
import HeaderImg from "../../assets/images/gv-about.png"

import EaseOfUseImg from "../../assets/images/about/ease_of_use.svg"
import RealTimeImg from "../../assets/images/about/real_time_results.svg"
import FreeImg from "../../assets/images/about/free.svg"

import PollsImg from "../../assets/images/about/polls.svg"
import FreeEventImg from "../../assets/images/about/free-event.svg"
import PaidEventImg from "../../assets/images/about/paid-event.svg"
import ExperienceImg from "../../assets/images/about/experience.svg"
import RealTimeResults from "../../assets/images/about/RealTimeResults.png"
import ManageComp from "../../assets/images/about/ManageComp.svg"
import {Icon} from "../../shared/components/ui/icon";

export const About = ():JSX.Element => {
    return (
        <>
            <main className="page-about">
                <div className="page-about__header">
                    <div className="container my-50">
                        <div className="row align-items-center">
                            <div className="col-md-6">
                                <h1 className="title">We make Voting & Polling easier.</h1>
                                <p className="description">GetVoted allows you to host contests and create polls to share online for people to vote</p>
                                <Link to={'/auth/register'}>
                                    <Button loading={false} variant='primary' disabled={false} body={'Get Started'} btnClass="button--large px" size='lg' onClick={() => {}}/>
                                </Link>
                            </div>
                            <div className="col-md-6">
                                <img src={HeaderImg} className="w-100" alt="Get voted"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="page-about__why">
                    <div className="container">
                        <h1 className="title">Why GetVoted?</h1>
                        <div className="row mt-50">
                            <div className="col-md-4">
                                <div className="why-container">
                                    <div className="img">
                                        <img src={EaseOfUseImg} alt="Ease Of use"/>
                                    </div>
                                    <h3 className="name">
                                        Easy to use
                                    </h3>
                                    <p className="description">
                                        From creating a poll to creating
                                        contests and sharing online to onboard voters, the whole process is fast, easy and straight forward for all users.
                                    </p>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="why-container">
                                    <div className="img">
                                        <img src={RealTimeImg} alt="Real Time"/>
                                    </div>
                                    <h3 className="name">
                                        Real-time results
                                    </h3>
                                    <p className="description">
                                        As your event goes on, results are
                                        updated for you and your voters (optional) to see. You can always see votes as soon as it is casted and see the voters as well.
                                    </p>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="why-container">
                                    <div className="img">
                                        <img src={FreeImg} alt="Free"/>
                                    </div>
                                    <h3 className="name">
                                        Mostly Free
                                    </h3>
                                    <p className="description">
                                        Two out of our three features are free.
                                        You get charged a small percentage when you use the third - paid voting. GetVoted only makes money when you do.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="page-about__features">
                    <div className="container">
                        <h1 className="title">Features</h1>
                        <div className="row mt-80">
                            <div className="col-md-4">
                                <div className="feature-container">
                                    <div className="img">
                                        <img src={PollsImg} alt=""/>
                                    </div>
                                    <h3 className="name">
                                        Polls
                                    </h3>
                                    <p className="description">
                                        Easily create a poll to gather the public’s opinion or choice on any subject matter. Polls on GetVoted can be created in a matter of seconds.
                                    </p>
                                    <div className="feature_btn">
                                        <a href="#">Get Started <Icon name="open-in-tab"/></a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="feature-container">
                                    <div className="img">
                                        <img src={FreeEventImg} alt=""/>
                                    </div>
                                    <h3 className="name">
                                        Free Contest
                                    </h3>
                                    <p className="description">
                                        This feature allows you to create a well detailed voting competition that can be shared online via links for voters to find their contestants and vote for them.
                                    </p>
                                    <div className="feature_btn">
                                        <a href="#">Get Started <Icon name="open-in-tab"/></a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="feature-container">
                                    <div className="img">
                                        <img src={PaidEventImg} alt=""/>
                                    </div>
                                    <h3 className="name">
                                        Paid Contest
                                    </h3>
                                    <p className="description">
                                        Use this feature when your voters are required to pay to validate their votes. Your earnings will be transparent and you can withdraw anytime the event ends.
                                    </p>
                                    <div className="feature_btn">
                                        <a href="#">Get Started <Icon name="open-in-tab"/></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="page-about__experience">
                    <div className="container">
                        <div className="row mt-80 align-items-center">
                            <div className="col-md-7">
                                <div className="img">
                                    <img src={ExperienceImg} alt="Experience"/>
                                </div>
                            </div>
                            <div className="col-md-5 details">
                                <h1 className="text-3x">Give your voters an engaging experience</h1>
                                <p className="mt-20">
                                    With GetVoted, your voters can see who they are voting for, learn their story and understand what the event is about with cover photos and appropriate descriptions that can be added
                                    to each event.
                                </p>
                                <p className="mt-20">
                                    You can choose to allow your viewers see live vote count of the event, category & contestants. This can make your event more interesting and competitive.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div className="container">
                        <div className="row mt-80 align-items-center">
                            <div className="col-md-5 details">
                                <h1 className="text-3x">
                                    Manage a pay per vote
                                    competition with ease
                                    and pay less
                                </h1>
                                <p className="mt-20">
                                    Create a paid event, receive payments for votes, withdraw your earnings without hassle and see all the stats for only a token.

                                    Head to our <Link to={'/pricing'}>Pricing</Link> page to get started.
                                </p>
                            </div>
                            <div className="col-md-7">
                                <div className="img">
                                    <img src={ManageComp} alt="ManageComp"/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="container">
                        <div className="row mt-80 align-items-center">
                            <div className="col-md-7">
                                <div className="img">
                                    <img src={RealTimeResults} alt="RealTimeResults"/>
                                </div>
                            </div>
                            <div className="col-md-5 details">
                                <h1 className="text-3x">
                                    See your results
                                    in real-time, well
                                    visualized
                                </h1>
                                <p className="mt-20">
                                    Knowing that statistics always help with decision making, GetVoted provides you with all necessary voting information as the event goes on till it ends.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="pb-50">
                    <div className="page-about__cta container">
                        <div className="row align-items-center justify-content-between">
                            <div className="col-md-7">
                                <h1 className="title text-3x">Create your first poll or contest now</h1>
                            </div>
                            <div className="col-md-3">
                                <Link to={'/auth/register'}>
                                    <Button loading={false} variant='primary' disabled={false} body={'Get Started'} btnClass="px-5" size='md' onClick={() => {}}/>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer />
            </main>
        </>
    )
}
