import {Button} from "../../shared/components/form/Button";
import {Icon} from "../../shared/components/ui/icon";
import {Formik, FormikHelpers, Form} from "formik";
import {Link} from "react-router-dom";
import {useState} from "react";
import * as Yup from "yup";
import {TextInput} from "../../shared/components/form/TextInput";
import CurrencyFormat from "react-currency-format";
import { Footer } from "../../shared/components/layout/Footer";


interface calculatorInterface {
    amount: number
    noOfVotes: number
}

export const Pricing = (): JSX.Element => {

    const prices = [
        {
            name: "Paid Contests",
            icon: {
                name: "money",
                viewBox: "0 0 24 24",
            },
            price: <h1 className='card-title pricing-card-title'>5% <small>per vote</small></h1>,
            features: [
                "Your voters pay per vote",
                "Multiple votes per person",
                "Seamless cash withdrawal",
                "View results in realtime"
            ],
            link: ""
        },
        {
            name: "Free Contests",
            icon: {
                name: "free-events",
                viewBox: "0 0 36 36",
            },
            price: <h1 className='card-title pricing-card-title'>Free </h1>,
            features: [
                "Free voting",
                "Single vote per person",
                "View results in realtime",
            ],
            link: ""
        },
        {
            name: "Polls",
            icon: {
                name: "analytics",
                viewBox: "0 0 36 36",
            },
            price: <h1 className='card-title pricing-card-title'>Free </h1>,
            features: [
                "Free voting",
                "Single vote per person",
                "View results in realtime",
            ],
            link: ""
        },
    ]

    const [formInitialValues,] = useState({amount: 50, noOfVotes: 1});

    const formValidation = () => Yup.object({
        amount: Yup.number().required('Required'),
        noOfVotes: Yup.number().required('Required'),
    })

    const testAmount = 50;
    const userPercentage = 0.95;
    const ourPercentage = 0.05;

    const [userAmount, setUserAmount] = useState(testAmount * userPercentage)
    const [appAmount, setAppAmount] = useState(testAmount * ourPercentage)

    const calcFinalAmount = (values: calculatorInterface) => {
        setUserAmount(values.amount * values.noOfVotes * userPercentage);
        setAppAmount(values.amount * values.noOfVotes * ourPercentage);
    }



    const faqs = [
        {
            question: "Lorem iLorem ipsum dolor sit amet, consectetur?",
            answer: "Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum"
        },
        {
            question: "Lorem iLorem ipsum dolor sit?",
            answer: "Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum"
        },
        {
            question: "Lorem iLorem ipsum dolor sit amet?",
            answer: "Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum"
        },
    ]
    return (
        <>
            <main className="page">
                <div className="page-container pricing-page container my-5">
                    <div className="page__header">
                        <h1 className="title text-4x">Pricing</h1>
                        <p>We only charge you when your voters pay you.</p>
                    </div>
                    <div className="card-deck mt-5 text-center">
                        {
                            prices.map((pricing, index) =>
                                <div className="card mb-4 box-shadow pricing-page__card" key={index}>
                                    <div className="card-header d-flex align-items-center text-left">
                                        <div className="result-icon cl-secondary">
                                            <Icon name={pricing.icon.name} viewBox={pricing.icon.viewBox}/>
                                        </div>
                                        <h4 className="my-0 ml-2">{pricing.name}</h4>
                                    </div>
                                    <div className="card-body text-left">
                                        {pricing.price}
                                        <ul className="list-unstyled mt-3 mb-4 features">
                                            {
                                                pricing.features.map((feature, index) => <li key={index}><Icon name="checkmark-circle" />{feature}</li>)
                                            }
                                        </ul>
                                    </div>
                                    <div className="card-footer">
                                        <Link to={'/auth/register'}>
                                            <Button loading={false} variant='primary' disabled={false} body={'Get Started'} btnClass="w-100" size='md' onClick={() => {}}/>
                                        </Link>
                                    </div>
                                </div>
                            )
                        }
                    </div>
                    <div className="pricing-calculator">
                        <div className="row align-items-center">
                            <div className="col-md-4">
                                <h1 className="text-primary-deep">Paid Contest Calculator</h1>
                                <p className="text-md">Use this calculator to get an estimate of your upcoming Paid Contest</p>
                            </div>
                            <div className="col-md-8">
                                <div className="card">
                                    <div className="card-body">
                                        <Formik
                                            initialValues={formInitialValues}
                                            validationSchema={formValidation}
                                            enableReinitialize
                                            onSubmit={(formValues: calculatorInterface, actions: FormikHelpers<calculatorInterface>) => {}}>
                                            {({isSubmitting, setFieldValue, values,  isValid, dirty}) => (
                                                <Form>
                                                    <div className="row">
                                                        <div className="form-group col-md-8">
                                                            <div className="form-group">
                                                                <label htmlFor="amount">Amount Per Vote</label>
                                                                <CurrencyFormat className="form-control"
                                                                                allowNegative={false}
                                                                                thousandSeparator={true}
                                                                                name="amount"
                                                                                type="text"
                                                                                prefix="₦"
                                                                                value={values.amount}
                                                                                onValueChange={(e) => setFieldValue('amount' , e.floatValue)}
                                                                                onKeyUp={() => calcFinalAmount(values)}
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="form-group col-md-4">
                                                            <TextInput
                                                                label="Number of votes"
                                                                name="noOfVotes"
                                                                type="number"
                                                                min="0"
                                                                onWheel={(e: any) => e.target.blur()}
                                                                onKeyUp={() => calcFinalAmount(values)}
                                                            />
                                                        </div>
                                                    </div>
                                                </Form>
                                                )}
                                            </Formik>
                                        <div className="d-flex">
                                            <div>
                                                <span className="text-muted font-weight-light">
                                                    You get
                                                </span>
                                                <h2>₦{<CurrencyFormat value={parseFloat(userAmount.toFixed(2))} displayType={'text'} thousandSeparator={true} />}</h2>
                                            </div>
                                            <div className="ml-40">
                                            <span className="text-muted font-weight-light">
                                                GetVoted gets
                                            </span>
                                                <h2>₦{<CurrencyFormat value={parseFloat(appAmount.toFixed(2))} displayType={'text'} thousandSeparator={true} />}</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {/*<div className="page-faq">*/}
                    {/*    <h1 className="text-center text-primary-deep mb-40">Frequently Asked Questions (FAQs)</h1>*/}
                    {/*    <div className="row">*/}
                    {/*        <div className="col-md-12">*/}
                    {/*            <div className="accordion" id="accordionExample">*/}
                    {/*                {*/}
                    {/*                    faqs.map((faq, index) =>*/}
                    {/*                        <div className="faq-accordion accordion-item mb-30 border-0" key={index}>*/}
                    {/*                            <h2 className="accordion-header" id={`heading${index}`}>*/}
                    {/*                                <button className="accordion-button p-4 collapsed" type="button" data-bs-toggle="collapse"*/}
                    {/*                                        data-bs-target={`#collapse${index}`} aria-expanded="true"*/}
                    {/*                                        aria-controls={`collapse${index}`}>*/}
                    {/*                                    {faq.question}*/}
                    {/*                                </button>*/}
                    {/*                            </h2>*/}
                    {/*                            <div id={`collapse${index}`} className="accordion-collapse collapse"*/}
                    {/*                                 aria-labelledby={`heading${index}`} data-bs-parent="#accordionExample">*/}
                    {/*                                <div className="accordion-body">*/}
                    {/*                                    {faq.answer}*/}
                    {/*                                </div>*/}
                    {/*                            </div>*/}
                    {/*                        </div>*/}
                    {/*                    )*/}

                    {/*                }*/}
                    {/*            </div>*/}
                    {/*        </div>*/}
                    {/*    </div>*/}
                    {/*</div>*/}
                </div>
                <Footer />
            </main>
        </>
    )
}
