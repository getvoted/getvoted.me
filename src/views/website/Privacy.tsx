import {useState} from "react";
import {Footer} from "../../shared/components/layout/Footer";

export const Privacy = (): JSX.Element => {

    const sections = [
        {
            title: "Privacy Commitment",
            body: <>
                    <p className="mb-4">
                    390labs Limited, its subsidiaries and its affiliates are focused on your security.
                    We handle and treat your personal information in line with requirements of the Nigeria Data
                    Protection Regulation (NDPR).
                </p>
            </>
        },
        {
            title: "Introduction",
            body: <>
                <p className="mb-4">
                    Thanks for choosing GetVoted To access our service(s) (as defined in the Terms and Condition of
                    Use),
                    GetVoted (“we”, “us”, “our” “platform” “your account”) needs to collect some information about you,
                    and in this Privacy Policy, we will describe the information we collect,
                    what we do with it, and how you can manage and control the use of your information which we collect.
                    By using or interacting with GetVoted, you are entering into a binding contract with 390labs
                    Limited,
                    allowing and consenting to the use of the information you provide us. If you do not agree to these
                    terms,
                    please do not use this Service.
                </p>
            </>
        },
        {
            title: "Information we collect",
            body: <>
                <h4>3.1. Registration Data</h4>
                <p className="mb-4">
                    When you sign up to use GetVoted, we may collect information we ask you for,
                    like your name, password, username, email address, date of birth, gender, business name, business
                    address,
                    business registration number, telephone number, payment and transaction information for billing
                    purposes.
                    We may also collect information you voluntarily provide us. If you connect to GetVoted using your
                    Facebook or Gmail
                    Credentials, you authorize us to collect your authentication information, such as your username,
                    encrypted access credentials,
                    and other information that may be available on or through your Facebook or Gmail account, including
                    your name,
                    email address, date of birth, gender and other information available on such a platform.
                </p>
                <h4>3.2. Usage, log data & cookies</h4>
                <p className="mb-4">
                    When you use GetVoted, we automatically collect certain information, including:
                    (i) information about your interaction with GetVoted, including your search results, services opted
                    for on GetVoted;
                    (ii) technical data, which may include the URL you are coming from, your IP address, unique device
                    ID, network and computer performance,
                    browser type, language and operating system; and
                    (iii) location information. If you have connected your account to Facebook. We may use cookies and
                    other technologies to collect this information.
                    We may store this information so that it can be used for the purposes further explained in this
                    privacy policy.
                </p>
                <h4>3.3. Other Services</h4>
                <p className="mb-4">
                    You may choose to integrate your account with third party services besides Facebook (“Other
                    Integrated Services”).
                    If you do, we may receive, use and share the categories of information described above, including
                    information made
                    public on or through your account on such Other Integrated Service, in accordance with this Privacy
                    Policy.
                </p>
            </>
        },
        {
            title: "Use of information collected",
            body: <>
                <p className="mb-4">
                    We may use the information we collect, including your personal information, to:
                    (i) provide, personalize, and improve your experience with the service;
                    (ii) ensure technical functioning of the platform, develop
                    new services, and analyze your use of the platform;
                    (iii) communicate with you for service-related purposes, including promotional emails or messages;
                    (iv) enforce this privacy policy, the terms and conditions of use, including to protect the rights
                    or safety of GetVoted or any other person(s); and
                    (v) as otherwise stated in this privacy policy.
                </p>
            </>
        },
        {
            title: "Sharing information collected",
            body:
                <>
                    <p className="mb-4">
                        We may share the information we collect, including your personal information, as follows:
                    </p>
                    <h4>5.1. Sharing with Third party services</h4>
                    <p className="mb-4">
                        If you accessed or signed up to our platform using Facebook or Other Integrated Services,
                        we may share your interaction with that service,
                        including the queries and services viewed. You understand that such information may be
                        attributed to your account on Facebook or Other
                        Integrated Service and may be published on such service. We may also enable you to share
                        services accessed on our platform,
                        on an individual basis to third party services by using the share functionality.
                        To share a service to a third-party service,
                        you’ll be prompted to enter your access credentials for that third-party service.
                        We may store your username and an access token or similar
                        credentials obtained from the third-party service, so that you are able to share your
                        interactions,
                        on an individual basis,
                        without entering your access credentials again. You understand and agree that Facebook or other
                        third parties’ use of information they
                        collect from you is governed by their privacy policies.
                    </p>
                    <h4>5.2. Other sharing</h4>
                    <p className="mb-4">
                        We may also share your information to third parties for these limited purposes:
                        a. To allow for a merger, acquisition, or sale of all or a portion of our assets;
                        b. To respond to legal process
                        c. To inform business partners about use of the Service, here information is shared in the form
                        of aggregated statistics or otherwise in a format which does not reveal your identity; and
                        d. If you expressly opt into such sharing. You understand and agree that the third parties’
                        privacy policies will govern the use of information that we provide them under this section.
                    </p>
                </>
        },
        {
            title: "Links",
            body:
                <>
                    <p className="mb-4">
                        We may display advertisements from third parties and other content which links to third party
                        websites.
                        We cannot control or be held responsible for third parties’ privacy practices and content.
                        Please read their privacy policies
                        to find out how they collect and process your personal information.
                    </p>
                </>
        },
        {
            title: "Privacy Rights",
            body:
                <>
                    <p>
                        If you are resident in Nigeria, under the NDPR you have the following rights in respect of your
                        personal information that we hold:
                    </p>
                    <p>
                        <b>Right of access:</b> You have the right to obtain confirmation of whether, and where, we are
                        processing your personal information; information about the categories of personal information
                        we are
                        processing, the purposes for which we process your personal information and information as to
                        how we determine
                        applicable retention periods; information about the categories of recipients with whom we may
                        share
                        your personal information; and a copy of the personal information we hold about you.
                    </p>
                    <p>
                        <b>Right of portability:</b> You have the right, in certain circumstances, to receive a copy of
                        the personal information you have provided to us in a structured, commonly used,
                        machine-readable format
                        that supports re-use, or to request the transfer of your personal data to another person.
                    </p>
                    <p>
                        <b>Right to rectification:</b> You have the right to obtain rectification of any inaccurate or
                        incomplete personal information we hold about you without undue delay.
                    </p>
                    <p>
                        <b>Right to erasure:</b> You have the right, in some circumstances, to require us to erase your
                        personal information without undue delay if the continued processing of that personal
                        information is not justified.
                        Right to restriction: You have the right, in some circumstances, to require us to limit the
                        purposes for which we process
                        your personal information if the continued processing of the personal information in this way is
                        not justified, such as
                        where the accuracy of the personal information is contested by you.
                    </p>
                    <p>
                        <b>Right to object:</b> You have a right to object to any processing based on our legitimate
                        interests where there are grounds relating to your particular situation. There may be compelling
                        reasons
                        for continuing to process your personal information, and we will assess and inform you if that
                        is the case. You can object
                        to marketing activities for any reason. If you wish to exercise one of these rights, please
                        contact us at <a
                        href="mailto:hello@390labs.com">hello@390labs.com </a>
                        You also have the right to lodge a complaint to your local data protection authority (NITDA)
                        Residents in other
                        jurisdictions may also have similar rights to the above. if you would like to exercise one of
                        these rights, kindly contact
                        us and we will comply with any request to the extent required under applicable law
                    </p>
                </>
        },
        {
            title: "Security",
            body:
                <>
                    <p className="mb-4">
                        We are committed to protecting our users’ information. Your password protects your account,
                        so you should use a unique and strong password, limit access to your computer and browser, and
                        sign off after
                        having used the platform. While we take reasonable data protection precautions, no security
                        measures are completely secure,
                        and we do not guarantee the security of user information at any time.
                    </p>
                </>
        },
        {
            title: "Underage Information",
            body:
                <>
                    <p className="mb-4">
                        For users in Nigeria, our service is not directed to children under the age of 16, and we do not
                        knowingly collect personal information from children under 16. If you are under 16 years of age,
                        do not use the Service and do not provide any personal information to us.
                    </p>
                </>
        },
        {
            title: "Contact us",
            body:
                <>
                    <p className="mb-4">
                        390labs Limited is the company responsible for collection, use and disclosure of your Personal
                        Information
                        under this Privacy Policy. If you have any questions or comments about this Privacy Policy,
                        please contact us
                        at <a href="mailto:hello@390labs.com">hello@390labs.com</a>
                    </p>
                </>
        },
        {
            title: "Changes to our Privacy Policy",
            body:
                <>
                    <p className="mb-4">
                        We may make changes to this Privacy Policy from time to time, so please visit this Privacy
                        Policy regularly.
                        If we make changes which we believe are material, we will inform you through the Service. Your
                        continued use
                        of the service thereafter constitutes acceptance of the changes.
                    </p>
                </>
        },
        {
            title: "Information about cookies and similar technologies.",
            body:
                <>
                    <p className="mb-4">
                        What are cookies and other technologies? A cookie is a small text file that is placed on your
                        computer, mobile phone,
                        or other device when you visit a website. The cookie will help website providers to recognise
                        your device the next time
                        you visit their website. There are other technologies such as pixel tags, web bugs, web storage
                        and other similar files and
                        technologies that can also do the same thing as cookies. GetVoted uses the term "cookies" in
                        this policy to refer to cookies
                        and all such other technologies that collect information in this way.
                    </p>
                </>
        },
        {
            title: "Manage your preferences",
            body:
                <>
                    <p className="mb-4">
                        Most web browsers allow you to manage your cookie preferences.
                        You can set your browser to refuse cookies or delete certain cookies.
                        Generally, you should also be able manage similar technologies in the same way that you manage
                        cookies
                        – using your browser's preferences. Please note that if you choose to block cookies, this may
                        impair or
                        prevent due functioning of the service.
                    </p>
                </>
        },
    ]
    const [active, setActive] = useState(0)
    return (
        <>
            <main className="page">
                <div className="page-container container my-5">
                    <div className="page__header">
                        <h1 className="title text-4x">Privacy Policy</h1>
                        <p className="text-md">Last updated on July 1, 2022. </p>
                    </div>
                    <div className="page__body mt-5">
                        <div className="row">
                            <div className="col-md-3">
                                {
                                    sections.map((section, index) =>
                                        <p className="navigation" key={index}>
                                            <a className={`mb-4${active === index ? '' : ' active'}`}
                                               href={`#section${index}`}
                                               onClick={() => setActive(index)}>{section.title}</a>
                                        </p>
                                    )
                                }
                            </div>
                            <div className="col-md-8 page__body-section">
                                <p>
                                    Hi, and thanks for visiting our Privacy Policy section. Our Policy is here to help
                                    you make informed decisions about your interaction with us, so kindly read it
                                    carefully. Effective as from July 1, 2022
                                </p>
                                {
                                    sections.map((section, index) =>
                                        <div className="mb-40" id={`section${index}`} key={index}>
                                            <h2>{section.title}</h2>
                                            <div>
                                                {section.body}
                                            </div>
                                        </div>
                                    )
                                }
                            </div>
                        </div>
                    </div>
                </div>
                <Footer/>
            </main>
        </>
    )
}
