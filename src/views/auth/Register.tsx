import {Link, useNavigate} from "react-router-dom";
import {FormValidationError} from "../../shared/components/form/FormValidationError";
import {useAppDispatch} from "../../shared/store/hooks";
import {authInitialState, registerAsync, RegisterInterface} from "../../shared/store/features/authSlice";
import * as Yup from "yup";
import {Form, Formik, FormikHelpers} from "formik";
import GetVotedLogo from "../../assets/images/getvoted-logo.svg";
import RegisterImage from "../../assets/images/register-image-1.svg";
import {TextInput} from "../../shared/components/form/TextInput";
import {useState} from "react";
import {Button} from "../../shared/components/form/Button";

export const Register = (): JSX.Element => {

    const dispatch = useAppDispatch()
    const navigate = useNavigate();

    const [formError, setFormError] = useState("");

    const formValidation = () => Yup.object({
        userName: Yup.string().required('Required'),
        email: Yup.string().required('Required'),
        password: Yup.string().required('Required'),
        confirmPassword: Yup.string().oneOf([Yup.ref('password'), null], 'Passwords must match')
    });

    const register = (formValues: RegisterInterface, actions: FormikHelpers<RegisterInterface>) => {

        setFormError("")

        actions.setSubmitting(true);

        dispatch(registerAsync({
            userName: formValues.userName,
            email: formValues.email,
            password: formValues.password,
            confirmPassword: formValues.confirmPassword,
        }))
            .unwrap()
            .then(() => navigate(`/auth/verify-email/1/${formValues.email}`))
            .catch((error: any) => setFormError(error.errors))
            .finally(() => actions.setSubmitting(false));
    };

    return (
        <div className="auth-container">
            <div className="container">
                <div className="auth-1-grid">

                    <div className="auth-1-welcome">
                        <img src={GetVotedLogo} alt="Logo"/>
                        <h2 className="auth-1-welcome--text">Create a contest,
                            share your link & get
                            results in realtime.</h2>

                        <div className="floating-image">
                            <img src={RegisterImage} alt="Register"/>
                        </div>
                    </div>

                    <div>
                        <div className="auth-card">
                            <div>
                                <h2>Sign Up</h2>
                                <p className="other-links mb-4">
                                    Got an account? &nbsp;
                                    <Link to="/auth/Login" className="text-primary">Login</Link>
                                </p>
                            </div>

                            <Formik
                                initialValues={authInitialState.register}
                                validationSchema={formValidation}
                                onSubmit={(formValues: RegisterInterface, actions: FormikHelpers<RegisterInterface>) => register(formValues, actions)}>
                                {({isSubmitting, isValid, dirty}) => (
                                    <Form>
                                        <FormValidationError formError={formError}/>

                                        <TextInput
                                            label="Username"
                                            name="userName"
                                            type="text"
                                        />

                                        <TextInput
                                            label="Email Address"
                                            name="email"
                                            type="email"
                                        />

                                        <TextInput
                                            label="Password"
                                            name="password"
                                            type="password"
                                        />

                                        <TextInput
                                            label="Confirm Password"
                                            name="confirmPassword"
                                            type="password"
                                        />

                                        <Button type="submit" variant="primary" loading={isSubmitting}
                                                disabled={!dirty || !isValid} body="Continue"
                                                btnClass='button--block mb-4' size='md'/>
                                    </Form>
                                )}
                            </Formik>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    );
}

