export const AuthGreetingMessage = (): JSX.Element => {
    return <>
        <h4>Sign in</h4>
        <p><small className="text-muted">Login to manage your account</small></p>
    </>;
}
