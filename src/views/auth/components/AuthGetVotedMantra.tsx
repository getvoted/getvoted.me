import GetVotedLogo from '../../../assets/images/getvoted-logo.svg'

export const AuthGetVotedMantra = (): JSX.Element => {
    return <>
        <img src={GetVotedLogo} alt="Logo" width="200px" />
        {/*<h4 className="mb-3 text-white font-domaine">GetVoted</h4>*/}
        {/*<div className="text-fade">Vote or be voted.</div>*/}
    </>;
}
