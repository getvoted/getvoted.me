import {useNavigate, useParams} from "react-router-dom";
import {useAppDispatch} from "../../shared/store/hooks";
import {useEffect, useState} from "react";
import {authInitialState, resetPasswordAsync, ResetPasswordInterface,} from "../../shared/store/features/authSlice";
import {FormValidationError} from "../../shared/components/form/FormValidationError";
import {toast} from "react-toastify";
import {Form, Formik, FormikHelpers} from "formik";
import {TextInput} from "../../shared/components/form/TextInput";
import * as Yup from "yup";
import GetVotedLogo from "../../assets/images/getvoted-logo.svg";
import ForgotImage from "../../assets/images/forgot-image-1.svg";
import {Tokens} from "../../shared/utils/tokens";
import {App} from "../../shared/utils/app";

export const ResetPassword = (): JSX.Element => {

    const dispatch = useAppDispatch()
    const navigate = useNavigate();
    const params = useParams();

    const [formError, setFormError] = useState("");
    const [token, setToken] = useState("");

    useEffect(() => {
        if (params.token) {
            setToken(params.token);
        }
    }, [params]);

    const formValidation = () => Yup.object({
        newPassword: Yup.string().required('Password is required'),
        confirmPassword: Yup.string().oneOf([Yup.ref('newPassword'), null], 'Passwords must match')
    });

    const resetPassword = (formValues: ResetPasswordInterface, actions: FormikHelpers<ResetPasswordInterface>) => {
        setFormError("")

        actions.setSubmitting(true);

        dispatch(resetPasswordAsync({
            newPassword: formValues.newPassword,
            confirmPassword: formValues.confirmPassword,
            token: token,
        }))
            .unwrap()
            .then(() => {
                actions.resetForm();

                toast.success("Password Reset Successful")

                const intendedURL = Tokens.getIntendedURL();

                App.clearIntendedURL();

                if (intendedURL) {
                    window.location.href = intendedURL;
                } else {
                    navigate(`/`, {replace: true})
                }
            })
            .catch((error: any) => {
                setFormError(error.errors)
                toast.error(error.message)
            })
            .finally(() => {
                actions.setSubmitting(false);
            });
    };

    return (
        <div className="auth-container">
            <div className="container">
                <div className="auth-2-grid">

                    <div className="logo">
                        <img src={GetVotedLogo} alt="Logo"/>
                    </div>

                    <div className="auth-card">
                        <div className="text-center">
                            <h3>Reset Password</h3>
                            <p className="other-links mb-4">Congratulations on getting thus far!</p>
                        </div>

                        <Formik
                            initialValues={authInitialState.resetPassword}
                            validationSchema={formValidation}
                            onSubmit={(formValues: ResetPasswordInterface, actions: FormikHelpers<ResetPasswordInterface>) => resetPassword(formValues, actions)}>

                            {({isSubmitting}) => (
                                <Form>
                                    <FormValidationError formError={formError}/>

                                    <TextInput
                                        label="New Password"
                                        name="newPassword"
                                        type="password"
                                    />

                                    <TextInput
                                        label="Repeat Password"
                                        name="confirmPassword"
                                        type="password"
                                    />

                                    <button type="submit"
                                            className="button button--primary button--block mb-4"
                                            disabled={isSubmitting}>
                                        {isSubmitting ? 'Resetting...' : 'Reset Password'}
                                    </button>
                                </Form>
                            )}
                        </Formik>
                    </div>

                    <div className="floating-image">
                        <img src={ForgotImage} alt="Forgot"/>
                    </div>

                </div>
            </div>
        </div>
    );
}

