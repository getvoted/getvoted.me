import {Link, useNavigate, useParams} from "react-router-dom";
import {useAppDispatch} from "../../shared/store/hooks";
import {FormEvent, useEffect, useState} from "react";
import {
    resendEmailVerificationLinkAsync,
    verifyEmailAsync
} from "../../shared/store/features/authSlice";
import {FormValidationError} from "../../shared/components/form/FormValidationError";
import {toast} from "react-toastify";
import GetVotedLogo from "../../assets/images/getvoted-logo.svg";
import ForgotImage from "../../assets/images/forgot-image-1.svg";
import {Tokens} from "../../shared/utils/tokens";
import {App} from "../../shared/utils/app";

export const VerifyEmail = (): JSX.Element => {

    const dispatch = useAppDispatch()
    const navigate = useNavigate();
    const params = useParams();

    const [loading, setLoading] = useState(false);
    const [resending, setResending] = useState(false);
    const [formError, setFormError] = useState("");
    const [email, setEmail] = useState("");

    useEffect(() => {
        const queryParams = new URLSearchParams(window.location.search);
        const verificationToken = queryParams.get('verificationToken');

        if (params.email) {
            setEmail(params.email);

            if (verificationToken) {
                verifyEmail(params.email, verificationToken)
            }
        }
    }, [params]);

    const verifyEmail = (userEmail: string, userToken: string) => {
        setFormError("")
        setLoading(true)

        dispatch(verifyEmailAsync({
            email: userEmail,
            token: userToken
        })).unwrap()
            .then(() => {
                toast.success("Email Verified")

                const intendedURL = Tokens.getIntendedURL();

                App.clearIntendedURL();

                if (intendedURL) {
                    window.location.href = intendedURL;
                } else {
                    navigate(`/`, {replace: true})
                }
            })
            .catch((error: any) => setFormError(error.errors))
            .finally(() => setLoading(false));
    };

    const resendVerificationToken = (event: FormEvent) => {
        event.preventDefault();

        setResending(true)

        dispatch(resendEmailVerificationLinkAsync({
            email: email,
            redirectTo: `/auth/verify-email/0`
        })).unwrap()
            .then(() => {
                toast.success("Verification email resent")
            })
            .finally(() => setResending(false));
    };

    return (
        <div className="auth-container">
            <div className="container">
                <div className="auth-2-grid">
                    <div className="logo">
                        <Link to={'/'}>
                            <img src={GetVotedLogo} alt="Logo"/>
                        </Link>
                    </div>

                    <div className="auth-card text-center">
                        {formError &&
                            <>
                                <h3>Error verifying email</h3>
                                <br/>
                                <FormValidationError formError={formError}/>
                            </>
                        }

                        {(loading && !formError) &&
                            <>
                                <h3>Confirming Verification</h3>
                                <p className="mb-0">Give us a second (or more)...</p>
                            </>
                        }

                        {(!loading && !formError) &&
                            <>
                                <h3>Verify your email address</h3>
                                <p className="other-links">An email has been sent to <span
                                    className="text-primary">{email}</span></p>

                                <hr/>

                                <form onSubmit={resendVerificationToken}>
                                    <p className="mb-3">Didn’t receive any code?</p>
                                    <button type="submit"
                                            className="button button--primary button--outline"
                                            disabled={resending}>
                                        {resending ? 'In a bit...' : 'Resend Verification Code'}
                                    </button>
                                </form>

                                <p className="mt-4 mb-0">
                                    <Link to={'/'}>Back to log in</Link>
                                </p>
                            </>
                        }
                    </div>

                    <div className="floating-image">
                        <img src={ForgotImage} alt="Forgot"/>
                    </div>
                </div>
            </div>
        </div>
    );
}

