import {useState} from "react";
import GetVotedLogo from '../../assets/images/getvoted-logo.svg'
import ForgotImage from '../../assets/images/forgot-image-1.svg'
import {Link} from "react-router-dom";
import {useAppDispatch} from "../../shared/store/hooks";
import {authInitialState, forgotPasswordAsync, ForgotPasswordInterface} from "../../shared/store/features/authSlice";
import {FormValidationError} from "../../shared/components/form/FormValidationError";
import {FormSuccess} from "../../shared/components/form/FormSuccess";
import {Formik, Form, FormikHelpers} from 'formik';
import * as Yup from 'yup';
import {TextInput} from "../../shared/components/form/TextInput";
import {toast} from "react-toastify";
import {Button} from "../../shared/components/form/Button";

export const ForgotPassword = (): JSX.Element => {

    const dispatch = useAppDispatch()

    const [formError, setFormError] = useState("");
    const [formSuccess, setFormSuccess] = useState("");

    const formValidation = () => Yup.object({
        email: Yup.string().email('Invalid email address').required('Required'),
    });

    const forgotPassword = (formValues: ForgotPasswordInterface, actions: FormikHelpers<ForgotPasswordInterface>) => {

        setFormError("")
        setFormSuccess("")

        actions.setSubmitting(true);

        dispatch(forgotPasswordAsync({
            email: formValues.email,
        }))
            .unwrap()
            .then(() => {
                setFormSuccess("Password recovery link has been sent to your email")
                actions.resetForm();
            })
            .catch((error: any) => {
                setFormError(error.errors)
                toast.error(error.message)
            })
            .finally(() => {
                actions.setSubmitting(false);
            });
    };

    return (
        <div className="auth-container">
            <div className="container">
                <div className="auth-2-grid">

                    <div className="logo">
                        <img src={GetVotedLogo} alt="Logo"/>
                    </div>

                    <div className="auth-card">
                        <div className="text-center">
                            <h3>Forgot Password</h3>
                            <p className="other-links mb-4">We'll send the mail to your account</p>
                        </div>

                        <Formik
                            initialValues={authInitialState.forgotPassword}
                            validationSchema={formValidation}
                            onSubmit={(formValues: ForgotPasswordInterface, actions: FormikHelpers<ForgotPasswordInterface>) => forgotPassword(formValues, actions)}>

                            {({isSubmitting, isValid, dirty}) => (
                                <Form>

                                    <FormValidationError formError={formError}/>
                                    <FormSuccess formSuccess={formSuccess}/>

                                    <TextInput
                                        label="Email Address"
                                        name="email"
                                        type="email"
                                        placeholder="Type here"
                                    />

                                    <div className="mb-4">
                                        <Link to="/auth/login" className="other-links">
                                            Remember your password?
                                        </Link>
                                    </div>

                                    <Button type="submit" variant="primary" loading={isSubmitting}
                                            disabled={!dirty || !isValid} body="Send Email"
                                            btnClass='button--block mb-4' size='md'/>

                                </Form>
                            )}

                        </Formik>
                    </div>

                    <div className="floating-image">
                        <img src={ForgotImage} alt="Forgot"/>
                    </div>

                </div>
            </div>

        </div>
    );
}
