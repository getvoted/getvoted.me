import {useState} from "react";
import GetVotedLogo from '../../assets/images/getvoted-logo.svg'
import LoginImage from '../../assets/images/login-image-1.svg'
import {Link, useNavigate} from "react-router-dom";
import {useAppDispatch} from "../../shared/store/hooks";
import {authInitialState, loginAsync, LoginInterface} from "../../shared/store/features/authSlice";
import {FormValidationError} from "../../shared/components/form/FormValidationError";
import {Form, Formik, FormikHelpers} from "formik";
import {TextInput} from "../../shared/components/form/TextInput";
import * as Yup from "yup";
import {Tokens} from "../../shared/utils/tokens";
import {App} from "../../shared/utils/app";
import {Button} from "../../shared/components/form/Button";

export const Login = (): JSX.Element => {

    const dispatch = useAppDispatch()
    const navigate = useNavigate();

    const [formError, setFormError] = useState("");

    const formValidation = () => Yup.object({
        email: Yup.string().required('Required'),
        password: Yup.string().required('Required'),
    });

    const login = (formValues: LoginInterface, actions: FormikHelpers<LoginInterface>) => {

        setFormError("")

        actions.setSubmitting(true);

        dispatch(loginAsync({
            email: formValues.email,
            password: formValues.password,
        }))
            .unwrap()
            .then(() => {
                const intendedURL = Tokens.getIntendedURL();

                App.clearIntendedURL();

                if (intendedURL) {
                    window.location.href = intendedURL;
                } else {
                    navigate(`/`, {replace: true})
                }
            })
            .catch((error: any) => setFormError(error.message))
            .finally(() => actions.setSubmitting(false));
    };

    return (
        <div>
            <div className="container">
                <div className="auth-1-grid">

                    <div className="auth-1-welcome">
                        <img src={GetVotedLogo} alt="Logo"/>
                        <h2 className="auth-1-welcome--text">Create a contest,
                            share your link & get
                            results in realtime.</h2>

                        <div className="floating-image">
                            <img src={LoginImage} alt="Login"/>
                        </div>
                    </div>

                    <div>
                        <div className="auth-card">
                            <div>
                                <h2>Log In</h2>
                                <p className="other-links mb-4">
                                    Don't have an account? &nbsp;
                                    <Link to="/auth/register" className="text-primary">Sign Up</Link>
                                </p>
                            </div>

                            <Formik
                                initialValues={authInitialState.login}
                                validationSchema={formValidation}
                                onSubmit={(formValues: LoginInterface, actions: FormikHelpers<LoginInterface>) => login(formValues, actions)}>
                                {({isSubmitting, isValid, dirty}) => (
                                    <Form>
                                        <FormValidationError formError={formError}/>

                                        <TextInput
                                            label="Email Address"
                                            name="email"
                                            type="email"
                                        />

                                        <TextInput
                                            label="Password"
                                            name="password"
                                            type="password"
                                        />

                                        <div className="mb-4">
                                            <Link to="/auth/forgot-password" className="other-links">
                                                Forgot password?
                                            </Link>
                                        </div>

                                        <Button type="submit" loading={isSubmitting} variant='primary'
                                                disabled={!dirty || !isValid} body="Log In" btnClass='button--block'
                                                size='md'/>
                                    </Form>
                                )}
                            </Formik>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    );
}
