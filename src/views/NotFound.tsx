import {Navbar} from "../shared/components/layout/Navbar";
import {Footer} from "../shared/components/layout/Footer"
import PageImg from "../assets/images/Four-oh-four.svg"
import {Button} from "../shared/components/form/Button";
import {useNavigate} from "react-router-dom";

export const NotFound = (): JSX.Element => {

    const navigate = useNavigate();

    return (
        <main>
            <Navbar />
            <main className="page-404 row align-items-center justify-content-center">
                <section className="col-md-6">
                    <h1 className="text-5x">404</h1>
                    <img src={PageImg} alt="404"/>
                    <p className="description">The  page you are trying to access does not exist</p>
                    <Button loading={false} variant='primary' disabled={false} body={'Go to homepage'} size='md' onClick={() => navigate('/')}/>
                </section>
            </main>
            <Footer />
        </main>
    )

}
