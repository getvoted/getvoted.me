### STAGE 1: Build ###
FROM node:18.4-alpine AS build

LABEL maintainer="Opemipo Bolaji <bolajipemipo@gmail.com>"

WORKDIR /app
COPY . ./
RUN npm install
#COPY . .
RUN npm run build

### STAGE 2: Run ###
#ENV PORT=$PORT

FROM nginx:1.17.1-alpine
#COPY nginx.conf /etc/nginx/nginx.conf
COPY nginx.conf /etc/nginx/conf.d/configfile.template

COPY --from=build /app/build /usr/share/nginx/html

ENV PORT 8080
ENV HOST 0.0.0.0
EXPOSE 8080

#CMD ["nginx", "-g", "daemon off;"]
CMD sh -c "envsubst '\$PORT' < /etc/nginx/conf.d/configfile.template > /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'"
